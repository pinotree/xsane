/* xsane -- a graphical (X11, gtk) scanner-oriented SANE frontend

   xsane-preview.c

   Oliver Rauch <Oliver.Rauch@rauch-domain.de>
   Copyright (C) 1998-2010 Oliver Rauch
   This file is part of the XSANE package.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */ 

/* ---------------------------------------------------------------------------------------------------------------------- */

/*

  The preview strategy is as follows:
  -----------------------------------

   1) The preview is done on the full scan area or a part of it.

   2) The preview is zoomable so the user can precisely pick
      the selection area even for small scans on a large scan
      surface.

   3) The preview window is resizeable.

   4) The preview scan resolution depends on preview window size
      and the selected preview surface (zoom area).

   5) We let the user/backend pick whether a preview is in color,
      grayscale, lineart or what not.  The only options that the
      preview may (temporarily) modify are:

	- resolution (set so the preview fills the window)
	- scan area options (top-left corner, bottom-right corner)
	- preview option (to let the backend know we're doing a preview)
        - gamma table is set to default (gamma=1.0)

   5) The initial size of the scan surface is determined based on the constraints
      of the four corner coordinates.  Missing constraints are replaced
      by 0/+INF as appropriate (0 for top-left, +INF for bottom-right coords).

   6) Given the preview window size and the scan surface size, we
      select the resolution so the acquired preview image just fits
      in the preview window.  The resulting resolution may be out
      of range in which case we pick the minum/maximum if there is
      a range or word-list constraint or a default value if there is
      no such constraint.

   7) Once a preview image has been acquired, we know the size of the
      preview image (in pixels).  An initial scale factor is chosen
      so the image fits into the preview window.

   8) Surface definitions:
      device = surface of the scanner
      image = same oriantation like device
      preview = rotated (0/90/180/270 degree) device surface
      window = same oriantation like device, may be different scaling

*/

/* ---------------------------------------------------------------------------------------------------------------------- */

#include "xsane.h"
/* #include <sys/param.h> */
#include "xsane-back-gtk.h"
#include "xsane-front-gtk.h"
#include "xsane-batch-scan.h"
#include "xsane-preview.h"
#include "xsane-preferences.h"
#include "xsane-gamma.h"


#ifndef PATH_MAX
# define PATH_MAX	1024
#endif

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Cut fp conversion routines some slack: */
#define GROSSLY_EQUAL(f1,f2)		(fabs ((f1) - (f2)) < 1e-3) 

#ifdef __alpha__
  /* This seems to be necessary for at least some XFree86 3.1.2
     servers.  It's known to be necessary for the XF86_TGA server for
     Linux/Alpha.  Fortunately, it's no great loss so we turn this on
     by default for now.  */
# define XSERVER_WITH_BUGGY_VISUALS
#endif

#define min(x,y) ((x) > (y) ? (y) : (x))
#define max(x,y) ((x) > (y) ? (x) : (y))

/* ---------------------------------------------------------------------------------------------------------------------- */

#define XSANE_ZOOM_SIZE 80
#define XSANE_ZOOM_FACTOR 4

/* ---------------------------------------------------------------------------------------------------------------------- */

static u_char *preview_gamma_data_red   = 0;
static u_char *preview_gamma_data_green = 0;
static u_char *preview_gamma_data_blue  = 0;

static u_char *histogram_gamma_data_red   = 0;
static u_char *histogram_gamma_data_green = 0;
static u_char *histogram_gamma_data_blue  = 0;

/* histogram_medium_gamma_data_* is used when medium correction is done after preview-scan by xsane */
static u_char *histogram_medium_gamma_data_red   = 0;
static u_char *histogram_medium_gamma_data_green = 0;
static u_char *histogram_medium_gamma_data_blue  = 0;

static guint preview_gamma_input_bits;

static char *ratio_string[] = { "free", " 2:1", "16:9", "15:10",    " 4:3",   " 1:1", " 3:4", " 9:16", "10:15",  " 1:2"};
static double ratio_value[]  =  { 0.0,    2.0, 16.0/9.0, 15.0/10.0, 4.0/3.0,  1.0,  0.75,  0.5625, 10.0/15.0, 0.5 };

/* ---------------------------------------------------------------------------------------------------------------------- */

/* General geometric calculation. */

static void geom_set_surface(const double source[4], double destination[4]);
static void geom_set_int_surface_by_double_surface(const double source[4], int destination[4]);
static void geom_get_center_of_surface(const double surface[4], double center[2]);
static double geom_get_width_of_surface(const double surface[4]);
static double geom_get_height_of_surface(const double surface[4]);
static void geom_get_dimensions_of_surface(const double surface[4], double dimensions[2]);
static double geom_get_aspect_ratio(const double surface[4]);
static int geom_check_point_in_surface(const double point[2], const double surface[4]);

static void geom_set_point(const double source[2], double destination[2]);
static void geom_multiply_point_by_scalar(const double multiplicand[2], double multiplier, double product[2]);
static void geom_subtract_points(const double minuend[2], const double subtrahend[2], double difference[2]);
static void geom_add_points(const double first[2], const double second[2], double sum[2]);

static int geom_points_not_equal(const double first[2], const double second[2]);

static void geom_shift_surface_by_point(double surface[4], const double shift[2]);
static void geom_shift_point_by_point(double point[2], const double shift[2]);

static void geom_bound_surface_to_surface(double surface[4], const double bound[4]);
static void geom_bound_point_to_surface(double point[2], const double bound[4]);

static const double point_0_0[2] = { 0., 0. };
static const double point_1_0[2] = { 1., 0. };
static const double point_0_1[2] = { 0., 1. };

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Basic rotations.

   1. Rotation is defined by an integer:
        0...3 - 0, 90, 180, 270 degrees clockwise,
        4...7 - the same but mirrored by Oy axis after rotation.
   2. Rotation of orig_scanner_surface is a rectangle with required orientation, its left-bottom point is (0; 0).
   3. Rotation of any other object is its transition from orig_scanner_surface to its rotated version.
*/

static void geom_rotate_point(const Preview* p, const double point[2], double result[2]);
static void geom_rotate_back_point(const Preview* p, const double point[2], double result[2]);

static void geom_rotate_surface(const Preview* p, const double surface[4], double result[4]);
static void geom_rotate_back_surface(const Preview* p, const double surface[4], double result[4]);

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Transformations between different coordinates. */

static void preview_transform_surface_device_to_window(const Preview *p, const double dsurface[4], double wsurface[4]);

static void preview_transform_coordinate_window_to_device(const Preview *p, double winx, double winy, double dcoord[2]);

static void preview_transform_coordinate_device_to_image(const Preview *p, const double dcoord[2], double icoord[2]);
static void preview_tranform_surface_device_to_image(const Preview *p, const double dsurface[4], double isurface[4]);

static void preview_transform_coordinate_image_to_device(const Preview *p, const double icoord[2], double dcoord[2]);
static void preview_transform_surface_image_to_device(const Preview *p, const double isurface[4], double dsurface[4]);

static void preview_transform_coordinate_window_to_image(const Preview *p, const double wcoord[2], double icoord[2]);

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_bound_selection(Preview *p);
static void preview_draw_rect(Preview *p, GdkWindow *win, GdkGC *gc, double coord[4]);
static void preview_draw_selection(Preview *p);
static void preview_update_selection(Preview *p);
static void preview_establish_selection(Preview *p);
/* static void preview_update_batch_selection(Preview *p); */
static void preview_paint_image(Preview *p);
static void preview_display_partial_image(Preview *p);
static void preview_display_maybe(Preview *p);
static void preview_display_image(Preview *p);
static void preview_save_option(Preview *p, int option, void *save_loc, int *valid);
static void preview_restore_option(Preview *p, int option, void *saved_value, int valid);
static void preview_set_option(Preview *p, int option, void *value);
static void preview_set_option_double(Preview *p, int option, double value);
static void preview_set_option_val(Preview *p, int option, SANE_Int value);
static int  preview_increment_image_y(Preview *p);
static gboolean preview_read_image_data(GIOChannel* source, GIOCondition cond, gpointer data);
static void preview_scan_done(Preview *p, int save_image);
static void preview_scan_start(Preview *p);
static int preview_make_image_path(Preview *p, size_t filename_size, char *filename, int level);
static void preview_restore_image(Preview *p);
static gint preview_expose_event_handler_start(GtkWidget *window, GdkEvent *event, gpointer data);
static gint preview_expose_event_handler_end(GtkWidget *window, GdkEvent *event, gpointer data);
static gboolean preview_hold_event_handler(gpointer data);
static gint preview_motion_event_handler(GtkWidget *window, GdkEvent *event, gpointer data);
static gint preview_button_press_event_handler(GtkWidget *window, GdkEvent *event, gpointer data);
static gint preview_button_release_event_handler(GtkWidget *window, GdkEvent *event, gpointer data);
static void preview_start_button_clicked(GtkWidget *widget, gpointer data);
static void preview_cancel_button_clicked(GtkWidget *widget, gpointer data);
static void preview_save_image(const Preview *p);
static void preview_delete_images(Preview *p);
static int preview_calculate_full_scanner_surface(Preview *p, double surface[4], SANE_Value_Type *type, SANE_Unit *unit);

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Modifications of the preview window. */

static void preview_select_zoom_point(Preview *p, int preview_x, int preview_y);
static void preview_zoom_not(GtkWidget *window, gpointer data);
static void preview_zoom_out(GtkWidget *window, gpointer data);
static void preview_zoom_area(GtkWidget *window, gpointer data);
static void preview_zoom_undo(GtkWidget *window, gpointer data);
static void preview_update_zoom_buttons(Preview *p);

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_zoom_in(GtkWidget *window, gpointer data);
static void preview_get_color(const Preview *p, int x, int y, int range, int *red, int *green, int *blue);
static void preview_add_batch(GtkWidget *window, Preview *p);
static void preview_pipette_white(GtkWidget *window, gpointer data);
static void preview_pipette_gray(GtkWidget *window, gpointer data);
static void preview_pipette_black(GtkWidget *window, gpointer data);
static void preview_init_autoraise_scan_area(GtkWidget *window, gpointer data);
//void preview_select_full_preview_area(Preview *p);
static void preview_full_preview_area_callback(GtkWidget *widget, gpointer call_data);
static void preview_135_film_half_frame_area_callback(GtkWidget *widget, gpointer call_data);
static void preview_delete_images_callback(GtkWidget *widget, gpointer call_data);
static gint preview_preset_area_rename_callback(GtkWidget *widget, GtkWidget *preset_area_widget);
static gint preview_preset_area_add_callback(GtkWidget *widget, GtkWidget *preset_area_widget);
static gint preview_preset_area_delete_callback(GtkWidget *widget, GtkWidget *preset_area_widget);
static gint preview_preset_area_move_up_callback(GtkWidget *widget, GtkWidget *preset_area_widget);
static gint preview_preset_area_move_down_callback(GtkWidget *widget, GtkWidget *preset_area_widget);
static gint preview_preset_area_context_menu_callback(GtkWidget *widget, GdkEvent *event);
static void preview_preset_area_callback(GtkWidget *widget, gpointer data);
static void preview_rotation_callback(GtkWidget *widget, gpointer data);
static void preview_establish_ratio(Preview *p);
static void preview_ratio_callback(GtkWidget *widget, gpointer data);
static void preview_autoselect_scan_area_callback(GtkWidget *window, gpointer data);
static void preview_init_window_size(Preview *p);

void preview_display_with_correction(Preview *p);
void preview_do_gamma_correction(Preview *p);
int preview_do_color_correction(Preview *p);
gint preview_area_resize_handler(GtkWidget *widget, GdkEvent *event, gpointer data);
void preview_update_maximum_output_size(Preview *p);
void preview_autoraise_scan_area(const Preview *p, int preview_x, int preview_y, double *autoselect_coord);
void preview_autoselect_scan_area(const Preview *p, double *autoselect_coord);

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_set_surface(const double source[4], double destination[4])
{
  int i;
  for(i = 0; i < 4; i++)
  {
    destination[i] = source[i];
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_set_int_surface_by_double_surface(const double source[4], int destination[4])
{
  int i;
  for(i = 0; i < 4; i++)
  {
    destination[i] =(int) source[i];
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_get_center_of_surface(const double surface[4], double center[2]) {
  center[0] = (surface[0] + surface[2]) / 2.;
  center[1] = (surface[1] + surface[3]) / 2.;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static double geom_get_width_of_surface(const double surface[4])
{
  return surface[2] - surface[0];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static double geom_get_height_of_surface(const double surface[4])
{
  return surface[3] - surface[1];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_get_dimensions_of_surface(const double surface[4], double dimensions[2]) {
  dimensions[0] = geom_get_width_of_surface(surface);
  dimensions[1] = geom_get_height_of_surface(surface);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static int geom_check_point_in_surface(const double point[2], const double surface[4]) {
  return
      point[0] >= surface[0] &&
      point[0] <= surface[2] &&
      point[1] >= surface[1] &&
      point[1] <= surface[3];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_shift_surface_by_point(double surface[4], const double shift[2]) {
  surface[0] += shift[0];
  surface[1] += shift[1];
  surface[2] += shift[0];
  surface[3] += shift[1];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_shift_point_by_point(double point[2], const double shift[2]) {
  point[0] += shift[0];
  point[1] += shift[1];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_bound_surface_to_surface(double surface[4], const double bound[4])
{
  geom_bound_point_to_surface(surface, bound);
  geom_bound_point_to_surface(surface + 2, bound);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_bound_point_to_surface(double point[2], const double bound[4])
{
  xsane_bound_double(point, bound[0], bound[2]);
  xsane_bound_double(point + 1, bound[1], bound[3]);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_set_point(const double source[2], double destination[2])
{
  destination[0] = source[0];
  destination[1] = source[1];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_multiply_point_by_scalar(const double multiplicand[2], double multiplier, double product[2]) {
  product[0] = multiplicand[0] * multiplier;
  product[1] = multiplicand[1] * multiplier;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_subtract_points(const double minuend[2], const double subtrahend[2], double difference[2]) {
   difference[0] = minuend[0] - subtrahend[0];
   difference[1] = minuend[1] - subtrahend[1];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_add_points(const double first[2], const double second[2], double sum[2]) {
  sum[0] = first[0] + second[0];
  sum[1] = first[1] + second[1];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static int geom_points_not_equal(const double first[2], const double second[2])
{
  return
      first[0] != second[0] ||
      first[1] != second[1];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_rotate_point(const Preview* p, const double point[2], double result[2])
{
  switch(p->rotation)
  {
  case 0:
    /* 0 degrees. */
    result[0] = point[0] - p->orig_scanner_surface[0];
    result[1] = point[1] - p->orig_scanner_surface[1];
    break;
  case 1:
    /* 90 degrees. */
    result[0] = p->orig_scanner_surface[3] - point[1];
    result[1] = point[0] - p->orig_scanner_surface[0];
    break;
  case 2:
    /* 180 degrees. */
    result[0] = p->orig_scanner_surface[2] - point[0];
    result[1] = p->orig_scanner_surface[3] - point[1];
    break;
  case 3:
    /* 270 degrees. */
    result[0] = point[1] - p->orig_scanner_surface[1];
    result[1] = p->orig_scanner_surface[2] - point[0];
    break;

  case 4:
    /* 0 degrees, mirrored. */
    result[0] = p->orig_scanner_surface[2] - point[0];
    result[1] = point[1] - p->orig_scanner_surface[1];
    break;
  case 5:
    /* 90 degrees, mirrored. */
    result[0] = point[1] - p->orig_scanner_surface[1];
    result[1] = point[0] - p->orig_scanner_surface[0];
    break;
  case 6:
    /* 180 degrees, mirrored. */
    result[0] = point[0] - p->orig_scanner_surface[0];
    result[1] = p->orig_scanner_surface[3] - point[1];
    break;
  case 7:
    /* 270 degrees, mirrored. */
    result[0] = p->orig_scanner_surface[3] - point[1];
    result[1] = p->orig_scanner_surface[2] - point[0];
    break;
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_rotate_back_point(const Preview* p, const double point[2], double result[2])
{
  switch(p->rotation)
  {
  case 0:
    /* 0 degrees. */
    result[0] = point[0] + p->orig_scanner_surface[0];
    result[1] = point[1] + p->orig_scanner_surface[1];
    break;
  case 1:
    /* 90 degrees. */
    result[0] = point[1] + p->orig_scanner_surface[0];
    result[1] = p->orig_scanner_surface[3] - point[0];
    break;
  case 2:
    /* 180 degrees. */
    result[0] = p->orig_scanner_surface[2] - point[0];
    result[1] = p->orig_scanner_surface[3] - point[1];
    break;
  case 3:
    /* 270 degrees. */
    result[0] = p->orig_scanner_surface[2] - point[1];
    result[1] = point[0] - p->orig_scanner_surface[1];
    break;

  case 4:
    /* 0 degrees, mirrored. */
    result[0] = p->orig_scanner_surface[2] - point[0];
    result[1] = point[1] + p->orig_scanner_surface[1];
    break;
  case 5:
    /* 90 degrees, mirrored. */
    result[0] = point[1] + p->orig_scanner_surface[0];
    result[1] = point[0] + p->orig_scanner_surface[1];
    break;
  case 6:
    /* 180 degrees, mirrored. */
    result[0] = point[0] - p->orig_scanner_surface[0];
    result[1] = p->orig_scanner_surface[3] - point[1];
    break;
  case 7:
    /* 270 degrees, mirrored. */
    result[0] = p->orig_scanner_surface[2] - point[1];
    result[1] = p->orig_scanner_surface[3] - point[0];
    break;
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_rotate_surface(const Preview* p, const double surface[4], double result[4])
{
  double left_top[2];
  double right_bottom[2];

  geom_rotate_point(p, surface, left_top);
  geom_rotate_point(p, surface + 2, right_bottom);

  if(left_top[0] < right_bottom[0])
  {
    result[0] = left_top[0];
    result[2] = right_bottom[0];
  }
  else
  {
    result[0] = right_bottom[0];
    result[2] = left_top[0];
  }

  if(left_top[1] < right_bottom[1])
  {
    result[1] = left_top[1];
    result[3] = right_bottom[1];
  }
  else
  {
    result[1] = right_bottom[1];
    result[3] = left_top[1];
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void geom_rotate_back_surface(const Preview* p, const double surface[4], double result[4])
{
  double left_top[2];
  double right_bottom[2];

  geom_rotate_back_point(p, surface, left_top);
  geom_rotate_back_point(p, surface + 2, right_bottom);

  if(left_top[0] < right_bottom[0])
  {
    result[0] = left_top[0];
    result[2] = right_bottom[0];
  }
  else
  {
    result[0] = right_bottom[0];
    result[2] = left_top[0];
  }

  if(left_top[1] < right_bottom[1])
  {
    result[1] = left_top[1];
    result[3] = right_bottom[1];
  }
  else
  {
    result[1] = right_bottom[1];
    result[3] = left_top[1];
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static double geom_get_aspect_ratio(const double surface[4])
{
  if(surface[1] == surface[3]) {
    return INF;
  }
  else
  {
    return (surface[2] - surface[0]) / (surface[3] - surface[1]);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_transform_surface_device_to_window(const Preview *p, const double dsurface[4], double wsurface[4])
{
  double rotated[4];
  geom_rotate_surface(p, dsurface, rotated);

  double surface_dimensions[2];
  geom_get_dimensions_of_surface(p->surface, surface_dimensions);

  double win_surface_dimensions[2];
  geom_get_dimensions_of_surface(p->win_image_surface, win_surface_dimensions);

  wsurface[0] = p->win_image_surface[0] + (rotated[0] - p->surface[0]) * win_surface_dimensions[0] / surface_dimensions[0];
  wsurface[1] = p->win_image_surface[1] + (rotated[1] - p->surface[1]) * win_surface_dimensions[1] / surface_dimensions[1];
  wsurface[2] = p->win_image_surface[0] + (rotated[2] - p->surface[0]) * win_surface_dimensions[0] / surface_dimensions[0];
  wsurface[3] = p->win_image_surface[1] + (rotated[3] - p->surface[1]) * win_surface_dimensions[1] / surface_dimensions[1];

  DBG(DBG_info, "preview_transform_surface_device_to_window: [%3.2f %3.2f %3.2f %3.2f] -> device[%3.2f %3.2f %3.2f %3.2f]\n",
                dsurface[0], dsurface[1], dsurface[2], dsurface[3],
                wsurface[0], wsurface[1], wsurface[2], wsurface[3]);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_transform_coordinate_window_to_device(const Preview *p, double winx, double winy, double dcoord[2])
{
  DBG(DBG_proc, "preview_transform_coordinate_window_to_device\n");

  double surface_dimensions[2];
  geom_get_dimensions_of_surface(p->surface, surface_dimensions);

  double win_surface_dimensions[2];
  geom_get_dimensions_of_surface(p->win_image_surface, win_surface_dimensions);

  double point[2];
  point[0] = p->surface[0] + (winx - p->win_image_surface[0]) * surface_dimensions[0] / win_surface_dimensions[0];
  point[1] = p->surface[1] + (winy - p->win_image_surface[1]) * surface_dimensions[1] / win_surface_dimensions[1];

  geom_rotate_back_point(p, point, dcoord);

  DBG(DBG_info, "window[%3.2f %3.2f] -> device[%3.2f %3.2f]\n", winx, winy, point[0], point[1]);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_transform_coordinate_device_to_image(const Preview *p, const double dcoord[2], double icoord[2])
{
  icoord[0] = (dcoord[0] - p->image_surface[0]) * ((double) p->image_width) / geom_get_width_of_surface(p->image_surface);
  icoord[1] = (dcoord[1] - p->image_surface[1]) * ((double) p->image_height) / geom_get_height_of_surface(p->image_surface);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_tranform_surface_device_to_image(const Preview *p, const double dsurface[4], double isurface[4])
{
  preview_transform_coordinate_device_to_image(p, dsurface, isurface);
  preview_transform_coordinate_device_to_image(p, dsurface + 2, isurface + 2);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_transform_coordinate_image_to_device(const Preview *p, const double icoord[2], double dcoord[2])
{
  dcoord[0] = icoord[0] * geom_get_width_of_surface(p->image_surface) / ((double) p->image_width) + p->image_surface[0];
  dcoord[1] = icoord[1] * geom_get_height_of_surface(p->image_surface) / ((double) p->image_height) + p->image_surface[1];
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_transform_surface_image_to_device(const Preview *p, const double isurface[4], double dsurface[4])
{
  preview_transform_coordinate_image_to_device(p, isurface, dsurface);
  preview_transform_coordinate_image_to_device(p, isurface + 2, dsurface + 2);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_transform_coordinate_window_to_image(const Preview *p, const double wcoord[2], double icoord[2])
{
  DBG(DBG_proc, "preview_transform_coordinate_window_to_image\n");

  double device_coord[2];
  preview_transform_coordinate_window_to_device(p, wcoord[0], wcoord[1], device_coord);

  icoord[0] = (device_coord[0] - p->image_surface[0]) * ((double) p->image_width) / geom_get_width_of_surface(p->image_surface);
  icoord[1] = (device_coord[1] - p->image_surface[1]) * ((double) p->image_height) / geom_get_height_of_surface(p->image_surface);

  DBG(DBG_info, "window[%f %f] -> image[%f %f]\n", wcoord[0], wcoord[1], icoord[0], icoord[1]);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_bound_selection(Preview *p)
{
  DBG(DBG_proc, "preview_bound_selection\n");

  p->selection.active = ( (p->selection.coordinate[0] != p->selection.coordinate[2]) &&
                          (p->selection.coordinate[1] != p->selection.coordinate[3]) );

  if (p->selection.active)
  {
    geom_bound_surface_to_surface(p->selection.coordinate, p->scanner_surface);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_draw_rect(Preview *p, GdkWindow *win, GdkGC *gc, double preview_coord[4])
{
  double win_coord[4];

  DBG(DBG_proc, "preview_draw_rect [%3.2f %3.2f %3.2f %3.2f]\n", preview_coord[0], preview_coord[1], preview_coord[2], preview_coord[3]);

  preview_transform_surface_device_to_window(p, preview_coord, win_coord);

  const int width = (int) geom_get_width_of_surface(win_coord);
  const int height = (int) geom_get_height_of_surface(win_coord);

  if(!width && !height)
  {
    /* There is a weak point of the empty selection: it is first shown, but then goes to inactive state (due to the size) without proper removing from screen. */
    return;
  }

  if( width < 3 || height < 3)
  {
    gdk_draw_point(win, gc, (gint) win_coord[0], (gint) win_coord[1]);
  }
  else
  {
    gdk_draw_rectangle(win, gc, FALSE, (gint) win_coord[0], (gint) win_coord[1], width - 1, height - 1);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_draw_selection(Preview *p)
{
  DBG(DBG_proc, "preview_draw_selection\n");

  if (!p->gc_selection) /* window isn't mapped yet */
  {
    return;
  }

  if ( (p->show_selection == FALSE) || (p->calibration) )
  {
    return;
  }

  if (p->previous_selection.active)
  {
    preview_draw_rect(p, gtk_widget_get_window(p->window), p->gc_selection, p->previous_selection.coordinate);
  }

  if (p->selection.active)
  {
    preview_draw_rect(p, gtk_widget_get_window(p->window), p->gc_selection, p->selection.coordinate);
  }

  p->previous_selection = p->selection;


  if (!p->gc_selection_maximum) /* window isn't mapped yet */
  {
    return;
  }

  if (p->previous_selection_maximum.active)
  {
    preview_draw_rect(p, gtk_widget_get_window(p->window), p->gc_selection_maximum, p->previous_selection_maximum.coordinate);
  }

  if (p->selection_maximum.active)
  {
    preview_draw_rect(p, gtk_widget_get_window(p->window), p->gc_selection_maximum, p->selection_maximum.coordinate);
  }

  p->previous_selection_maximum = p->selection_maximum;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_update_selection(Preview *p)
/* draw selection box as defined in backend */
{
 const SANE_Option_Descriptor *opt;
 SANE_Status status;
 SANE_Word val;
 int i, optnum;

  DBG(DBG_proc, "preview_update_selection\n");

  p->previous_selection = p->selection;

  for (i = 0; i < 4; ++i)
  {
    optnum = xsane.well_known.coord[i];
    if (optnum > 0)
    {
      opt = xsane_get_option_descriptor(xsane.dev, optnum);
      status = xsane_control_option(xsane.dev, optnum, SANE_ACTION_GET_VALUE, &val, 0);
      if (status != SANE_STATUS_GOOD)
      {
        continue;
      }
      if (opt->type == SANE_TYPE_FIXED)
      {
        p->selection.coordinate[i] = (double) SANE_UNFIX(val);
      }
      else
      {
        p->selection.coordinate[i] = (double) val;
      }
    }
    else /* backend does not use scan area options */
    {
      switch (i)
      {
        case 0:
        case 1:
          p->selection.coordinate[i] = 0;
         break;

        case 2:
          p->selection.coordinate[i] = geom_get_width_of_surface(p->image_surface);
         break;

        case 3:
          p->selection.coordinate[i] = geom_get_height_of_surface(p->image_surface);
         break;
      }
    }
  }

  p->selection.active = ( (p->selection.coordinate[0] != p->selection.coordinate[2]) &&
                          (p->selection.coordinate[1] != p->selection.coordinate[3]));

  preview_update_maximum_output_size(p);
  preview_draw_selection(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_establish_selection(Preview *p)
{
  /* This routine only shall be called if the preview area really is changed. */
 int i;

  DBG(DBG_proc, "preview_establish_selection\n");

  preview_bound_selection(p);

  xsane.block_update_param = TRUE; /* do not change parameters each time */

  for (i = 0; i < 4; ++i)
  {
    preview_set_option_double(p, xsane.well_known.coord[i], p->selection.coordinate[i]);
  }

  xsane_back_gtk_update_scan_window();

  xsane.block_update_param = FALSE;

  xsane_update_param(0); 
}

/* ---------------------------------------------------------------------------------------------------------------------- */

#if 0
static void preview_update_batch_selection(Preview *p)
{
 Batch_selection *batch_selection;

  DBG(DBG_proc, "preview_update_batch_selection\n");

  if (!p->gc_selection) /* window isn't mapped yet */
  {
    return;
  }

  batch_selection = p->batch_selection;

  while (batch_selection)
  {
    preview_draw_rect(p, p->window->window, p->gc_selection, batch_selection->coordinate);

    batch_selection = batch_selection->next;
  }
}
#endif

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_paint_image(Preview *p)
{
  DBG(DBG_proc, "preview_paint_image\n");

  if (!p->image_data_enh)
  {
    return; /* no image data */
  }

  /* Auxiliary values for lighten further navigation through the image. */
  double image_top_left[2];
  double image_step_right[2];
  double image_step_down[2];
  {
    double window_sum[2], image_sum[2];

    preview_transform_coordinate_window_to_image(p, p->win_image_surface, image_top_left);

    geom_add_points(p->win_image_surface, point_1_0, window_sum);
    preview_transform_coordinate_window_to_image(p, window_sum, image_sum);
    geom_subtract_points(image_sum, image_top_left, image_step_right);

    geom_add_points(p->win_image_surface, point_0_1, window_sum);
    preview_transform_coordinate_window_to_image(p, window_sum, image_sum);
    geom_subtract_points(image_sum, image_top_left, image_step_down);
  }

  /* Keep it ready, avoid redundant cast operations. */
  const int image_left_limit = (int) p->win_image_surface[0];
  const int image_right_limit =(int)  p->win_image_surface[2];
  const int image_top_limit = (int) p->win_image_surface[1];
  const int image_bottom_limit = (int) p->win_image_surface[3];

  int y;

  /* Paint the top and bottom gaps. */
  memset(p->preview_row, 0x80, (guint) p->preview_window_width * 3);
  for(y = 0; y < image_top_limit; y++)
  {
    gtk_preview_draw_row(GTK_PREVIEW(p->window), p->preview_row, 0, y, (gint) p->preview_window_width);
  }
  memset(p->preview_row, 0x80, 3 * (guint) p->preview_window_width);
  for(y = image_bottom_limit; y < p->preview_window_height; y++)
  {
    gtk_preview_draw_row(GTK_PREVIEW(p->window), p->preview_row, 0, y, (gint) p->preview_window_width);
  }

  /* Paint the image. */
  {
    double previous_first_point[2] = { -1, -1 };

    for(y = image_top_limit; y < image_bottom_limit; y++)
    {
      /* Find the first point of row. */
      double current_point[2];
      geom_multiply_point_by_scalar(image_step_down, (gdouble) (y - image_top_limit), current_point);
      geom_shift_point_by_point(current_point, image_top_left);

      if(geom_points_not_equal(current_point, previous_first_point))
      {
        /* This row is not in the buffer yet, read it. */

        geom_set_point(current_point, previous_first_point);

        int x;
        int row_offset = image_left_limit * 3;

        for(x = image_left_limit; x < image_right_limit; x++) {

          int image_offset = ((int) current_point[1]) * 3 * p->image_width + ((int) current_point[0]) * 3;

          p->preview_row[row_offset++] = p->image_data_enh[image_offset++];
          p->preview_row[row_offset++] = p->image_data_enh[image_offset++];
          p->preview_row[row_offset++] = p->image_data_enh[image_offset];

          geom_shift_point_by_point(current_point, image_step_right);
        }
      }

      gtk_preview_draw_row(GTK_PREVIEW(p->window), p->preview_row, 0, y, (gint) p->preview_window_width);
    }
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_display_partial_image(Preview *p)
{
  DBG(DBG_proc, "preview_display_partial_image\n");

  preview_paint_image(p);

  if (gtk_widget_is_drawable(p->window))
  {
    GtkPreview *preview = GTK_PREVIEW(p->window);
    GtkAllocation alloc;
    int src_x, src_y;

    gtk_widget_get_allocation(p->window, &alloc);
    src_x = (alloc.width - preview->buffer_width)/2;
    src_y = (alloc.height - preview->buffer_height)/2;

    gtk_preview_put(preview, gtk_widget_get_window(p->window), gtk_widget_get_style(p->window)->black_gc, src_x, src_y,
                    0, 0, (gint) p->preview_window_width, (gint) p->preview_window_height);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_display_maybe(Preview *p)
{
 time_t now;

  DBG(DBG_proc, "preview_display_maybe\n");

  time(&now);

  if (now > p->image_last_time_updated) /* wait at least one secone */
  {
    p->image_last_time_updated = now;
    preview_display_partial_image(p);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_display_image(Preview *p)
{
  DBG(DBG_proc, "preview_display_image\n");

  /* if image height was unknown and got larger than expected get missing memory */
  if (p->params.lines <= 0 && p->image_y < p->image_height)
  {
    p->image_height   = p->image_y;

    p->image_data_raw = realloc(p->image_data_raw, 6 * (guint) p->image_width * (guint) p->image_height);
    p->image_data_enh = realloc(p->image_data_enh, 3 * (guint) p->image_width * (guint) p->image_height);
    assert(p->image_data_raw);
    assert(p->image_data_enh);
  }

  preview_display_with_correction(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_save_option(Preview *p, int option, void *save_loc, int *valid)
{
 SANE_Status status;

  DBG(DBG_proc, "preview_save_option\n");

  if (option <= 0)
  {
    *valid = 0;
    return;
  }

  status = xsane_control_option(xsane.dev, option, SANE_ACTION_GET_VALUE, save_loc, 0);
  *valid = (status == SANE_STATUS_GOOD);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_restore_option(Preview *p, int option, void *saved_value, int valid)
{
 const SANE_Option_Descriptor *opt;
 SANE_Status status;
 SANE_Handle dev;

  DBG(DBG_proc, "preview_restore_option\n");

  if (!valid)
  {
    return;
  }

  dev = xsane.dev;
  status = xsane_control_option(dev, option, SANE_ACTION_SET_VALUE, saved_value, 0);

  if (status != SANE_STATUS_GOOD)
  {
    char buf[TEXTBUFSIZE];
    opt = xsane_get_option_descriptor(dev, option);
    if (opt && opt->name)
    {
      snprintf(buf, sizeof(buf), "%s %s: %s.", ERR_SET_OPTION, opt->name, XSANE_STRSTATUS(status));
    }
    else
    {
      snprintf(buf, sizeof(buf), "%s %d: %s.", ERR_SET_OPTION, option, XSANE_STRSTATUS(status));
    }
    xsane_back_gtk_error(buf, TRUE);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_set_option_double(Preview *p, int option, double value)
{
 const SANE_Option_Descriptor *opt;
 SANE_Handle dev;
 SANE_Word word;

  DBG(DBG_proc, "preview_set_option_double\n");

  if (option <= 0 || value <= -INF || value >= INF)
  {
    return;
  }

  dev = xsane.dev;
  opt = xsane_get_option_descriptor(dev, option);
  if (opt->type == SANE_TYPE_FIXED)
  {
    word = SANE_FIX(value);
  }
  else
  {
    word = (SANE_Word) (value + 0.5);
  }

  xsane_control_option(dev, option, SANE_ACTION_SET_VALUE, &word, 0);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_set_option(Preview *p, int option, void *value)
{
 SANE_Handle dev;

  DBG(DBG_proc, "preview_set_option\n");

  if (option <= 0)
  {
    return;
  }

  dev = xsane.dev;
  xsane_control_option(dev, option, SANE_ACTION_SET_VALUE, value, 0);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_set_option_val(Preview *p, int option, SANE_Int value)
{
 SANE_Handle dev;

  DBG(DBG_proc, "preview_set_option_val\n");

  if (option <= 0)
  {
    return;
  }

  dev = xsane.dev;
  xsane_control_option(dev, option, SANE_ACTION_SET_VALUE, &value, 0);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static int preview_test_image_y(Preview *p)
{
  if (p->image_y >= p->image_height) /* make sure backend does not send more data then expected */
  {
   char buf[TEXTBUFSIZE];

    --p->image_y;
    preview_scan_done(p, 1);
    snprintf(buf, sizeof(buf), "%s", ERR_TOO_MUCH_DATA);
    xsane_back_gtk_error(buf, TRUE);
   return -1;
  }

 return 0;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static int preview_increment_image_y(Preview *p)
{
 size_t extra_size, offset;
 char buf[TEXTBUFSIZE];

  DBG(DBG_proc, "preview_increment_image_y\n");

  p->image_x = 0;
  ++p->image_y;

  if (p->params.lines <= 0 && p->image_y >= p->image_height) /* backend said it does not know image height */
  {
    offset = (size_t) (3 * p->image_width*p->image_height);
    extra_size = (size_t) (3 * 32 * p->image_width);
    p->image_height += 32;

    p->image_data_raw = realloc(p->image_data_raw, (offset + extra_size) * 2);
    p->image_data_enh = realloc(p->image_data_enh, offset + extra_size);

    if ( (!p->image_data_enh) || (!p->image_data_raw) )
    {
      snprintf(buf, sizeof(buf), "%s %s.", ERR_FAILED_ALLOCATE_IMAGE, strerror(errno));
      preview_scan_done(p, 0);
      xsane_back_gtk_error(buf, TRUE);
     return -1;
    }
    memset(p->image_data_enh + offset, 0xff, extra_size);
  }

  return 0;
}

/* ---------------------------------------------------------------------------------------------------------------------- */
static gboolean preview_read_image_data(GIOChannel* source, GIOCondition cond, gpointer data)
{
 SANE_Status status;
 Preview *p = data;
 char buf[TEXTBUFSIZE];
 u_char imagebuf8[8192];
 guint16 *imagebuf16 = (guint16 *) imagebuf8;
 SANE_Handle dev;
 SANE_Int len;
 int i, j;

  DBG(DBG_proc, "preview_read_image_data\n");

  dev = xsane.dev;
  while (1)
  {
    if ((p->params.depth == 1) || (p->params.depth == 8))
    {
      status = sane_read(dev, imagebuf8, sizeof(imagebuf8), &len);
    }
    else if (p->params.depth == 16)
    {
      if (p->read_offset_16)
      {
        imagebuf8[0] = p->last_offset_16_byte; 
        /* use imagebuf8 and sizeof(imagebuf8) here because sizeof(imagebuf16) returns the size of a pointer */
        status = sane_read(dev, imagebuf8+1, sizeof(imagebuf8) - 1, &len);
        len++;
      }
      else
      {
        status = sane_read(dev, (SANE_Byte *) imagebuf16, sizeof(imagebuf8), &len);
      }

      if (len % 2) /* odd number of bytes */
      {
        len--;
        p->last_offset_16_byte = (guchar) imagebuf16[len];
        p->read_offset_16 = 1;
      }
      else /* even number of bytes */
      {
        p->read_offset_16 = 0;
      }
    }
    else /* bad bitdepth */
    {
      snprintf(buf, sizeof(buf), "%s %d.", ERR_PREVIEW_BAD_DEPTH, p->params.depth);
      preview_scan_done(p, 0);
      xsane_back_gtk_error(buf, TRUE);
     return TRUE;
    }


    if (!p->scanning) /* preview scan may have been canceled while sane_read was executed */
    {
      return TRUE; /* ok, the scan has been canceled */
    }


    if (status != SANE_STATUS_GOOD)
    {
      if (status == SANE_STATUS_EOF)
      {
        if (p->params.last_frame) /* got all preview image data */
        {
          p->invalid = FALSE; /* preview is valid now */
          preview_scan_done(p, 1);     /* scan is done, save image */
         return TRUE; /* ok, all finished */
        }
        else
        {
          if (p->input_active)
          {
            g_source_remove(p->event_source_id);
            p->input_active = FALSE;
          }
          preview_scan_start(p);
          break; /* exit while loop, display_maybe */
        }
      }
      else if (status == SANE_STATUS_CANCELLED)
      {
        p->invalid = FALSE; /* preview is valid now - although it is cancled */
        p->scan_incomplete = TRUE; /* preview is incomplete */
        preview_scan_done(p, 1); /* save scanned part of the preview */
        snprintf(buf, sizeof(buf), "%s", XSANE_STRSTATUS(status));
        xsane_back_gtk_info(buf, TRUE);
       return TRUE;
      }

      /* not SANE_STATUS_GOOD and not SANE_STATUS_EOF and not SANE_STATUS_CANCELLED */
      preview_scan_done(p, 0);
      snprintf(buf, sizeof(buf), "%s %s.", ERR_DURING_READ, XSANE_STRSTATUS(status));
      xsane_back_gtk_error(buf, TRUE);
     return TRUE;
    }

    if (!len)
    {
      if (p->input_active) /* we have a selecet fd */
      {
        break; /* leave preview_read_image_data, will be called by gdk when select_fd event occurs */
      }
      else
      {
        while (gtk_events_pending())
        {
          DBG(DBG_info, "preview_read_image_data: calling gtk_main_iteration\n");
          gtk_main_iteration();
        }
        continue; /* we have to keep this loop running because it will never be called again */
      }
    }

    switch (p->params.format)
    {
      case SANE_FRAME_RGB:
        switch (p->params.depth)
        {
          case 8:
            {
              for (i = 0; i < len; ++i)
              {
                if (preview_test_image_y(p))
                {
                  return TRUE; /* backend sends too much image data */
                }

                p->image_data_raw[p->image_offset]   = imagebuf8[i] * 256;
                p->image_data_enh[p->image_offset++] = imagebuf8[i];

                if (p->image_offset%3 == 0)
                {
                  if (++p->image_x >= p->image_width && preview_increment_image_y(p) < 0)
                  {
                    return TRUE;
                  }
                }
              }
            }
            break;

          case 16:
            {
              for (i = 0; i < len/2; ++i)
              {
                if (preview_test_image_y(p))
                {
                  return TRUE; /* backend sends too much image data */
                }
  
                p->image_data_raw[p->image_offset]   = imagebuf16[i];
                p->image_data_enh[p->image_offset++] = (u_char) (imagebuf16[i]/256);
  
                if (p->image_offset%3 == 0)
                {
                  if (++p->image_x >= p->image_width && preview_increment_image_y(p) < 0)
                  {
                    return TRUE;
                  }
                }
              }
            }
            break;

          default:
            snprintf(buf, sizeof(buf), "%s %d.", ERR_PREVIEW_BAD_DEPTH, p->params.depth);
            preview_scan_done(p, 0);
            xsane_back_gtk_error(buf, TRUE);
           return TRUE;
        }
       break;

      case SANE_FRAME_GRAY:
        switch (p->params.depth)
        {
          case 1:
            for (i = 0; i < len; ++i)
            {
              u_char mask = imagebuf8[i];

              if (preview_test_image_y(p))
              {
                return TRUE; /* backend sends too much image data */
              }

              for (j = 7; j >= 0; --j)
              {
                u_char gl = (mask & (1 << j)) ? 0x00 : 0xff;

                p->image_data_raw[p->image_offset]   = gl * 256;
                p->image_data_enh[p->image_offset++] = gl;

                p->image_data_raw[p->image_offset]   = gl * 256;
                p->image_data_enh[p->image_offset++] = gl;

                p->image_data_raw[p->image_offset]   = gl * 256;
                p->image_data_enh[p->image_offset++] = gl;

                if (++p->image_x >= p->image_width)
                {
                  if (preview_increment_image_y(p) < 0)
                  {
                    return TRUE;
                  }
                  break;	/* skip padding bits */
                }
              }
            }
           break;

          case 8:
            for (i = 0; i < len; ++i)
            {
             u_char gray = imagebuf8[i];

              if (preview_test_image_y(p))
              {
                return TRUE; /* backend sends too much image data */
              }

              p->image_data_raw[p->image_offset]   = gray * 256;
              p->image_data_enh[p->image_offset++] = gray;

              p->image_data_raw[p->image_offset]   = gray * 256;
              p->image_data_enh[p->image_offset++] = gray;

              p->image_data_raw[p->image_offset]   = gray * 256;
              p->image_data_enh[p->image_offset++] = gray;
              if (++p->image_x >= p->image_width && preview_increment_image_y(p) < 0)
	      {
                return TRUE;
              }
            }
           break;

          case 16:
            for (i = 0; i < len/2; ++i)
            {
             u_char gray = (u_char) (imagebuf16[i]/((guint16) 256));

              if (preview_test_image_y(p))
              {
                return TRUE; /* backend sends too much image data */
              }

              p->image_data_raw[p->image_offset]   = imagebuf16[i];
              p->image_data_enh[p->image_offset++] = gray;

              p->image_data_raw[p->image_offset]   = imagebuf16[i];
              p->image_data_enh[p->image_offset++] = gray;

              p->image_data_raw[p->image_offset]   = imagebuf16[i];
              p->image_data_enh[p->image_offset++] = gray;

              if (++p->image_x >= p->image_width && preview_increment_image_y(p) < 0)
	      {
                return TRUE;
              }
            }
           break;

          default:
            snprintf(buf, sizeof(buf), "%s %d.", ERR_PREVIEW_BAD_DEPTH, p->params.depth);
            preview_scan_done(p, 0);
            xsane_back_gtk_error(buf, TRUE);
           return TRUE;
        }
       break;

          case SANE_FRAME_RED:
          case SANE_FRAME_GREEN:
          case SANE_FRAME_BLUE:
            switch (p->params.depth)
            {
              case 1:
                for (i = 0; i < len; ++i)
                {
                 u_char mask = imagebuf8[i];

                  if (preview_test_image_y(p))
                  {
                    return TRUE; /* backend sends too much image data */
                  }

                  for (j = 0; j < 8; ++j)
                  {
                    u_char gl = (mask & 1) ? 0xff : 0x00;
                    mask >>= 1;

                    p->image_data_raw[p->image_offset] = gl * 256;
                    p->image_data_enh[p->image_offset] = gl;

                    p->image_offset += 3;
                    if (++p->image_x >= p->image_width && preview_increment_image_y(p) < 0)
                    {
                      return TRUE;
                    }
                  }
                }
               break;

              case 8:
                for (i = 0; i < len; ++i)
                {
                  if (preview_test_image_y(p))
                  {
                    return TRUE; /* backend sends too much image data */
                  }

                  p->image_data_raw[p->image_offset] = imagebuf8[i] * 256;
                  p->image_data_enh[p->image_offset] = imagebuf8[i];

                  p->image_offset += 3;
                  if (++p->image_x >= p->image_width && preview_increment_image_y(p) < 0)
                  {
                    return TRUE;
                  }
                }
               break;

              case 16:
                for (i = 0; i < len/2; ++i)
                {
                  if (preview_test_image_y(p))
                  {
                    return TRUE; /* backend sends too much image data */
                  }

                  p->image_data_raw[p->image_offset] = imagebuf16[i];
                  p->image_data_enh[p->image_offset] = (u_char) (imagebuf16[i]/256);

                  p->image_offset += 3;
                  if (++p->image_x >= p->image_width && preview_increment_image_y(p) < 0)
                  {
                    return TRUE;
                  }
                }
               break;

              default:
                snprintf(buf, sizeof(buf), "%s %d.", ERR_PREVIEW_BAD_DEPTH, p->params.depth);
                preview_scan_done(p, 0);
                xsane_back_gtk_error(buf, TRUE);
               return TRUE;
            }
           break;

          default:
            snprintf(buf, sizeof(buf), "%s %d.", ERR_BAD_FRAME_FORMAT, p->params.format);
            preview_scan_done(p, 0);
            xsane_back_gtk_error(buf, TRUE);
           return TRUE;
    }

    if (!p->input_active)
    {
      preview_display_maybe(p);
      while (gtk_events_pending())
      {
        DBG(DBG_info, "preview_read_image_data: calling gtk_main_iteration\n");
        gtk_main_iteration();
      }
    }
  }
  preview_display_maybe(p);

 return TRUE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_scan_done(Preview *p, int save_image)
{
 int i;

  DBG(DBG_proc, "preview_scan_done\n");

  p->scanning = FALSE;

  if (p->input_active)
  {
    g_source_remove(p->event_source_id);
    p->input_active = FALSE;
  }

  sane_cancel(xsane.dev);

  xsane.block_update_param = TRUE; /* do not change parameters each time */

  preview_restore_option(p, xsane.well_known.dpi,   &p->saved_dpi,   p->saved_dpi_valid);
  preview_restore_option(p, xsane.well_known.dpi_x, &p->saved_dpi_x, p->saved_dpi_x_valid);
  preview_restore_option(p, xsane.well_known.dpi_y, &p->saved_dpi_y, p->saved_dpi_y_valid);

  for (i = 0; i < 4; ++i)
  {
    preview_restore_option(p, xsane.well_known.coord[i], &p->saved_coord[i], p->saved_coord_valid[i]);
  }

  preview_restore_option(p, xsane.well_known.scanmode, &p->saved_scanmode, p->saved_scanmode_valid);

  preview_restore_option(p, xsane.well_known.bit_depth, &p->saved_bit_depth, p->saved_bit_depth_valid);

  preview_set_option_val(p, xsane.well_known.preview, SANE_FALSE);

  gtk_widget_set_sensitive(p->cancel, FALSE);
  xsane_set_sensitivity(TRUE);

  xsane.block_update_param = FALSE;

  preview_update_selection(p);

  if (save_image)
  {
    preview_save_image(p);    /* save preview image */
    preview_display_image(p);
  }

  preview_update_surface(p, 1); /* if surface was not defined it's necessary to redefine it now */

  xsane_update_histogram(TRUE /* update raw */);

  sane_get_parameters(xsane.dev, &xsane.param); /* update xsane.param */

  if ((preferences.preselect_scan_area) && (!p->startimage) && (!xsane.medium_calibration))
  {
    preview_autoselect_scan_area(p, p->selection.coordinate); /* get autoselection coordinates */
    preview_draw_selection(p); 
    preview_establish_selection(p); 
    xsane_update_histogram(TRUE /* update_raw */); /* update histogram (necessary because overwritten by preview_update_surface) */
  }

  if ((preferences.auto_correct_colors) && (!xsane.medium_calibration) && (!xsane.enable_color_management))
  {
    xsane_calculate_raw_histogram();
    xsane_set_auto_enhancement();
    xsane_enhancement_by_histogram(preferences.auto_enhance_gamma);
  }

  xsane_batch_scan_update_icon_list();
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static int preview_get_memory(Preview *p)
{
 char buf[TEXTBUFSIZE];

  DBG(DBG_proc, "preview_get_memory\n");

  if (p->image_data_enh)
  {
    free(p->image_data_enh);
    p->image_data_enh = 0;
  }

  if (p->image_data_raw)
  {
    free(p->image_data_raw);
    p->image_data_raw = 0;
  }

  if (p->preview_row)
  {
    free(p->preview_row);
    p->preview_row = 0;
  }

  p->image_data_raw = malloc((size_t) (6 * p->image_width * p->image_height));
  p->image_data_enh = malloc((size_t) (3 * p->image_width * p->image_height));
  p->preview_row    = malloc((size_t) (3 * p->preview_window_width));

  if ( (!p->image_data_raw) || (!p->image_data_enh) || (!p->preview_row) )
  {
    if (p->image_data_enh)
    {
      free(p->image_data_enh);
      p->image_data_enh = 0;
    }

    if (p->image_data_raw)
    {
      free(p->image_data_raw);
      p->image_data_raw = 0;
    }

    if (p->preview_row)
    {
      free(p->preview_row);
      p->preview_row = 0;
    }

    DBG(DBG_error, "failed to allocate image buffer: %s", strerror(errno));
    snprintf(buf, sizeof(buf), "%s %s.", ERR_FAILED_ALLOCATE_IMAGE, strerror(errno));
    xsane_back_gtk_error(buf, TRUE);

    return -1; /* error */
  }

  memset(p->image_data_raw, 0x80, (size_t) (6*p->image_width*p->image_height)); /* clean memory */
  memset(p->image_data_enh, 0x80, (size_t) (3*p->image_width*p->image_height)); /* clean memory */

  return 0; /* ok */
}

/* ---------------------------------------------------------------------------------------------------------------------- */

/* preview_scan_start is called 3 times in 3 pass color scanning mode */
static void preview_scan_start(Preview *p)
{
 SANE_Handle dev = xsane.dev;
 SANE_Status status;
 char buf[TEXTBUFSIZE];
 int fd, y;

  DBG(DBG_proc, "preview_scan_start\n");

  p->read_offset_16 = 0;

  xsane.medium_changed = FALSE;

  preview_display_valid(p);

  p->startimage = 0; /* we start the scan so lets say the startimage is not displayed any more */

  gtk_widget_set_sensitive(p->cancel, TRUE);
  xsane_set_sensitivity(FALSE);

  const guint win_image_width = (guint) geom_get_width_of_surface(p->win_image_surface);

  /* clear preview row */
  memset(p->preview_row, 0xff, 3 * win_image_width);

  for (y = (int) p->win_image_surface[1]; y < p->win_image_surface[3]; ++y)
  {
    gtk_preview_draw_row(GTK_PREVIEW(p->window), p->preview_row, (gint) p->win_image_surface[0], y, (gint) win_image_width);
  }

  if (p->input_active)
  {
    g_source_remove(p->event_source_id);
    p->input_active = FALSE;
  }

  status = sane_start(dev);

  if (status != SANE_STATUS_GOOD)
  {
    preview_scan_done(p, 0);
    snprintf(buf, sizeof(buf), "%s %s.", ERR_FAILED_START_SCANNER, XSANE_STRSTATUS(status));
    xsane_back_gtk_error(buf, TRUE);
   return;
  }

  status = sane_get_parameters(dev, &p->params);
  if (status != SANE_STATUS_GOOD)
  {
    preview_scan_done(p, 0);
    snprintf(buf, sizeof(buf), "%s %s.", ERR_FAILED_GET_PARAMS, XSANE_STRSTATUS(status));
    xsane_back_gtk_error(buf, TRUE);
   return;
  }

  p->image_offset = p->image_x = p->image_y = 0;

  if (p->params.format >= SANE_FRAME_RED && p->params.format <= SANE_FRAME_BLUE)
  {
    p->image_offset = (int) (p->params.format - SANE_FRAME_RED);
  }

  if ( (!p->image_data_enh)  || (p->params.pixels_per_line != p->image_width)
      || ( (p->params.lines >= 0) && (p->params.lines != p->image_height) ) )
  {
    p->image_width  = p->params.pixels_per_line;
    p->image_height = p->params.lines;

    if (p->params.lines < 0)
    {
      p->image_height = 32;	/* may have to adjust as we go... */
    }

    if (preview_get_memory(p))
    {
      preview_scan_done(p, 0); /* error */
      snprintf(buf, sizeof(buf), "%s", ERR_NO_MEM);
      xsane_back_gtk_error(buf, TRUE);
     return;
    }
  }
  else if (p->scanning == FALSE) /* single pass scan or first run in 3 pass mode */
  {
    memset(p->image_data_raw, 0x80, (size_t) (6*p->image_width*p->image_height)); /* clean memory */
    memset(p->image_data_enh, 0x80, (size_t) (3*p->image_width*p->image_height)); /* clean memory */
  }

  /* we do not have any active selection (image is redrawn while scanning) */
  p->selection.active = FALSE;
  p->previous_selection_maximum.active = FALSE;

#ifndef BUGGY_GDK_INPUT_EXCEPTION
  if ((sane_set_io_mode(dev, SANE_TRUE) == SANE_STATUS_GOOD) && (sane_get_select_fd(dev, &fd) == SANE_STATUS_GOOD))
  {
    GIOChannel *channel  = g_io_channel_unix_new(fd);
    p->input_active = TRUE;
    p->event_source_id = g_io_add_watch(channel, G_IO_IN | G_IO_HUP | G_IO_ERR | G_IO_PRI, preview_read_image_data, p);
    g_io_channel_unref(channel);
  }
  else
#endif
  {
    preview_read_image_data(NULL, G_IO_IN, p);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static int preview_make_image_path(Preview *p, size_t filename_size, char *filename, int level)
{
 char buf[TEXTBUFSIZE];

  DBG(DBG_proc, "preview_make_image_path\n");

  snprintf(buf, sizeof(buf), "xsane-preview-level-%d-", level);
  return xsane_back_gtk_make_path(filename_size, filename, 0, 0, buf, xsane.dev_name, ".ppm", XSANE_PATH_TMP);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

int preview_create_batch_icon_from_file(const Preview *p, FILE *in, const Batch_Scan_Parameters *parameters,
                                        int min_quality, int *min_time)
{
 u_int psurface_type, psurface_unit;
 guint image_width, image_height;
 int xoffset, yoffset;
 guint width, height;
 int max_val;
 int x, y, dx, dy;
 int time;
 double psurface[4];
 char buf[TEXTBUFSIZE];
 double scale;
 long int header = 0;
 guint rotate16 = 16 - preview_gamma_input_bits;
 guint rotate8 = preview_gamma_input_bits - 8;
 guint16 r,g,b;
 int maximum_size;
 int quality = 0;
 int xx, yy;
 int offset = 0;
 guchar *data;

  DBG(DBG_proc, "preview_create_batch_icon_from_file\n");

  if (!in)
  {
    return min_quality;
  }

  /* See whether there is a saved preview and load it if present: */
  if (fscanf(in, "P6\n"
                 "# surface: %lg %lg %lg %lg %u %u\n"
                 "# time: %d\n"
                 "%d %d\n%d",
	      psurface + 0, psurface + 1, psurface + 2, psurface + 3,
	      &psurface_type, &psurface_unit,
              &time,
	      &image_width, &image_height,
              &max_val) != 10)
  {
    DBG(DBG_info, "no preview image\n");
    return min_quality;
  }

  if (!fgets(buf, sizeof(buf), in)) /* skip newline character. this made a lot of problems in the past, so I skip it this way */
  {
    DBG(DBG_info, "preview image corrupt\n");
    return min_quality;
  }

  header = ftell(in);

  if (min_quality >= 0) /* read real preview */
  {
    if ((psurface_type != p->surface_type) || (psurface_unit != p->surface_unit))
    {
      DBG(DBG_info, "incompatible surface types %d <> %d\n", psurface_type, p->surface_type);
     return min_quality;
    }

    DBG(DBG_info, "stored image surface = [%3.2f %3.2f %3.2f %3.2f]\n",
                   psurface[0], psurface[1], psurface[2], psurface[3]);
    DBG(DBG_info, "batch selection = [%3.2f %3.2f %3.2f %3.2f]\n",
                   parameters->tl_x, parameters->tl_y, parameters->br_x, parameters->br_y);

    xoffset = (int) ((parameters->tl_x - psurface[0])/(psurface[2] - psurface[0]) * image_width);
    yoffset = (int) ((parameters->tl_y - psurface[1])/(psurface[3] - psurface[1]) * image_height);
    width   = (guint) ((parameters->br_x - parameters->tl_x)/(psurface[2] - psurface[0]) * image_width);
    height  = (guint) ((parameters->br_y - parameters->tl_y)/(psurface[3] - psurface[1]) * image_height);

    quality = (gint) width;

    if ((xoffset < 0) || (yoffset < 0) ||
        ((guint) (xoffset+ (int) width) > image_width) || ((guint) (yoffset+ (int) height) > image_height) ||
        (width == 0) || (height == 0))
    {
      DBG(DBG_info, "image does not cover wanted surface part\n");
     return min_quality;
    }

    DBG(DBG_info, "quality = %d\n", quality);

    if ( ((double) min_quality / (double) (quality+1)) > 1.05) /* already loaded image has better quality */
    {
      DBG(DBG_info, "already loaded image has higher quality\n");
     return min_quality;
    }

    if ( ((double) min_quality / (double) (quality+1)) > 0.95) /* qualities are comparable */
    {
      if (*min_time > time) /* take more recent scan */
      {
        DBG(DBG_info, "images have comparable quality, already loaded is more up to date\n");
       return min_quality;
      }
      DBG(DBG_info, "images have comparable quality, this image is more up to date\n");
    }
    else
    {
      DBG(DBG_info, "image has best quality\n");
    }
  }
  else
  {
    xoffset = 0;
    yoffset = 0;
    width   = image_width;
    height  = image_height;
  }


  {
    double xscale = (double) width / (double) parameters->gtk_preview_size;
    double yscale = (double) height / (double) parameters->gtk_preview_size;

    if (xscale > yscale)
    {
      scale = xscale;
    }
    else
    {
      scale = yscale;
    }
  }

  width  = (guint) ((double) width / scale);
  height = (guint) ((double) height / scale);

  if (width > parameters->gtk_preview_size)
  {
    width = parameters->gtk_preview_size;
  }

  if (height > parameters->gtk_preview_size)
  {
    height = parameters->gtk_preview_size;
  }

  maximum_size = (int) parameters->gtk_preview_size -1;

  dx = ((int) parameters->gtk_preview_size - (int) width) / 2;
  dy = ((int) parameters->gtk_preview_size - (int) height) / 2;


  data = malloc(parameters->gtk_preview_size * parameters->gtk_preview_size * 3);
  if (!data)
  {
    DBG(DBG_error, "preview_create_batch_icon_from_file: out of memory\n");
   return min_quality;
  }

  /* make unused parts white */
  for (x = 0; x< parameters->gtk_preview_size * parameters->gtk_preview_size * 3; x++)
  {
    data[x] = 0xF0;
  }

  if (max_val == 65535)
  {
    for (y=0; y < height; y++)
    { 
      for (x=0; x < width; x++)
      {
        fseek(in, header + ((xoffset + (int) ((double) x * scale) + (yoffset + (int) ((double) y * scale))) * (int) image_width) * 6, SEEK_SET);

        /* read 16 bit value in machine's byte order */
        if (!fread(&r, 2, 1, in) || !fread(&g, 2, 1, in) || !fread(&b, 2, 1, in))
        {
          free(data);
          DBG(DBG_info, "preview image truncated\n");
          return min_quality;
        }

        r = preview_gamma_data_red[r >> rotate16];
        g = preview_gamma_data_green[g >> rotate16];
        b = preview_gamma_data_blue[b >> rotate16];

        switch (parameters->rotation)
        {
          case 0: /* 0 degree */
            xx = x + dx;
            yy = y + dy;
            offset = (int) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 1: /* 90 degree */
            xx = maximum_size - y;
            yy = x + dx;
            offset = (int) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 2: /* 180 degree */
            xx = maximum_size - x - dx;
            yy = maximum_size - y - dy;
            offset = (int) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 3: /* 270 degree */
            xx = y + dy;
            yy = maximum_size - x - dx;
            offset = (int) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 4: /* 0 degree, x-mirror */
            xx = maximum_size - x - dx;
            yy = y + dy;
            offset = (int) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 5: /* 90 degree, x-mirror */
            xx = y + dy;
            yy = x + dx;
            offset = (int) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 6: /* 180 degree, x-mirror */
            xx = x + dx;
            yy = maximum_size - y - dy;
            offset = (int) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 7: /* 270 degree, x-mirror */
            xx = maximum_size - y - dy;
            yy = maximum_size - x - dx;
            offset = (int) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;
        }

        data[offset + 0] = (guchar) r;
        data[offset + 1] = (guchar) g;
        data[offset + 2] = (guchar) b;
      }
    }

    for (y = 0; y < parameters->gtk_preview_size; y++)
    {
      gtk_preview_draw_row(GTK_PREVIEW(parameters->gtk_preview), data + 3 * parameters->gtk_preview_size * (guint) y,
                           0, y, (gint) parameters->gtk_preview_size);
    }
  }
  else /* depth = 8 */
  {
    for (y=0; y < height; y++)
    { 
      for (x=0; x < width; x++)
      {
        fseek(in, header + (xoffset + (int) ((double) x * scale) + (yoffset + (int) ((double) y * scale)) * (gint) image_width) * 3, SEEK_SET);

        r = (guint16) fgetc(in);
        r = preview_gamma_data_red[r << rotate8];

        g = (guint16) fgetc(in);
        g = preview_gamma_data_green[g << rotate8];

        b = (guint16) fgetc(in);
        b = preview_gamma_data_blue[b << rotate8];
        switch (parameters->rotation)
        {
          case 0: /* 0 degree */
            xx = x + dx;
            yy = y + dy;
            offset = (gint) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 1: /* 90 degree */
            xx = maximum_size - y;
            yy = x + dx;
            offset = (gint) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 2: /* 180 degree */
            xx = maximum_size - x - dx;
            yy = maximum_size - y - dy;
            offset = (gint) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 3: /* 270 degree */
            xx = y + dy;
            yy = maximum_size - x - dx;
            offset = (gint) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 4: /* 0 degree, x-mirror */
            xx = maximum_size - x - dx;
            yy = y + dy;
            offset = (gint) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 5: /* 90 degree, x-mirror */
            xx = y + dy;
            yy = x + dx;
            offset = (gint) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 6: /* 180 degree, x-mirror */
            xx = x + dx;
            yy = maximum_size - y - dy;
            offset = (gint) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;

          case 7: /* 270 degree, x-mirror */
            xx = maximum_size - y - dy;
            yy = maximum_size - x - dx;
            offset = (gint) parameters->gtk_preview_size * 3 * yy + 3*(xx);
          break;
        }

        data[offset + 0] = (guchar) r;
        data[offset + 1] = (guchar) g;
        data[offset + 2] = (guchar) b;
      }
    }

    for (y = 0; y < parameters->gtk_preview_size; y++)
    {
      gtk_preview_draw_row(GTK_PREVIEW(parameters->gtk_preview), data + 3 * (gint) parameters->gtk_preview_size * y,
                           0, y, (gint) parameters->gtk_preview_size);
    }
  }

  free(data);

 return quality;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_create_batch_icon(const Preview *p, const Batch_Scan_Parameters *parameters)
{
 FILE *in = NULL;
 int quality = 0;
 int time = 0;
 int level;

  for(level = 2; level >= 0; level--)
  {
    if (p->filename[level])
    {
      in = fopen(p->filename[level], "rb"); /* read binary (b for win32) */
      if (in)
      {
        quality = preview_create_batch_icon_from_file(xsane.preview, in, parameters, quality, &time);
        fclose(in);
      }
    }
  }

  if (quality <= 0)
  {
   char filename[PATH_MAX];

    xsane_back_gtk_make_path(sizeof(filename), filename, "xsane", 0, "xsane-startimage", 0, ".pnm", XSANE_PATH_SYSTEM);
    in = fopen(filename, "rb"); /* read binary (b for win32) */
    if (in)
    {
      preview_create_batch_icon_from_file(xsane.preview, in, parameters, -1, &time);
      fclose(in);
    }
  }
}
/* ---------------------------------------------------------------------------------------------------------------------- */

static int preview_restore_image_from_file(Preview *p, FILE *in, int min_quality, int *min_time)
{
 char buf[TEXTBUFSIZE];

  DBG(DBG_proc, "preview_restore_image_from_file\n");

  if (!in)
  {
    DBG(DBG_info, "no file\n");
    return min_quality;
  }

  /* Try to find the image and load it . */
  u_int psurface_type, psurface_unit;
  int image_width, image_height;
  int max_val;
  double psurface[4];
  int time;
  if (fscanf(in, "P6\n"
                 "# surface: %lg %lg %lg %lg %u %u\n"
                 "# time: %d\n"
                 "%d %d\n%d",
	      psurface + 0, psurface + 1, psurface + 2, psurface + 3,
	      &psurface_type, &psurface_unit,
        &time,
	      &image_width, &image_height,
        &max_val) != 10)
  {
    DBG(DBG_info, "wrong header of the image\n");
    return min_quality;
  }

  /* Skip newline character. This made a lot of problems in the past, so I skip it this way. */
  if (!fgets(buf, sizeof(buf), in))
  {
    DBG(DBG_info, "preview image corrupt\n");
    return min_quality;
  }

  DBG(DBG_info, "stored image surface      = [%3.2f %3.2f %3.2f %3.2f]\n",
                 psurface[0], psurface[1], psurface[2], psurface[3]);
  DBG(DBG_info, "preview selection surface = [%3.2f %3.2f %3.2f %3.2f]\n",
                 p->surface[0], p->surface[1], p->surface[2], p->surface[3]);

  /* Calculate the required area of image. */
  int xoffset, yoffset, width, height;
  int quality = 0;
  if (min_quality >= 0)
  {
    /* Read real preview. */

    if ((psurface_type != p->surface_type) || (psurface_unit != p->surface_unit))
    {
      DBG(DBG_info, "incompatible surface types or units, [%d; %d] <> [%d; %d]\n",
          psurface_type, p->surface_type,
          psurface_unit, p->surface_unit);
      return min_quality;
    }

    // 'Pixels by unit' values.
    double scale_x = (double) image_width / geom_get_width_of_surface(psurface);
    double scale_y = (double) image_height / geom_get_height_of_surface(psurface);

    xoffset = (int) ((p->image_surface[0] - psurface[0]) * scale_x);
    yoffset = (int) ((p->image_surface[1] - psurface[1]) * scale_y);
    width   = (int) (geom_get_width_of_surface(p->image_surface) * scale_x);
    height  = (int) (geom_get_height_of_surface(p->image_surface) * scale_y);

    // Border condition: the view window is less that one pixel.
    width = max(1, width);
    height = max(1, height);

    quality = width;

    DBG(DBG_info, "xoffset = %d, yoffset = %d, width = %d, height = %d, image_width = %d, image_height = %d\n",
        xoffset, yoffset, width, height,
        image_width, image_height);

    if ((xoffset < 0) || (yoffset < 0) ||
        (xoffset+width > image_width) || (yoffset+height > image_height))
    {
      DBG(DBG_info, "wrong area of image\n");
      return min_quality;
    }

    if ( ((double) min_quality / ((double) quality+1)) > 1.05) /* already loaded image has better quality */
    {
      DBG(DBG_info, "already loaded image has higher quality\n");
      return min_quality;
    }

    if ( ((double) min_quality / ((double) quality+1)) > 0.95) /* qualities are comparable */
    {
      if (*min_time > time) /* take more recent scan */
      {
        DBG(DBG_info, "images have comparable quality, already loaded is more up to date\n");
        return min_quality;
      }
      DBG(DBG_info, "images have comparable quality, this image is more up to date\n");
    }
    else
    {
      DBG(DBG_info, "image has best quality\n");
    }
  }
  else
  {
    /* Read start image or calibration image. */

    double scale_x = (double) image_width / geom_get_width_of_surface(p->orig_scanner_surface);
    double scale_y = (double) image_height / geom_get_height_of_surface(p->orig_scanner_surface);

    xoffset = (int) ((p->image_surface[0] - p->orig_scanner_surface[0]) * scale_x);
    yoffset = (int) ((p->image_surface[1] - p->orig_scanner_surface[1]) * scale_y);
    width   = (int) (geom_get_width_of_surface(p->image_surface) * scale_x);
    height  = (int) (geom_get_height_of_surface(p->image_surface) * scale_x);

    // Border condition: the view window is less that one pixel.
    width = max(1, width);
    height = max(1, height);

    quality = width;

    DBG(DBG_info, "xoffset = %d, yoffset = %d, width = %d, height = %d, image_width = %d, image_height = %d\n",
        xoffset, yoffset, width, height,
        image_width, image_height);
  }

  if (max_val == 65535)
  {
    p->params.depth = 16;
  }
  else
  {
    p->params.depth = 8;
  }

  p->image_width  = width;
  p->image_height = height;

  if (preview_get_memory(p))
  {
    return min_quality; /* error allocating memory */
  }

  /* Get the part of image. */
  guint16 *imagep;
  int x, y;
  if (p->params.depth == 16)
  {
    fseek(in, yoffset * image_width * 6, SEEK_CUR); /* skip unused lines */

    imagep = p->image_data_raw;

    for (y = yoffset; y < yoffset + height; y++)
    {
      fseek(in, xoffset * 6, SEEK_CUR); /* skip unused pixel left of area */

      if (!fread(imagep, 6, (size_t) width, in))
      {
        DBG(DBG_error, "preview image truncated\n");
        return min_quality;
      }

      imagep += width * 3; /* imagep is a pointer to a 2 byte value, so we use 3 instead 6 here */

      fseek(in, (image_width - width - xoffset) * 6, SEEK_CUR); /* skip unused pixel right of area */
    }
  }
  else /* depth = 8 */
  {
    fseek(in, yoffset * image_width * 3, SEEK_CUR); /* skip unused lines */

    imagep = p->image_data_raw;

    for (y = yoffset; y < yoffset + height; y++)
    {
      fseek(in, xoffset * 3, SEEK_CUR); /* skip unused pixel left of area */

      guint16 *imagepx = imagep;
      for (x = 0; x < width; x++)
      {
        *imagepx++ = ((guint16) fgetc(in)) * 256; /* transfrom to 16 bit image with correct byte order */
        *imagepx++ = ((guint16) fgetc(in)) * 256;
        *imagepx++ = ((guint16) fgetc(in)) * 256;
      }
      imagep += width * 3; /* imagep is a pointer to a 2 byte value, so we use 3 instead 6 here */

      fseek(in, (image_width - width - xoffset) * 3, SEEK_CUR); /* skip unused pixel right of area */
    }
  }

  p->image_x = width;
  p->image_y = height;

  *min_time = time;

 return quality;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_restore_image(Preview *p)
{
 FILE *in;
 int quality = 0;
 int time = 0;
 int level;

  DBG(DBG_proc, "preview_restore_image\n");

  p->startimage = 0;

  if (p->calibration)
  {
   char filename[PATH_MAX];

    DBG(DBG_proc, "calibration mode\n");
    xsane_back_gtk_make_path(sizeof(filename), filename, "xsane", 0, "xsane-calibration", 0, ".pnm", XSANE_PATH_SYSTEM);
    in = fopen(filename, "rb"); /* read binary (b for win32) */
    if (in)
    {
      /* TODO: quality is not used. Is this correct or is there a bracing issue in this function
       * such that quality is not checked further down?
       * I'm not sure what this function actually does. [RL]
       */
      /*quality = */ (void)preview_restore_image_from_file(p, in, -1, &time);
      fclose(in);
    }
  }
  else
  {
    /* See whether there is a saved preview and load it if present: */
    for(level = 2; level >= 0; level--)
    {
      if (p->filename[level])
      {
        in = fopen(p->filename[level], "rb"); /* read binary (b for win32) */
        if (in)
        {
          quality = preview_restore_image_from_file(p, in, quality, &time);
          fclose(in);
        }
      }
    }

    if (quality == 0) /* no image found, read startimage */
    {
     char filename[PATH_MAX];

      DBG(DBG_proc, "no suitable image available, using startimage\n");
      xsane_back_gtk_make_path(sizeof(filename), filename, "xsane", 0, "xsane-startimage", 0, ".pnm", XSANE_PATH_SYSTEM);
      in = fopen(filename, "rb"); /* read binary (b for win32) */
      if (in)
      {
        /* TODO: quality is not used. Is this correct or is there a bracing issue in this function
         * such that quality is not checked further down?
         * I'm not sure what this function actually does. [RL]
         */
        /*quality =*/ (void)preview_restore_image_from_file(p, in, -1, &time);
        fclose(in);
      }
      else
      {
       guint16 *imagep;

        DBG(DBG_error0, "ERROR: xsane-startimage not found. Looks like xsane is not installed correct.\n");

        p->image_width  = 1;
        p->image_height = 1;
        p->params.depth = 16;

        if (preview_get_memory(p) == 0)
        {
          imagep = p->image_data_raw;
          *imagep++ = 65535;
          *imagep++ = 00000;
          *imagep++ = 00000;

          p->image_x = p->image_width;
          p->image_y = p->image_height;
        }
        else
        {
          DBG(DBG_error0, "Failed to get preview image memory.\n");
        }
      }
      p->startimage = 1;
    }
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static int preview_get_pixel_color(const Preview *p, int x, int y, int *raw_red, int *raw_green, int *raw_blue,
                                   int *enh_red, int *enh_green, int *enh_blue)
{
 int offset;
 guint rotate = 16 - preview_gamma_input_bits;

  DBG(DBG_proc, "preview_get_pixel_color\n");
 
  if (p->image_data_raw)
  {
    double point_coord[2], image_coord[2];
    point_coord[0] = (double) x;
    point_coord[1] = (double) y;

    preview_transform_coordinate_window_to_image(p, point_coord, image_coord);
    int image_x = (int) image_coord[0];
    int image_y = (int) image_coord[1];

    if ( (image_x >= 0) && (image_x < p->image_width) && (image_y >=0) && (image_y < p->image_height) )
    {
      offset = 3 * (image_y * p->image_width + image_x);

      if (!xsane.negative) /* positive */
      {
        *raw_red   = (p->image_data_raw[offset    ]) >> 8;
        *raw_green = (p->image_data_raw[offset + 1]) >> 8;
        *raw_blue  = (p->image_data_raw[offset + 2]) >> 8;
      }
      else /* negative */
      {
        *raw_red   = 255 - ((p->image_data_raw[offset    ]) >> 8);
        *raw_green = 255 - ((p->image_data_raw[offset + 1]) >> 8);
        *raw_blue  = 255 - ((p->image_data_raw[offset + 2]) >> 8);
      }

      /* the enhanced pixels are already inverted when negative is selected */ 
      /* do not use image_data_enh because the preview gamma value is applied to this */
      *enh_red   = histogram_gamma_data_red  [(p->image_data_raw[offset    ]) >> rotate];
      *enh_green = histogram_gamma_data_green[(p->image_data_raw[offset + 1]) >> rotate];
      *enh_blue  = histogram_gamma_data_blue [(p->image_data_raw[offset + 2]) >> rotate];

     return 0;
    }
  }

 return -1;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_display_zoom(const Preview *p, int x, int y)
{
  DBG(DBG_proc, "preview_display_zoom\n");

  if (!p->image_data_raw)
  {
    DBG(DBG_proc, "no data\n");
    return;
  }

  u_char *row = calloc(XSANE_ZOOM_SIZE, 3);
  if (!p->image_data_raw)
  {
    DBG(DBG_error, "preview_display_zoom: no memory\n");
    return;
  }

  /* Calculate the basis for moving around in the image. */
  int img_x_axis[2], img_y_axis[2];
  {
    double zero_point[2];
    double rotated_point[2];
    double rotated_vector[2];

    geom_rotate_back_point(p, point_0_0, zero_point);

    geom_rotate_back_point(p, point_1_0, rotated_point);
    geom_subtract_points(rotated_point, zero_point, rotated_vector);
    img_x_axis[0] = (int) rotated_vector[0];
    img_x_axis[1] = (int) rotated_vector[1];

    geom_rotate_back_point(p, point_0_1, rotated_point);
    geom_subtract_points(rotated_point, zero_point, rotated_vector);
    img_y_axis[0] = (int) rotated_vector[0];
    img_y_axis[1] = (int) rotated_vector[1];
  }

  /* Find the top left point of zoomed preview in the image. */
  int img_top_left[2];
  {
    double point_coord[2], image_coord[2];
    point_coord[0] = (double) x;
    point_coord[1] = (double) y;
    preview_transform_coordinate_window_to_image(p, point_coord, image_coord);

    const int shift = XSANE_ZOOM_SIZE / 2 / XSANE_ZOOM_FACTOR;
    img_top_left[0] = ((int)image_coord[0]) - (img_x_axis[0] + img_y_axis[0]) * shift;
    img_top_left[1] = ((int)image_coord[1]) - (img_x_axis[1] + img_y_axis[1]) * shift;
  }

  /* Default color for pixels outside the image. */
  const u_char default_color[3] = { 0x80, 0x80, 0x80 };

  /* Draw it. */
  const u_char* current_pixel;
  for(y=0; y<XSANE_ZOOM_SIZE; y++)
  {
    int row_offset = 0;

    for(x=0; x<XSANE_ZOOM_SIZE; x++)
    {
      if(!(x % XSANE_ZOOM_FACTOR))
      {
        /* It is a new point of image. */
        int img_point[2];
        img_point[0] = img_top_left[0] + img_x_axis[0] * (x / XSANE_ZOOM_FACTOR) + img_y_axis[0] * (y / XSANE_ZOOM_FACTOR);
        img_point[1] = img_top_left[1] + img_x_axis[1] * (x / XSANE_ZOOM_FACTOR) + img_y_axis[1] * (y / XSANE_ZOOM_FACTOR);

        if(img_point[0] >= 0 && img_point[0] < p->image_width &&
           img_point[1] >= 0 && img_point[1] < p->image_height)
        {
          current_pixel = p->image_data_enh + (p->image_width * img_point[1] + img_point[0]) * 3;
        }
        else
        {
          current_pixel = default_color;
        }
      }

      row[row_offset++] = current_pixel[0];
      row[row_offset++] = current_pixel[1];
      row[row_offset++] = current_pixel[2];
    }

    gtk_preview_draw_row(GTK_PREVIEW(p->zoom), row, 0, y, XSANE_ZOOM_SIZE);
  }

  gtk_widget_queue_draw(p->zoom);

  free(row);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_display_color_components(const Preview *p, int x, int y)
{
 char buffer[TEXTBUFSIZE];
 int raw_red, raw_green, raw_blue, enh_red, enh_green, enh_blue;

  if (! preview_get_pixel_color(p, x, y, &raw_red, &raw_green, &raw_blue, &enh_red, &enh_green, &enh_blue))
  {
    snprintf(buffer, sizeof(buffer), " %03d, %03d, %03d \n" \
                                     " %03d, %03d, %03d ",
    raw_red, raw_green, raw_blue, enh_red, enh_green, enh_blue);
  }
  else
  {
    snprintf(buffer, sizeof(buffer), " ###, ###, ### \n" \
                                     " ###, ###, ### ");
  }

  gtk_label_set_text(GTK_LABEL(p->rgb_label), buffer);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gboolean preview_hold_event_handler(gpointer data)
{
 Preview *p = data;

  DBG(DBG_proc, "preview_hold_event_handler\n");

  preview_draw_selection(p);
  p->gamma_functions_interruptable = TRUE;
  preview_establish_selection(p);
  p->gamma_functions_interruptable = FALSE;

  g_source_remove(p->hold_timer);
  p->hold_timer = 0;

 return FALSE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_motion_event_handler(GtkWidget *window, GdkEvent *event, gpointer data)
{
 Preview *p = data;

  DBG(DBG_proc, "preview_motion_event_handler\n");

  if(p->scanning)
  {
    DBG(DBG_proc, "scanning now\n");
    return FALSE;
  }

  /* cursor-position (window) -> preview coordinate (device) */
  double preview_point[2];
  preview_transform_coordinate_window_to_device(p, (double) event->button.x, (double) event->button.y, preview_point);

  preview_display_zoom(p, (int) event->motion.x, (int) event->motion.y);
  preview_display_color_components(p, (int) event->motion.x, (int) event->motion.y);

  switch (((GdkEventMotion *)event)->state & (GDK_BUTTON1_MASK | GDK_BUTTON2_MASK | GDK_BUTTON3_MASK))
  {
    case GDK_BUTTON1_MASK: /* left button */

      DBG(DBG_info2, "left button\n");

      if ( (p->selection_drag) || (p->selection_drag_edge) )
      {
        p->selection.active = TRUE;

        geom_bound_point_to_surface(preview_point, p->scanner_surface);

        /* User is making a new selection. */
        if (p->selection_xedge != -1)
        {
          p->selection.coordinate[p->selection_xedge] = preview_point[0];
        }
        if (p->selection_yedge != -1)
        {
          p->selection.coordinate[p->selection_yedge] = preview_point[1];
        }

        if (p->ratio) /* forced preview ratio ? */
        {
          if ( (p->selection_xedge == 0) && (p->selection_yedge == 0) ) /* top left corner */
          {
            const double width = geom_get_width_of_surface(p->selection.coordinate);

            p->selection.coordinate[3] = p->selection.coordinate[1] + width / p->ratio;

            if (p->selection.coordinate[3] > p->scanner_surface[3])
            {
              p->selection.coordinate[3] = p->scanner_surface[3];

              const double height = geom_get_width_of_surface(p->selection.coordinate);
              p->selection.coordinate[0] = p->selection.coordinate[2] - height * p->ratio;
            }
          }
          else if ( (p->selection_xedge == 3) && (p->selection_yedge == 1) )/* top right corner */
          {
            const double width = geom_get_width_of_surface(p->selection.coordinate);

            p->selection.coordinate[3] = p->selection.coordinate[1] + width / p->ratio;

            if (p->selection.coordinate[3] > p->scanner_surface[3])
            {
              p->selection.coordinate[3] = p->scanner_surface[3];

              const double height = geom_get_width_of_surface(p->selection.coordinate);
              p->selection.coordinate[2] = p->selection.coordinate[0] + height * p->ratio;
            }
          }
          else if ( (p->selection_xedge == 0) && (p->selection_yedge == 3) ) /* bottom left edge */
          {
            const double width = geom_get_width_of_surface(p->selection.coordinate);

            p->selection.coordinate[1] = p->selection.coordinate[3] - width / p->ratio;

            if (p->selection.coordinate[1] <  p->scanner_surface[1])
            {
              p->selection.coordinate[1] = p->scanner_surface[1];

              const double height = geom_get_width_of_surface(p->selection.coordinate);
              p->selection.coordinate[0] = p->selection.coordinate[2] - height * p->ratio;
            }
          }
          else if ( (p->selection_xedge == 2) && (p->selection_yedge == 3) ) /* bottom right edge */
          {
            const double width = geom_get_width_of_surface(p->selection.coordinate);

            p->selection.coordinate[1] = p->selection.coordinate[3] - width / p->ratio;

            if (p->selection.coordinate[1] <  p->scanner_surface[1])
            {
              p->selection.coordinate[1] = p->scanner_surface[1];

              const double height = geom_get_width_of_surface(p->selection.coordinate);
              p->selection.coordinate[2] = p->selection.coordinate[0] + height * p->ratio;
            }
          }
          else if (p->selection_xedge == 0) /* left edge */
          {
            const double width = geom_get_width_of_surface(p->selection.coordinate);

            p->selection.coordinate[3] = p->selection.coordinate[1] + width / p->ratio;

            if (p->selection.coordinate[3] > p->scanner_surface[3])
            {
              p->selection.coordinate[3] = p->scanner_surface[3];

              const double height = geom_get_width_of_surface(p->selection.coordinate);
              p->selection.coordinate[0] = p->selection.coordinate[2] - height * p->ratio;
            }
          }
          else if (p->selection_xedge == 2) /* right edge */
          {
            const double width = geom_get_width_of_surface(p->selection.coordinate);

            p->selection.coordinate[3] = p->selection.coordinate[1] + width / p->ratio;

            if (p->selection.coordinate[3] > p->scanner_surface[3])
            {
              p->selection.coordinate[3] = p->scanner_surface[3];

              const double height = geom_get_width_of_surface(p->selection.coordinate);
              p->selection.coordinate[2] = p->selection.coordinate[0] + height * p->ratio;
            }
          }
          else if (p->selection_yedge == 1) /* top edge */
          {
            const double height = geom_get_width_of_surface(p->selection.coordinate);

            p->selection.coordinate[2] = p->selection.coordinate[0] + height * p->ratio;

            if (p->selection.coordinate[2] > p->scanner_surface[2])
            {
              p->selection.coordinate[2] = p->scanner_surface[2];

              const double width = geom_get_width_of_surface(p->selection.coordinate);
              p->selection.coordinate[1] = p->selection.coordinate[3] - width / p->ratio;
            }
          }
          else if (p->selection_yedge == 3) /* bottom edge */
          {
            const double height = geom_get_width_of_surface(p->selection.coordinate);

            p->selection.coordinate[2] = p->selection.coordinate[0] + height * p->ratio;

            if (p->selection.coordinate[2] > p->scanner_surface[2])
            {
              p->selection.coordinate[2] = p->scanner_surface[2];

              const double width = geom_get_width_of_surface(p->selection.coordinate);
              p->selection.coordinate[3] = p->selection.coordinate[1] + width / p->ratio;
            }
          }
        }

        preview_bound_selection(p);

        if (preferences.gtk_update_policy == GTK_UPDATE_CONTINUOUS)
        {
          if (!p->hold_timer) /* hold timer active? then remove it, we had a motion */
          {
            p->hold_timer = g_timeout_add(XSANE_CONTINUOUS_HOLD_TIME, preview_hold_event_handler, (gpointer) p);
          }
          preview_update_maximum_output_size(p);
          preview_draw_selection(p);
        }
        else if (preferences.gtk_update_policy == GTK_UPDATE_DELAYED)
        {
          /* call preview_hold_event_hanlder if mouse is not moved for ??? ms */
          if (p->hold_timer) /* hold timer active? then remove it, we had a motion */
          {
            g_source_remove(p->hold_timer);
            p->hold_timer = 0;
          }
          p->hold_timer = g_timeout_add(XSANE_HOLD_TIME, preview_hold_event_handler, (gpointer) p);
          preview_update_maximum_output_size(p);
          preview_draw_selection(p);
        }
        else /* discontinuous */
        {
          preview_update_maximum_output_size(p);
          preview_draw_selection(p); /* only draw selection, do not update backend geometry options */
        }
      }

      /* Select cursor image to show. */
      {
        double preview_selection[4];
        preview_transform_surface_device_to_window(p, p->selection.coordinate, preview_selection);

        int cursornr = p->cursornr;

        if ( (p->selection_xedge != -1) && (p->selection_yedge != -1) ) /* move corner */
        {
          if ( ( (preview_selection[0] - SELECTION_RANGE_OUT < event->button.x) &&
                 (event->button.x < preview_selection[0] + SELECTION_RANGE_IN) ) && /* left  */
               ( (preview_selection[1] - SELECTION_RANGE_OUT < event->button.y) &&
                 (event->button.y < preview_selection[1] + SELECTION_RANGE_IN) ) ) /* top */
          {
            cursornr = GDK_TOP_LEFT_CORNER;
          }
          else if ( ( (preview_selection[2] - SELECTION_RANGE_IN < event->button.x) &&
                      (event->button.x < preview_selection[2] + SELECTION_RANGE_OUT) ) && /* right */
                    ( (preview_selection[1] - SELECTION_RANGE_OUT < event->button.y) &&
                      (event->button.y < preview_selection[1] + SELECTION_RANGE_IN) ) ) /* top */
          {
            cursornr = GDK_TOP_RIGHT_CORNER;
          }
          else if ( ( (preview_selection[0] - SELECTION_RANGE_OUT < event->button.x) &&
                      (event->button.x < preview_selection[0] + SELECTION_RANGE_IN) ) && /* left  */
                    ( (preview_selection[3] - SELECTION_RANGE_IN < event->button.y) &&
                      (event->button.y < preview_selection[3] + SELECTION_RANGE_OUT) ) ) /* bottom */
          {
            cursornr = GDK_BOTTOM_LEFT_CORNER;
          }
          else if ( ( (preview_selection[2] - SELECTION_RANGE_IN < event->button.x) &&
                      (event->button.x < preview_selection[2] + SELECTION_RANGE_OUT) ) && /* right */
                    ( (preview_selection[3] - SELECTION_RANGE_IN < event->button.y) &&
                      (event->button.y < preview_selection[3] + SELECTION_RANGE_OUT) ) ) /* bottom */
          {
            cursornr = GDK_BOTTOM_RIGHT_CORNER;
          }
        }
        else if ( (preview_selection[0] - SELECTION_RANGE_OUT < event->button.x) &&
                  (event->button.x < preview_selection[0] + SELECTION_RANGE_IN) ) /* left */
        {
          if (cursornr == GDK_RIGHT_SIDE)
          {
            cursornr = GDK_LEFT_SIDE;
          }
        }
        else if ( (preview_selection[2] - SELECTION_RANGE_IN < event->button.x) &&
                  (event->button.x < preview_selection[2] + SELECTION_RANGE_OUT) )  /* right */
        {
          if (cursornr == GDK_LEFT_SIDE)
          {
            cursornr = GDK_RIGHT_SIDE;
          }
        }
        else if ( (preview_selection[1] - SELECTION_RANGE_OUT < event->button.y) &&
                  (event->button.y < preview_selection[1] + SELECTION_RANGE_IN) )  /* top */
        {
          if (cursornr == GDK_BOTTOM_SIDE)
          {
            cursornr = GDK_TOP_SIDE;
          }
        }
        else if ( (preview_selection[3] - SELECTION_RANGE_IN < event->button.y) &&
                    (event->button.y < preview_selection[3] + SELECTION_RANGE_OUT) )  /* bottom */
        {
          if (cursornr == GDK_TOP_SIDE)
          {
            cursornr = GDK_BOTTOM_SIDE;
          }
        }

        if (cursornr != p->cursornr)
        {
          GdkCursor *cursor = gdk_cursor_new(cursornr); /* set cursor */
          gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
          gdk_cursor_unref(cursor);
          p->cursornr = cursornr;
        }
      }
     break;

    case GDK_BUTTON2_MASK: /* middle button */
    case GDK_BUTTON3_MASK: /* right button */
      DBG(DBG_info2, "middle or right button\n");

      if (p->selection_drag)
      {
        /* Calculate the shift from the previous step. */
        double shift[2];
        {
          double new_selection_pos[2];
          preview_transform_coordinate_window_to_device(p, (double) event->motion.x, (double) event->motion.y, new_selection_pos);

          geom_subtract_points(new_selection_pos, p->selection_pos, shift);
          geom_set_point(new_selection_pos, p->selection_pos);

          if(p->selection.coordinate[0] + shift[0] < p->scanner_surface[0])
          {
            shift[0] = p->scanner_surface[0] - p->selection.coordinate[0];
          }
          else if(p->selection.coordinate[2] + shift[0] > p->scanner_surface[2])
          {
            shift[0] = p->scanner_surface[2] - p->selection.coordinate[2];
          }

          if(p->selection.coordinate[1] + shift[1] < p->scanner_surface[1])
          {
            shift[1] = p->scanner_surface[1] - p->selection.coordinate[1];
          }
          else if(p->selection.coordinate[3] + shift[1] > p->scanner_surface[3])
          {
            shift[1] = p->scanner_surface[3] - p->selection.coordinate[3];
          }
        }

        p->selection.active = TRUE;
        geom_shift_surface_by_point(p->selection.coordinate, shift);

        if (preferences.gtk_update_policy == GTK_UPDATE_CONTINUOUS)
        {
          if (!p->hold_timer) /* hold timer active? then remove it, we had a motion */
          {
            p->hold_timer = g_timeout_add(XSANE_CONTINUOUS_HOLD_TIME, preview_hold_event_handler, (gpointer) p);
          }
          preview_update_maximum_output_size(p);
          preview_draw_selection(p);
        }
        else if (preferences.gtk_update_policy == GTK_UPDATE_DELAYED)
        {
          if (p->hold_timer) /* hold timer active? then remove it, we had a motion */
          {
            g_source_remove(p->hold_timer);
            p->hold_timer = 0;
          }
          p->hold_timer = g_timeout_add (XSANE_HOLD_TIME, preview_hold_event_handler, (gpointer) p);
          preview_update_maximum_output_size(p);
          preview_draw_selection(p);
        }
        else /* discontinuous */
        {
          preview_update_maximum_output_size(p);
          preview_draw_selection(p);
        }
      }
     break;

    default:
      /* Select cursor if the current cursor is not blocked. */
      if(p->cursornr != -1)
      {
        double preview_selection[4];
        preview_transform_surface_device_to_window(p, p->selection.coordinate, preview_selection);
         int cursornr = p->cursornr;
         if ( ( (preview_selection[0] - SELECTION_RANGE_OUT < event->button.x) &&
                 (event->button.x < preview_selection[0] + SELECTION_RANGE_IN) ) && /* left  */
               ( (preview_selection[1] - SELECTION_RANGE_OUT < event->button.y) &&
                 (event->button.y < preview_selection[1] + SELECTION_RANGE_IN) ) ) /* top */
        {
          cursornr = GDK_TOP_LEFT_CORNER;
        }
        else if ( ( (preview_selection[2] - SELECTION_RANGE_IN < event->button.x) &&
                    (event->button.x < preview_selection[2] + SELECTION_RANGE_OUT) ) && /* right */
                  ( (preview_selection[1] - SELECTION_RANGE_OUT < event->button.y) &&
                    (event->button.y < preview_selection[1] + SELECTION_RANGE_IN) ) ) /* top */
        {
          cursornr = GDK_TOP_RIGHT_CORNER;
        }
        else if ( ( (preview_selection[0] - SELECTION_RANGE_OUT < event->button.x) &&
                    (event->button.x < preview_selection[0] + SELECTION_RANGE_IN) ) && /* left  */
                  ( (preview_selection[3] - SELECTION_RANGE_IN < event->button.y) &&
                    (event->button.y < preview_selection[3] + SELECTION_RANGE_OUT) ) ) /* bottom */
        {
          cursornr = GDK_BOTTOM_LEFT_CORNER;
        }
        else if ( ( (preview_selection[2] - SELECTION_RANGE_IN < event->button.x) &&
                    (event->button.x < preview_selection[2] + SELECTION_RANGE_OUT) ) && /* right */
                  ( (preview_selection[3] - SELECTION_RANGE_IN < event->button.y) &&
                    (event->button.y < preview_selection[3] + SELECTION_RANGE_OUT) ) ) /* bottom */
        {
          cursornr = GDK_BOTTOM_RIGHT_CORNER;
        }
        else if ( ( (preview_selection[0] - SELECTION_RANGE_OUT < event->button.x) &&
                    (event->button.x < preview_selection[0] + SELECTION_RANGE_IN) ) &&  /* left */
                  ( (event->button.y > preview_selection[1]) && (event->button.y < preview_selection[3]) ) ) /* in height */
        {
           cursornr = GDK_LEFT_SIDE;
        }
        else if ( ( (preview_selection[2] - SELECTION_RANGE_IN < event->button.x) &&
                    (event->button.x < preview_selection[2] + SELECTION_RANGE_OUT) ) &&  /* right */
                  ( (event->button.y > preview_selection[1]) && (event->button.y < preview_selection[3]) ) ) /* in height */
        {
           cursornr = GDK_RIGHT_SIDE;
        }
        else if ( ( (preview_selection[1] - SELECTION_RANGE_OUT < event->button.y) &&
                    (event->button.y < preview_selection[1] + SELECTION_RANGE_IN) ) &&  /* top */
                  ( (event->button.x > preview_selection[0]) && (event->button.x < preview_selection[2]) ) ) /* in width */
        {
           cursornr = GDK_TOP_SIDE;
        }
        else if ( ( (preview_selection[3] - SELECTION_RANGE_IN < event->button.y) &&
                    (event->button.y < preview_selection[3] + SELECTION_RANGE_OUT) ) &&  /* bottom */
                  ( (event->button.x > preview_selection[0]) && (event->button.x < preview_selection[2]) ) ) /* in width */
        {
           cursornr = GDK_BOTTOM_SIDE;
        }
        else
        {
          cursornr = XSANE_CURSOR_PREVIEW;
        }

        if (cursornr != p->cursornr)
        {
          GdkCursor *cursor = gdk_cursor_new(cursornr); /* set cursor */
          gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
          gdk_cursor_unref(cursor);
          p->cursornr = cursornr;
        }
      }
      break;
  }

  return FALSE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_button_press_event_handler(GtkWidget *window, GdkEvent *event, gpointer data)
{
 Preview *p = data;
 GdkCursor *cursor;
 double preview_selection[4];
 int cursornr;

  DBG(DBG_proc, "preview_button_press_event_handler\n");

  /* preview selection (device) -> cursor-position (window) */
  preview_transform_surface_device_to_window(p, p->selection.coordinate, preview_selection);

  /* cursor-prosition (window) -> preview coordinate (device) */
  double mouse_point[2];
  preview_transform_coordinate_window_to_device(p, (double) event->button.x, (double) event->button.y, mouse_point);

  if (!p->scanning)
  {
    switch (p->mode)
    {
      case MODE_PIPETTE_WHITE:
      {
        DBG(DBG_info, "pipette white mode\n");
        if ( ( (((GdkEventButton *)event)->button == 1) || (((GdkEventButton *)event)->button == 2) ) && (p->image_data_raw) ) /* left or middle button */
        {
         int r=255, g=255, b=255; /* preset color to white */

          preview_get_color(p, (int) event->button.x, (int) event->button.y, preferences.preview_pipette_range, &r, &g, &b);

          xsane.slider_gray.value[2]  = sqrt( (r*r+g*g+b*b) / 3)/2.55;

          if ( (!xsane.enhancement_rgb_default) && (((GdkEventButton *)event)->button == 2) ) /* middle button */
          {
            xsane.slider_red.value[2]   = r/2.55;
            xsane.slider_green.value[2] = g/2.55;
            xsane.slider_blue.value[2]  = b/2.55;
          }
          else
          {
            xsane.slider_red.value[2]   = xsane.slider_gray.value[2];
            xsane.slider_green.value[2] = xsane.slider_gray.value[2];
            xsane.slider_blue.value[2]  = xsane.slider_gray.value[2];
          }

          if (xsane.slider_gray.value[2] < 2)
          {
            xsane.slider_gray.value[2] = 2;
          }
          if (xsane.slider_gray.value[1] >= xsane.slider_gray.value[2])
          {
            xsane.slider_gray.value[1] = xsane.slider_gray.value[2]-1;
            if (xsane.slider_gray.value[0] >= xsane.slider_gray.value[1])
            {
              xsane.slider_gray.value[0] = xsane.slider_gray.value[1]-1;
            }
          }

          if (xsane.slider_red.value[2] < 2)
          {
            xsane.slider_red.value[2] = 2;
          }
          if (xsane.slider_red.value[1] >= xsane.slider_red.value[2])
          {
            xsane.slider_red.value[1] = xsane.slider_red.value[2]-1;
            if (xsane.slider_red.value[0] >= xsane.slider_red.value[1])
            {
              xsane.slider_red.value[0] = xsane.slider_red.value[1]-1;
            }
          }

          if (xsane.slider_green.value[2] < 2)
          {
            xsane.slider_green.value[2] = 2;
          }
          if (xsane.slider_green.value[1] >= xsane.slider_green.value[2])
          {
            xsane.slider_green.value[1] = xsane.slider_green.value[2]-1;
            if (xsane.slider_green.value[0] >= xsane.slider_green.value[1])
            {
              xsane.slider_green.value[0] = xsane.slider_green.value[1]-1;
            }
          }

          if (xsane.slider_blue.value[2] < 2)
          {
            xsane.slider_blue.value[2] = 2;
          }
          if (xsane.slider_blue.value[1] >= xsane.slider_blue.value[2])
          {
            xsane.slider_blue.value[1] = xsane.slider_blue.value[2]-1;
            if (xsane.slider_blue.value[0] >= xsane.slider_blue.value[1])
            {
              xsane.slider_blue.value[0] = xsane.slider_blue.value[1]-1;
            }
          }

          xsane_enhancement_by_histogram(TRUE);
        }

        p->mode = MODE_NORMAL;

        cursor = gdk_cursor_new(XSANE_CURSOR_PREVIEW);
        gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
        gdk_cursor_unref(cursor);
        p->cursornr = XSANE_CURSOR_PREVIEW;
      }
      break;

      case MODE_PIPETTE_GRAY:
      {
        DBG(DBG_info, "pipette gray mode\n");

        if ( ( (((GdkEventButton *)event)->button == 1) || (((GdkEventButton *)event)->button == 2) ) && (p->image_data_raw) ) /* left or middle button */
        {
         int r=128, g=128, b=128; /* preset color to gray */

          preview_get_color(p, (int) event->button.x, (int) event->button.y, preferences.preview_pipette_range, &r, &g, &b);

          xsane.slider_gray.value[1] = sqrt( (r*r+g*g+b*b) / 3)/2.55;

          if ( (!xsane.enhancement_rgb_default) && (((GdkEventButton *)event)->button == 2) ) /* middle button */
          {
            xsane.slider_red.value[1]   = r/2.55;
            xsane.slider_green.value[1] = g/2.55;
            xsane.slider_blue.value[1]  = b/2.55;
          }
          else
          {
            xsane.slider_red.value[1]   = xsane.slider_gray.value[1];
            xsane.slider_green.value[1] = xsane.slider_gray.value[1];
            xsane.slider_blue.value[1]  = xsane.slider_gray.value[1];
          }

          if (xsane.slider_gray.value[1] == 0)
          {
            xsane.slider_gray.value[1] += 1;
          }
          if (xsane.slider_gray.value[1] == 100)
          {
            xsane.slider_gray.value[1] -= 1;
          }
          if (xsane.slider_gray.value[1] >= xsane.slider_gray.value[2])
          {
            xsane.slider_gray.value[2] = xsane.slider_gray.value[1]+1;
          }
          if (xsane.slider_gray.value[1] <= xsane.slider_gray.value[0])
          {
            xsane.slider_gray.value[0] = xsane.slider_gray.value[1]-1;
          }

          if (xsane.slider_red.value[1] == 0)
          {
            xsane.slider_red.value[1] += 1;
          }
          if (xsane.slider_red.value[1] == 100)
          {
            xsane.slider_red.value[1] -= 1;
          }
          if (xsane.slider_red.value[1] >= xsane.slider_red.value[2])
          {
            xsane.slider_red.value[2] = xsane.slider_red.value[1]+1;
          }
          if (xsane.slider_red.value[1] <= xsane.slider_red.value[0])
          {
            xsane.slider_red.value[0] = xsane.slider_red.value[1]-1;
          }

          if (xsane.slider_green.value[1] == 0)
          {
            xsane.slider_green.value[1] += 1;
          }
          if (xsane.slider_green.value[1] == 100)
          {
            xsane.slider_green.value[1] -= 1;
          }
          if (xsane.slider_green.value[1] >= xsane.slider_green.value[2])
          {
            xsane.slider_green.value[2] = xsane.slider_green.value[1]+1;
          }
          if (xsane.slider_green.value[1] <= xsane.slider_green.value[0])
          {
            xsane.slider_green.value[0] = xsane.slider_green.value[1]-1;
          }

          if (xsane.slider_blue.value[1] == 0)
          {
            xsane.slider_blue.value[1] += 1;
          }
          if (xsane.slider_blue.value[1] == 100)
          {
            xsane.slider_blue.value[1] -= 1;
          }
          if (xsane.slider_blue.value[1] >= xsane.slider_blue.value[2])
          {
            xsane.slider_blue.value[2] = xsane.slider_blue.value[1]+1;
          }
          if (xsane.slider_blue.value[1] <= xsane.slider_blue.value[0])
          {
            xsane.slider_blue.value[0] = xsane.slider_blue.value[1]-1;
          }

          xsane_enhancement_by_histogram(TRUE);
        }

        p->mode = MODE_NORMAL;

        cursor = gdk_cursor_new(XSANE_CURSOR_PREVIEW);
        gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
        gdk_cursor_unref(cursor);
        p->cursornr = XSANE_CURSOR_PREVIEW;
      }
      break;

      case MODE_PIPETTE_BLACK:
      {
        DBG(DBG_info, "pipette black mode\n");

        if ( ( (((GdkEventButton *)event)->button == 1) || (((GdkEventButton *)event)->button == 2) ) &&
             (p->image_data_raw) ) /* left or middle button */
        {
         int r=0, g=0, b=0; /* preset color to black */

          preview_get_color(p, (int) event->button.x, (int) event->button.y, preferences.preview_pipette_range, &r, &g, &b);

          xsane.slider_gray.value[0] = sqrt( (r*r+g*g+b*b) / 3)/2.55;

          if ( (!xsane.enhancement_rgb_default) && (((GdkEventButton *)event)->button == 2) ) /* middle button */
          {
            xsane.slider_red.value[0]   = r/2.55;
            xsane.slider_green.value[0] = g/2.55;
            xsane.slider_blue.value[0]  = b/2.55;
          }
          else
          {
            xsane.slider_red.value[0]   = xsane.slider_gray.value[0];
            xsane.slider_green.value[0] = xsane.slider_gray.value[0];
            xsane.slider_blue.value[0]  = xsane.slider_gray.value[0];
          }

          if (xsane.slider_gray.value[0] > 98)
          {
            xsane.slider_gray.value[0] = 98;
          }
          if (xsane.slider_gray.value[1] <= xsane.slider_gray.value[0])
          {
            xsane.slider_gray.value[1] = xsane.slider_gray.value[0]+1;
            if (xsane.slider_gray.value[2] <= xsane.slider_gray.value[1])
            {
              xsane.slider_gray.value[2] = xsane.slider_gray.value[1]+1;
            }
          }

          if (xsane.slider_red.value[0] > 98)
          {
            xsane.slider_red.value[0] = 98;
          }
          if (xsane.slider_red.value[1] <= xsane.slider_red.value[0])
          {
            xsane.slider_red.value[1] = xsane.slider_red.value[0]+1;
            if (xsane.slider_red.value[2] <= xsane.slider_red.value[1])
            {
              xsane.slider_red.value[2] = xsane.slider_red.value[1]+1;
            }
          }

          if (xsane.slider_green.value[0] > 98)
          {
            xsane.slider_green.value[0] = 98;
          }
          if (xsane.slider_green.value[1] <= xsane.slider_green.value[0])
          {
            xsane.slider_green.value[1] = xsane.slider_green.value[0]+1;
            if (xsane.slider_green.value[2] <= xsane.slider_green.value[1])
            {
              xsane.slider_green.value[2] = xsane.slider_green.value[1]+1;
            }
          }

          if (xsane.slider_blue.value[0] > 98)
          {
            xsane.slider_blue.value[0] = 98;
          }
          if (xsane.slider_blue.value[1] <= xsane.slider_blue.value[0])
          {
            xsane.slider_blue.value[1] = xsane.slider_blue.value[0]+1;
            if (xsane.slider_blue.value[2] <= xsane.slider_blue.value[1])
            {
              xsane.slider_blue.value[2] = xsane.slider_blue.value[1]+1;
            }
          }

          xsane_enhancement_by_histogram(TRUE);
        }

        p->mode = MODE_NORMAL;

        cursor = gdk_cursor_new(XSANE_CURSOR_PREVIEW);
        gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
        gdk_cursor_unref(cursor);
        p->cursornr = XSANE_CURSOR_PREVIEW;
      }
      break;

      case MODE_AUTORAISE_SCAN_AREA:
      {
        DBG(DBG_info, "autoraise scan area mode\n");

        if ( ( (((GdkEventButton *)event)->button == 1) || (((GdkEventButton *)event)->button == 2) ) &&
             (p->image_data_raw) ) /* left or middle button */
        {
          preview_autoraise_scan_area(p, (int) event->button.x, (int) event->button.y, p->selection.coordinate); /* raise selection area */
        }

        p->mode = MODE_NORMAL;

        cursor = gdk_cursor_new(XSANE_CURSOR_PREVIEW);
        gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
        gdk_cursor_unref(cursor);
        p->cursornr = XSANE_CURSOR_PREVIEW;
      }
      break;

      case MODE_ZOOM_IN:
      {
        DBG(DBG_info, "zoom in mode\n");

        if ( ( (((GdkEventButton *)event)->button == 1) || (((GdkEventButton *)event)->button == 2) ) &&
             (p->image_data_raw) ) /* left or middle button */
        {
          preview_select_zoom_point(p, (int) event->button.x, (int) event->button.y); /* select zoom point */
        }
        else
        {
          p->mode = MODE_NORMAL;

          cursor = gdk_cursor_new(XSANE_CURSOR_PREVIEW);
          gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
          gdk_cursor_unref(cursor);
          p->cursornr = XSANE_CURSOR_PREVIEW;
        }
      }
      break;

      case MODE_NORMAL:
      {
        DBG(DBG_info, "normal mode\n");

        if (p->show_selection)
        {
          switch (((GdkEventButton *)event)->button)
          {
            case 1: /* left button: define selection area */
              DBG(DBG_info, "left button\n");

              p->selection_xedge = -1;
              if ( (preview_selection[0] - SELECTION_RANGE_OUT < event->button.x) &&
                   (event->button.x < preview_selection[0] + SELECTION_RANGE_IN) ) /* left */
              {
                DBG(DBG_info, "-left\n");
                p->selection_xedge = 0;
              }
              else if ( (preview_selection[2] - SELECTION_RANGE_IN < event->button.x) &&
                        (event->button.x < preview_selection[2] + SELECTION_RANGE_OUT) ) /* right */
              {
                DBG(DBG_info, "-right\n");
                p->selection_xedge = 2;
              }

              p->selection_yedge = -1;
              if ( (preview_selection[1] - SELECTION_RANGE_OUT < event->button.y) &&
                   (event->button.y < preview_selection[1] + SELECTION_RANGE_IN) ) /* top */
              {
                DBG(DBG_info, "-top\n");
                p->selection_yedge = 1;
              }
              else if ( (preview_selection[3] - SELECTION_RANGE_IN < event->button.y) &&
                        (event->button.y < preview_selection[3] + SELECTION_RANGE_OUT) ) /* bottom */
              {
                DBG(DBG_info, "-bottom\n");
                p->selection_yedge = 3;
              }

              if ( (p->selection_xedge != -1) && (p->selection_yedge != -1) ) /* move corner */
              {
                DBG(DBG_info, "-move corner (%f, %f)\n", mouse_point[0], mouse_point[1]);
                p->selection_drag_edge = TRUE;
                p->selection.coordinate[p->selection_xedge] = mouse_point[0];
                p->selection.coordinate[p->selection_yedge] = mouse_point[1];
                preview_draw_selection(p);
              }
              else if ( (p->selection_xedge != -1) && (event->button.y > preview_selection[1])
                        && (event->button.y < preview_selection[3]) ) /* move x-edge */
              {
                DBG(DBG_info, "-move x-edge %f\n", mouse_point[0]);
                p->selection_drag_edge = TRUE;
                p->selection.coordinate[p->selection_xedge] = mouse_point[0];
                preview_draw_selection(p);
              }
              else if ( (p->selection_yedge != -1)  && (event->button.x > preview_selection[0])
                        && (event->button.x < preview_selection[2]) ) /* move y-edge */
              {
                DBG(DBG_info, "-move y-edge %f\n", mouse_point[1]);
                p->selection_drag_edge = TRUE;
                p->selection.coordinate[p->selection_yedge] = mouse_point[1];
                preview_draw_selection(p);
              } 
              else /* select new area */
              {
                DBG(DBG_info, "-define new area (%f, %f)\n", mouse_point[0], mouse_point[1]);
                p->selection_xedge = 2;
                p->selection_yedge = 3;
                p->selection.coordinate[0] = p->selection.coordinate[2] = mouse_point[0];
                p->selection.coordinate[1] = p->selection.coordinate[3] = mouse_point[1];
                p->selection_drag = TRUE;

                cursornr = GDK_CROSS;
                cursor = gdk_cursor_new(cursornr);	/* set cursor */
                gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
                gdk_cursor_unref(cursor);
                p->cursornr = cursornr;
              }
             break;

            case 2: /* middle button */
            case 3: /* right button */
              DBG(DBG_info, "middle or right button\n");

              if ( (preview_selection[0]-SELECTION_RANGE_OUT < event->button.x) &&
                   (preview_selection[2]+SELECTION_RANGE_OUT > event->button.x) &&
                   (preview_selection[1]-SELECTION_RANGE_OUT < event->button.y) &&
                   (preview_selection[3]+SELECTION_RANGE_OUT > event->button.y) )
              {
                DBG(DBG_info, "move selection area\n");
                p->selection_drag = TRUE;
                preview_transform_coordinate_window_to_device(p, event->button.x, event->button.y, p->selection_pos);

                cursornr = GDK_HAND2;
                cursor = gdk_cursor_new(cursornr);	/* set curosr */
                gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
                gdk_cursor_unref(cursor);
                p->cursornr = cursornr;
              }
             break;

            default:
             break;
          }
        }
      }
    }
  }

 return FALSE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_button_release_event_handler(GtkWidget *window, GdkEvent *event, gpointer data)
{
 Preview *p = data;
 GdkCursor *cursor;
 int cursornr;

  DBG(DBG_proc, "preview_button_release_event_handler\n");

  if (!p->scanning)
  {
    if (p->show_selection)
    {
      switch (((GdkEventButton *)event)->button)
      {
        case 1: /* left button */
        case 2: /* middle button */
        case 3: /* right button */
          if (p->selection_drag)
          {
            DBG(DBG_info, "selection finished\n");
            cursornr = XSANE_CURSOR_PREVIEW;
            cursor = gdk_cursor_new(cursornr);	/* set curosr */
            gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
            gdk_cursor_unref(cursor);
            p->cursornr = cursornr;
          }

          preview_draw_selection(p);
          preview_establish_selection(p);

          p->selection_drag_edge = FALSE;
          p->selection_drag = FALSE;
        break;

        default:
         break;
      }
    }
  }

 return FALSE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */
static int expose_event_selection_active, expose_event_selection_maximum_active;

static gint preview_expose_event_handler_start(GtkWidget *window, GdkEvent *event, gpointer data)
{
 Preview *p = data;
 GdkColor color;
 GdkColormap *colormap; 

  DBG(DBG_proc, "preview_expose_event_handler_start\n");

  if (event->type == GDK_EXPOSE)
  {
    if (!p->gc_selection)
    {
      DBG(DBG_info, "defining line styles for selection and page frames\n");
      colormap = gdk_drawable_get_colormap(gtk_widget_get_window(p->window));

      p->gc_selection = gdk_gc_new(gtk_widget_get_window(p->window));
      gdk_gc_set_function(p->gc_selection, GDK_INVERT);
      gdk_gc_set_line_attributes(p->gc_selection, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_BUTT, GDK_JOIN_MITER);

      p->gc_selection_maximum = gdk_gc_new(gtk_widget_get_window(p->window));
      gdk_gc_set_function(p->gc_selection_maximum, GDK_XOR);
      gdk_gc_set_line_attributes(p->gc_selection_maximum, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_BUTT, GDK_JOIN_MITER);

      color.red   = 0;
      color.green = 65535;
      color.blue  = 30000;
      gdk_color_alloc(colormap, &color);
      gdk_gc_set_foreground(p->gc_selection_maximum, &color);   
    }
    else
    {
      expose_event_selection_active         = p->selection.active;
      expose_event_selection_maximum_active = p->selection_maximum.active;
      p->selection_maximum.active = FALSE;
      p->selection.active = FALSE; /* do not draw new selections */
      p->selection_maximum.active = FALSE;
      preview_draw_selection(p); /* undraw selections */
    }
  }

 return FALSE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_expose_event_handler_end(GtkWidget *window, GdkEvent *event, gpointer data)
{
 Preview *p = data;
 GdkColor color;
 GdkColormap *colormap; 

  DBG(DBG_proc, "preview_expose_event_handler_end\n");

  if (event->type == GDK_EXPOSE)
  {
    if (!p->gc_selection)
    {
      DBG(DBG_info, "defining line styles for selection and page frames\n");
      colormap = gdk_drawable_get_colormap(gtk_widget_get_window(p->window));

      p->gc_selection = gdk_gc_new(gtk_widget_get_window(p->window));
      gdk_gc_set_function(p->gc_selection, GDK_INVERT);
      gdk_gc_set_line_attributes(p->gc_selection, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_BUTT, GDK_JOIN_MITER);

      p->gc_selection_maximum = gdk_gc_new(gtk_widget_get_window(p->window));
      gdk_gc_set_function(p->gc_selection_maximum, GDK_XOR);
      gdk_gc_set_line_attributes(p->gc_selection_maximum, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_BUTT, GDK_JOIN_MITER);

      color.red   = 0;
      color.green = 65535;
      color.blue  = 30000;
      gdk_color_alloc(colormap, &color);
      gdk_gc_set_foreground(p->gc_selection_maximum, &color);   
    }
    else
    {
      p->selection.active         = expose_event_selection_active;
      p->selection_maximum.active = expose_event_selection_maximum_active;
      preview_draw_selection(p); /* draw selections again */
    }
  }

 return FALSE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_start_button_clicked(GtkWidget *widget, gpointer data)
{
  DBG(DBG_proc, "preview_start_button_clicked\n");

  preview_scan(data);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_cancel_button_clicked(GtkWidget *widget, gpointer data)
{
 Preview *p = (Preview *) data;

  DBG(DBG_proc, "preview_cancel_button_clicked\n");

  sane_cancel(xsane.dev);
  gtk_widget_set_sensitive(p->cancel, FALSE); /* disable cancel button */

  /* we have to make sure that xsane does detect that the scan has been cancled */
  /* but the select_fd does not make sure that preview_read_image_data is called */
  /* when the select_fd is closed by the backend, so we have to make sure that */
  /* preview_read_image_data is called */
  preview_read_image_data(NULL, G_IO_IN, p);

  p->scan_incomplete = TRUE;
  preview_display_valid(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_create_preset_area_menu(Preview *p, guint selection)
{
 int i;
 GtkWidget *preset_area_menu, *preset_area_item;

  preset_area_menu = gtk_menu_new();

  for (i = 0; i < preferences.preset_area_definitions; ++i)
  {
    preset_area_item = gtk_menu_item_new_with_label(preferences.preset_area[i]->name);
    gtk_container_add(GTK_CONTAINER(preset_area_menu), preset_area_item);
    g_signal_connect(G_OBJECT(preset_area_item), "button_press_event", G_CALLBACK(preview_preset_area_context_menu_callback), p);
    g_signal_connect(G_OBJECT(preset_area_item), "activate", G_CALLBACK(preview_preset_area_callback), p);
    g_object_set_data(G_OBJECT(preset_area_item), "Selection", GINT_TO_POINTER(i));
    g_object_set_data(G_OBJECT(preset_area_item), "Preview", (void *) p);

    gtk_widget_show(preset_area_item);
  }                  

  gtk_option_menu_set_menu(GTK_OPTION_MENU(p->preset_area_option_menu), preset_area_menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(p->preset_area_option_menu), selection); 

  gtk_widget_show(preset_area_menu);
  gtk_widget_queue_draw(p->preset_area_option_menu);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_generate_preview_filenames(Preview *p)
{
 char filename[PATH_MAX];
 int i;

  DBG(DBG_proc, "preview_generate_preview_filenames\n");

  for(i=0; i<=2; i++) /* create random filenames for previews */
  {
    if (preview_make_image_path(p, sizeof(filename), filename, i)>=0)
    {
     FILE *testfile;

      testfile = fopen(filename, "wb");
      if (testfile)
      {
        fclose(testfile);
        p->filename[i] = strdup(filename);/* store filename */
        DBG(DBG_info, "preview file %s created\n", filename);
      }
      else
      {
        DBG(DBG_error, "ERROR: could not create preview file %s\n", filename);
        p->filename[0] = NULL; /* mark filename does not exist */
        p->filename[1] = NULL; /* mark filename does not exist */
        p->filename[2] = NULL; /* mark filename does not exist */
       break; /* do not try next preview level, one error is enough */
      }
    }
    else
    {
      DBG(DBG_error, "ERROR: could not create filename for preview level %d\n", i);
      p->filename[0] = NULL; /* mark filename does not exist */
      p->filename[1] = NULL; /* mark filename does not exist */
      p->filename[2] = NULL; /* mark filename does not exist */
     break; /* do not try next preview level, one error is enough */
    }
  }

 return;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

Preview *preview_new(void)
{
 GtkWidget *table, *frame;
 GCallback signal_func;
 GtkWidgetClass *class;
 GtkWidget *vbox, *action_box;
 GtkWidget *outer_hbox, *middle_vbox;
 GdkCursor *cursor;
 GtkWidget *preset_area_option_menu;
 GtkWidget *rotation_option_menu, *rotation_menu, *rotation_item;
 GtkWidget *ratio_option_menu, *ratio_menu, *ratio_item;
 GtkWidget *delete_images;
 GdkBitmap *mask;
 GdkPixmap *pixmap = NULL;
 GtkWidget *pixmapwidget;
 Preview *p;
 guint i;
 char buf[TEXTBUFSIZE];
 guint ratio_nr = 0;

  DBG(DBG_proc, "preview_new\n");

  p = malloc(sizeof(*p));
  if (!p)
  {
    return 0;
  }
  memset(p, 0, sizeof(*p));

  p->mode          = MODE_NORMAL; /* no pipette functions etc */
  p->calibration   = 0; /* do not display calibration image */
  p->input_active  = FALSE;
  p->rotation      = 0;
  p->gamma_functions_interruptable = FALSE;

  p->scanner_surface[0] = -INF;
  p->scanner_surface[1] = -INF;
  p->scanner_surface[2] = INF;
  p->scanner_surface[3] = INF;

  p->current_zoom.zoom_mode = Zoom_mode_fit_to_window;
  p->previous_zoom.zoom_mode = Zoom_mode_undefined;

  gtk_preview_set_gamma(1.0);
  gtk_preview_set_install_cmap(preferences.preview_own_cmap);

  preview_generate_preview_filenames(p);

  p->preset_surface[0] = 0;
  p->preset_surface[1] = 0;
  p->preset_surface[2] = INF;
  p->preset_surface[3] = INF;

  p->maximum_output_width  = INF; /* full output with */
  p->maximum_output_height = INF; /* full output height */
  p->block_update_maximum_output_size_clipping = FALSE;

  p->preview_channels = -1;
  p->invalid = TRUE; /* no valid preview */
  p->ratio = 0.0;

#ifndef XSERVER_WITH_BUGGY_VISUALS
  gtk_widget_push_visual(gtk_preview_get_visual()); /* this has no function for gtk+-2.0 */
#endif
  gtk_widget_push_colormap(gtk_preview_get_cmap());

  snprintf(buf, sizeof(buf), "%s %s", WINDOW_PREVIEW, xsane.device_text);
  p->top = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(p->top), buf);
  xsane_set_window_icon(p->top, 0);
  gtk_window_add_accel_group(GTK_WINDOW(p->top), xsane.accelerator_group);


  /* set the main vbox */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 0);
  gtk_container_add(GTK_CONTAINER(p->top), vbox);
  gtk_widget_show(vbox);     



  /* the button_box (hbox) */
  p->button_box = gtk_hbox_new(FALSE, 1);
  gtk_container_set_border_width(GTK_CONTAINER(p->button_box), 0);
  gtk_box_pack_start(GTK_BOX(vbox), p->button_box, FALSE, FALSE, 0);


  /* add new selection for batch scanning */
  p->add_batch  = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, add_batch_xpm, DESC_ADD_BATCH, G_CALLBACK(preview_add_batch), p);

  xsane_vseparator_new(p->button_box, 3);

  /* White, gray and black pipette button */
  p->pipette_white = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, pipette_white_xpm, DESC_PIPETTE_WHITE, G_CALLBACK(preview_pipette_white), p);
  p->pipette_gray  = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, pipette_gray_xpm,  DESC_PIPETTE_GRAY,  G_CALLBACK(preview_pipette_gray),  p);
  p->pipette_black = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, pipette_black_xpm, DESC_PIPETTE_BLACK, G_CALLBACK(preview_pipette_black), p);

  xsane_vseparator_new(p->button_box, 3);

  /* Zoom not, zoom out and zoom in button */
  p->zoom_not    = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, zoom_not_xpm,  DESC_ZOOM_FULL, G_CALLBACK(preview_zoom_not),  p);
  p->zoom_out    = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, zoom_out_xpm,  DESC_ZOOM_OUT,  G_CALLBACK(preview_zoom_out),  p);
  p->zoom_in     = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, zoom_in_xpm,   DESC_ZOOM_IN,   G_CALLBACK(preview_zoom_in),   p);
  p->zoom_area   = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, zoom_area_xpm, DESC_ZOOM_AREA, G_CALLBACK(preview_zoom_area), p);
  p->zoom_undo   = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, zoom_undo_xpm, DESC_ZOOM_UNDO, G_CALLBACK(preview_zoom_undo), p);

  xsane_vseparator_new(p->button_box, 3);

  p->full_area   = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, auto_select_preview_area_xpm, DESC_AUTOSELECT_SCAN_AREA, G_CALLBACK(preview_autoselect_scan_area_callback), p);
  p->autoraise   = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, auto_raise_preview_area_xpm,  DESC_AUTORAISE_SCAN_AREA,  G_CALLBACK(preview_init_autoraise_scan_area),      p);
  p->autoselect  = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, full_preview_area_xpm,        DESC_FULL_PREVIEW_AREA,    G_CALLBACK(preview_full_preview_area_callback),    p);

  p->autoselect  = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, area_135_film_half_frame_xpm, DESC_135_FILM_HALF_FRAME_AREA, G_CALLBACK(preview_135_film_half_frame_area_callback),   p);

  xsane_vseparator_new(p->button_box, 3);

  delete_images  = xsane_button_new_with_pixmap(gtk_widget_get_window(p->top), p->button_box, delete_images_xpm,            DESC_DELETE_IMAGES,        G_CALLBACK(preview_delete_images_callback),        p);

  gtk_widget_add_accelerator(p->zoom_not,   "clicked", xsane.accelerator_group, GDK_KEY_KP_Multiply, GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt keypad_* */
  gtk_widget_add_accelerator(p->zoom_out,   "clicked", xsane.accelerator_group, GDK_KEY_KP_Subtract, GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt keypad_- */
  gtk_widget_add_accelerator(p->zoom_in,    "clicked", xsane.accelerator_group, GDK_KEY_KP_Add,      GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt keypad_+ */
  gtk_widget_add_accelerator(p->zoom_area,  "clicked", xsane.accelerator_group, GDK_KEY_KP_Enter,    GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt keypad_Enter */
  gtk_widget_add_accelerator(p->zoom_undo,  "clicked", xsane.accelerator_group, GDK_KEY_KP_Divide,   GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt keypad_/ */
  gtk_widget_add_accelerator(p->full_area,  "clicked", xsane.accelerator_group, GDK_KEY_A, GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt keypad_* */
  gtk_widget_add_accelerator(p->autoselect, "clicked", xsane.accelerator_group, GDK_KEY_V, GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt keypad_* */
  gtk_widget_add_accelerator(delete_images, "clicked", xsane.accelerator_group, GDK_KEY_KP_Delete, GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt keypad_* */

  gtk_widget_set_sensitive(p->full_area,  FALSE); /* no selection */
  gtk_widget_set_sensitive(p->autoselect, FALSE); /* no selection */


  gtk_widget_show(p->button_box);
  /* the button box is ready */

  /* construct the preview area (table with sliders & preview window) */

  table = gtk_table_new(2, 2, /* homogeneous */ FALSE);
  gtk_table_set_col_spacing(GTK_TABLE(table), 0, 1);
  gtk_table_set_row_spacing(GTK_TABLE(table), 0, 1);
  gtk_container_set_border_width(GTK_CONTAINER(table), 1);
  gtk_box_pack_start(GTK_BOX(vbox), table, /* expand */ TRUE, /* fill */ TRUE, /* padding */ 0);
  gtk_widget_show(table);

  /* the empty box in the top-left corner */
  frame = gtk_frame_new(/* label */ 0);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  gtk_table_attach(GTK_TABLE(table), frame, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(frame);

  /* the unit label */
  p->unit_label = gtk_label_new("cm");
  gtk_container_add(GTK_CONTAINER(frame), p->unit_label);
  gtk_widget_show(p->unit_label);

  /* the horizontal ruler */
  p->hruler = gtk_hruler_new();
  gtk_table_attach(GTK_TABLE(table), p->hruler, 1, 2, 0, 1, GTK_FILL, 0, 0, 0);
  gtk_widget_show(p->hruler);

  /* the vertical ruler */
  p->vruler = gtk_vruler_new();
  gtk_table_attach(GTK_TABLE(table), p->vruler, 0, 1, 1, 2, 0, GTK_FILL, 0, 0);
  gtk_widget_show(p->vruler);

  /* the preview area */
  p->window = gtk_preview_new(GTK_PREVIEW_COLOR);
  gtk_preview_set_expand(GTK_PREVIEW(p->window), TRUE);

  gtk_widget_set_events(p->window, GDK_EXPOSURE_MASK | GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK |
			GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);

  /* the first expose_event is responsible to undraw the selection frame */
  g_signal_connect(G_OBJECT(p->window), "expose_event",         G_CALLBACK(preview_expose_event_handler_start), p);
  g_signal_connect(G_OBJECT(p->window), "button_press_event",   G_CALLBACK(preview_button_press_event_handler), p);
  g_signal_connect(G_OBJECT(p->window), "motion_notify_event",  G_CALLBACK(preview_motion_event_handler), p);
  g_signal_connect(G_OBJECT(p->window), "button_release_event", G_CALLBACK(preview_button_release_event_handler), p);

  g_signal_connect_after(G_OBJECT(p->window), "size_allocate", G_CALLBACK(preview_area_resize_handler), p);
  /* the second expose_event is responsible to redraw the selection frame */
  g_signal_connect_after(G_OBJECT(p->window), "expose_event",  G_CALLBACK(preview_expose_event_handler_end), p);

  /* Connect the motion-notify events of the preview area with the rulers.  Nifty stuff!  */

#ifdef HAVE_GTK2
  class = (GtkWidgetClass *) GTK_HSCROLLBAR_GET_CLASS(p->hruler);
#else
  class = GTK_WIDGET_CLASS(GTK_OBJECT(p->hruler)->klass);
#endif

  signal_func = G_CALLBACK(class->motion_notify_event);
  g_signal_connect_swapped(G_OBJECT(p->window), "motion_notify_event", G_CALLBACK(signal_func), G_OBJECT(p->hruler));

#ifdef HAVE_GTK2
  class = (GtkWidgetClass *) GTK_VSCROLLBAR_GET_CLASS(p->vruler);
#else
  class = GTK_WIDGET_CLASS(GTK_OBJECT(p->vruler)->klass);
#endif

  signal_func = G_CALLBACK(class->motion_notify_event);
  g_signal_connect_swapped(G_OBJECT(p->window), "motion_notify_event", G_CALLBACK(signal_func), G_OBJECT(p->vruler));


  p->viewport = gtk_frame_new(/* label */ 0);
  gtk_frame_set_shadow_type(GTK_FRAME(p->viewport), GTK_SHADOW_IN);
  gtk_container_add(GTK_CONTAINER(p->viewport), p->window);
  gtk_widget_show(p->viewport);

  gtk_table_attach(GTK_TABLE(table), p->viewport, 1, 2, 1, 2,
		   GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);
  /* the preview area is ready */

  /* the outer hbox at the bottom */
  outer_hbox = gtk_hbox_new(FALSE, 4);
  gtk_container_set_border_width(GTK_CONTAINER(outer_hbox), 1);
  gtk_box_pack_start(GTK_BOX(vbox), outer_hbox, FALSE, FALSE, 0);
  gtk_widget_show(outer_hbox);

  /* the middle vbox at the bottom */
  middle_vbox = gtk_vbox_new(FALSE, 4);
  gtk_container_set_border_width(GTK_CONTAINER(middle_vbox), 1);
  gtk_box_pack_start(GTK_BOX(outer_hbox), middle_vbox, FALSE, FALSE, 0);
  gtk_widget_show(middle_vbox);

  /* the menu_box (hbox) */
  p->menu_box = gtk_hbox_new(FALSE, 4);
  gtk_container_set_border_width(GTK_CONTAINER(p->menu_box), 1);
  gtk_box_pack_start(GTK_BOX(middle_vbox), p->menu_box, FALSE, FALSE, 0);

  xsane_separator_new(middle_vbox, 1);


  /* select maximum scan area */
  pixmap = gdk_pixmap_create_from_xpm_d(gtk_widget_get_window(p->top), &mask, xsane.bg_trans, (gchar **) size_xpm);
  pixmapwidget = gtk_image_new_from_pixmap(pixmap, mask);
  gtk_box_pack_start(GTK_BOX(p->menu_box), pixmapwidget, FALSE, FALSE, 2);
  gtk_widget_show(pixmapwidget);
  gdk_drawable_unref(pixmap);
  gdk_drawable_unref(mask);

  preset_area_option_menu = gtk_option_menu_new();
  xsane_back_gtk_set_tooltip(xsane.tooltips, preset_area_option_menu, DESC_PRESET_AREA);
  gtk_box_pack_start(GTK_BOX(p->menu_box), preset_area_option_menu, FALSE, FALSE, 0);
  gtk_widget_show(preset_area_option_menu);
  p->preset_area_option_menu = preset_area_option_menu;
  preview_create_preset_area_menu(p, 0); /* build menu and set default to 0=full size */

  xsane_vseparator_new(p->menu_box, 3);

  /* select rotation */
  pixmap = gdk_pixmap_create_from_xpm_d(gtk_widget_get_window(p->top), &mask, xsane.bg_trans, (gchar **) rotation_xpm);
  pixmapwidget = gtk_image_new_from_pixmap(pixmap, mask);
  gtk_box_pack_start(GTK_BOX(p->menu_box), pixmapwidget, FALSE, FALSE, 2);
  gtk_widget_show(pixmapwidget);
  gdk_drawable_unref(pixmap);
  gdk_drawable_unref(mask);

  rotation_menu = gtk_menu_new();

  for (i = 0; i < 12; ++i)
  {
   guint rot;

    if (i<4)
    {
      snprintf(buf, sizeof(buf), "%03d  ", i*90);  
      rot = i;
    }
    else if (i<8)
    {
      snprintf(buf, sizeof(buf), "%03d |", i*90-360);  
      rot = i;
    }
    else
    {
      snprintf(buf, sizeof(buf), "%03d -", i*90-2*360);  
      rot = (((i & 3) + 2) & 3) + 4;
    }
    rotation_item = gtk_menu_item_new_with_label(buf);
    gtk_container_add(GTK_CONTAINER(rotation_menu), rotation_item);
    g_signal_connect(G_OBJECT(rotation_item), "activate", G_CALLBACK(preview_rotation_callback), p);
    g_object_set_data(G_OBJECT(rotation_item), "Selection", GUINT_TO_POINTER(rot));

    gtk_widget_show(rotation_item);
  }                  

  rotation_option_menu = gtk_option_menu_new();
  xsane_back_gtk_set_tooltip(xsane.tooltips, rotation_option_menu, DESC_ROTATION);
  gtk_box_pack_start(GTK_BOX(p->menu_box), rotation_option_menu, FALSE, FALSE, 0);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(rotation_option_menu), rotation_menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(rotation_option_menu), p->rotation); /* set rotation */

  gtk_widget_show(rotation_option_menu);
  p->rotation_option_menu = rotation_option_menu;

  xsane_vseparator_new(p->menu_box, 3);

  /* the preview aspect ratio menu */
  pixmap = gdk_pixmap_create_from_xpm_d(gtk_widget_get_window(p->top), &mask, xsane.bg_trans, (gchar **) aspect_ratio_xpm);
  pixmapwidget = gtk_image_new_from_pixmap(pixmap, mask);
  gtk_box_pack_start(GTK_BOX(p->menu_box), pixmapwidget, FALSE, FALSE, 2);
  gtk_widget_show(pixmapwidget);
  gdk_drawable_unref(pixmap);
  gdk_drawable_unref(mask);

  ratio_menu = gtk_menu_new();

  for (i = 0; i < sizeof(ratio_value)/sizeof(double); ++i)
  {
    ratio_item = gtk_menu_item_new_with_label(ratio_string[i]);
    gtk_container_add(GTK_CONTAINER(ratio_menu), ratio_item);
    g_signal_connect(G_OBJECT(ratio_item), "activate", G_CALLBACK(preview_ratio_callback), p);
    g_object_set_data(G_OBJECT(ratio_item), "Selection", &ratio_value[i]);

    gtk_widget_show(ratio_item);

    if (ratio_value[i] == p->ratio)
    {
      ratio_nr = i;
    }
  }                  

  ratio_option_menu = gtk_option_menu_new();
  xsane_back_gtk_set_tooltip(xsane.tooltips, ratio_option_menu, DESC_RATIO);
  gtk_box_pack_start(GTK_BOX(p->menu_box), ratio_option_menu, FALSE, FALSE, 0);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(ratio_option_menu), ratio_menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(ratio_option_menu), ratio_nr); /* set ratio */

  gtk_widget_show(ratio_option_menu);
  p->ratio_option_menu = ratio_option_menu;

  /* the pointer zoom */
  frame = gtk_frame_new(0);
  gtk_box_pack_start(GTK_BOX(outer_hbox), frame, FALSE, FALSE, 3);
  gtk_container_set_border_width(GTK_CONTAINER(frame), 0);
  gtk_widget_show(frame);
  p->zoom = gtk_preview_new(GTK_PREVIEW_COLOR);
  gtk_preview_size(GTK_PREVIEW(p->zoom), XSANE_ZOOM_SIZE, XSANE_ZOOM_SIZE);
  gtk_container_add(GTK_CONTAINER(frame), p->zoom);
  gtk_widget_show(p->zoom);

  gtk_widget_show(p->menu_box);
  /* the menu box is ready */

  /* set the action_hbox */
  action_box = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(middle_vbox), action_box, FALSE, FALSE, 2);
  gtk_container_set_border_width(GTK_CONTAINER(action_box), 0);
  gtk_widget_show(action_box);


  /* the (in)valid pixmaps */
  pixmap = gdk_pixmap_create_from_xpm_d(gtk_widget_get_window(p->top), &mask, xsane.bg_trans, (gchar **) valid_xpm);
  p->valid_pixmap = gtk_image_new_from_pixmap(pixmap, mask);
  gtk_box_pack_start(GTK_BOX(action_box), p->valid_pixmap, FALSE, FALSE, 0);
  gtk_widget_show(p->valid_pixmap);
  gdk_drawable_unref(pixmap);
  gdk_drawable_unref(mask);

  pixmap = gdk_pixmap_create_from_xpm_d(gtk_widget_get_window(p->top), &mask, xsane.bg_trans, (gchar **) scanning_xpm);
  p->scanning_pixmap = gtk_image_new_from_pixmap(pixmap, mask);
  gtk_box_pack_start(GTK_BOX(action_box), p->scanning_pixmap, FALSE, FALSE, 0);
  gtk_widget_show(p->scanning_pixmap);
  gdk_drawable_unref(pixmap);
  gdk_drawable_unref(mask);

  pixmap = gdk_pixmap_create_from_xpm_d(gtk_widget_get_window(p->top), &mask, xsane.bg_trans, (gchar **) incomplete_xpm);
  p->incomplete_pixmap = gtk_image_new_from_pixmap(pixmap, mask);
  gtk_box_pack_start(GTK_BOX(action_box), p->incomplete_pixmap, FALSE, FALSE, 0);
  gtk_widget_show(p->incomplete_pixmap);
  gdk_drawable_unref(pixmap);
  gdk_drawable_unref(mask);

  pixmap = gdk_pixmap_create_from_xpm_d(gtk_widget_get_window(p->top), &mask, xsane.bg_trans, (gchar **) invalid_xpm);
  p->invalid_pixmap = gtk_image_new_from_pixmap(pixmap, mask);
  gtk_box_pack_start(GTK_BOX(action_box), p->invalid_pixmap, FALSE, FALSE, 0);
  gtk_widget_show(p->invalid_pixmap);
  gdk_drawable_unref(pixmap);
  gdk_drawable_unref(mask);

  /* Start button */
  p->start = gtk_button_new_with_label(BUTTON_PREVIEW_ACQUIRE);
  xsane_back_gtk_set_tooltip(xsane.tooltips, p->start, DESC_PREVIEW_ACQUIRE);
  g_signal_connect(G_OBJECT(p->start), "clicked", G_CALLBACK(preview_start_button_clicked), p);
  gtk_box_pack_start(GTK_BOX(action_box), p->start, TRUE, TRUE, 5);
  gtk_widget_add_accelerator(p->start, "clicked", xsane.accelerator_group, GDK_KEY_P, GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt P */
  gtk_widget_show(p->start);

  /* Cancel button */
  p->cancel = gtk_button_new_with_label(BUTTON_PREVIEW_CANCEL);
  xsane_back_gtk_set_tooltip(xsane.tooltips, p->cancel, DESC_PREVIEW_CANCEL);
  g_signal_connect(G_OBJECT(p->cancel), "clicked", G_CALLBACK(preview_cancel_button_clicked), p);
  gtk_box_pack_start(GTK_BOX(action_box), p->cancel, TRUE, TRUE, 5);
  gtk_widget_add_accelerator(p->cancel, "clicked", xsane.accelerator_group, GDK_KEY_Escape, GDK_MOD1_MASK, DEF_GTK_ACCEL_LOCKED); /* Alt ESC */
  gtk_widget_show(p->cancel);
  gtk_widget_set_sensitive(p->cancel, FALSE);

  /* the RGB label */
  frame = gtk_frame_new(0);
  gtk_box_pack_start(GTK_BOX(action_box), frame, FALSE, FALSE, 3);
  gtk_widget_show(frame);
  p->rgb_label = gtk_label_new(0);
  gtk_container_add(GTK_CONTAINER(frame), p->rgb_label);
  gtk_widget_show(p->rgb_label);
  preview_display_color_components(p, -1, -1); /* display "###, ###, ###" */

  preview_init_window_size(p);

  gtk_widget_show(p->window);
  gtk_widget_show(p->top);

  cursor = gdk_cursor_new(XSANE_CURSOR_PREVIEW);	/* set default cursor */
  gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
  gdk_cursor_unref(cursor);
  p->cursornr = XSANE_CURSOR_PREVIEW;

  gtk_widget_pop_colormap();
#ifndef XSERVER_WITH_BUGGY_VISUALS
  gtk_widget_pop_visual();
#endif

  preview_display_valid(p);

 return p;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static int preview_calculate_full_scanner_surface(Preview *p, double surface[4], SANE_Value_Type *type, SANE_Unit *unit)
{
  DBG(DBG_proc, "preview_calculate_full_scanner_surface\n");

  int i;

  /* Get the bounds. */
  SANE_Word w_surface[4];
  for(i=0; i<4; i++)
  {
    if(!xsane.well_known.coord[i])
    {
      /* All values should exist. */
      return 0;
    }

    const SANE_Option_Descriptor *opt = xsane_get_option_descriptor(xsane.dev, xsane.well_known.coord[i]);

    if(!i)
    {
      if(opt->type != SANE_TYPE_FIXED && opt->type != SANE_TYPE_INT)
      {
        /* Wrong type. */
        return 0;
      }

      *unit = opt->unit;
      *type = opt->type;
    }
    else
    {
      if(*unit != opt->unit ||
         *type != opt->type)
      {
        /* Values should have same types and units. */
        return 0;
      }
    }

    SANE_Word min, max;
    xsane_get_bounds_word(opt, &min, &max);
    w_surface[i] = (i & 2) ? max : min;
  }

  /* Max Ushakov 24-Apr-2020 : Canonscan 9000F Mark II for the 'photo films scanning' mode returns an image with reduced height when asked
     for scanning the full area (A4).
     There is a solution for this and similar problems: ask the scanner for the estimated size of image and reduce the full area to keep the ratios
     of image and area equal. */
  {
    /* Keep the current values and push bounds. */
    SANE_Word old_values[4];
    for(i=0; i<4; i++)
    {
      sane_control_option(xsane.dev, xsane.well_known.coord[i], SANE_ACTION_GET_VALUE, old_values + i, NULL);
      sane_control_option(xsane.dev, xsane.well_known.coord[i], SANE_ACTION_SET_VALUE, w_surface + i, NULL);
    }

    /* Now read the estimated size of image and shrink the surface to the same ratio. */
    SANE_Parameters params;
    if(sane_get_parameters(xsane.dev, &params) == SANE_STATUS_GOOD)
    {
      const double image_ratio = (double)params.pixels_per_line / params.lines;

      const double width = w_surface[2] - w_surface[0];
      const double height = w_surface[3] - w_surface[1];
      const double area_ratio = width / height;

      /* Check if ratios differse too much. */
      if(image_ratio > area_ratio + .01)
      {
        /* Reduce the height. */
        w_surface[3] = w_surface[1] + (SANE_Word) (width / image_ratio);
      }
      else if(image_ratio + .01 < area_ratio)
      {
        /* Reduce the width. */
        w_surface[2] = w_surface[0] + (SANE_Word) (height * image_ratio);
      }
    }

    /* Restore previous values. */
    for(i=0; i<4; i++)
    {
      sane_control_option(xsane.dev, xsane.well_known.coord[i], SANE_ACTION_SET_VALUE, old_values + i, NULL);
    }
  }

  /* Save the result. */
  if(*type == SANE_TYPE_INT)
  {
    for(i=0; i<4; i++)
    {
      surface[i] = (double) w_surface[i];
    }
  }
  else
  {
    for(i=0; i<4; i++)
    {
      surface[i] = (double) SANE_UNFIX(w_surface[i]);
    }
  }

  return 1;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_update_surface(Preview *p, int surface_changed)
{
  DBG(DBG_proc, "preview_update_surface\n");

  /* Obtain the full scanner surface. */
  double new_full_scanner_surface[4];
  SANE_Value_Type type;
  SANE_Unit unit;
  if(preview_calculate_full_scanner_surface(p, new_full_scanner_surface, &type, &unit))
  {
    /* It is ok. */
    p->show_selection = TRUE;
  }
  else
  {
    /* Something has failed, use default values. */
    p->show_selection = FALSE;
    unit = SANE_UNIT_PIXEL;
    type = SANE_TYPE_INT;
  }

  /* Check changes. */
  {
    int i;
    for(i=0; i<4; i++)
    {
      if(p->orig_scanner_surface[i] != new_full_scanner_surface[i])
      {
        surface_changed = 2;
        p->orig_scanner_surface[i] = new_full_scanner_surface[i];
      }
    }
  }

  /* Recalculate scanner_surface. */
  geom_set_surface(p->preset_surface, p->scanner_surface);
  geom_bound_surface_to_surface(p->scanner_surface, p->orig_scanner_surface);

  DBG(DBG_info, "preview_update_surface: scanner_surface: [%f; %f; %f; %f]\n",
      p->scanner_surface[0], p->scanner_surface[1], p->scanner_surface[2], p->scanner_surface[3]);

  if (p->surface_unit != unit)
  {
    surface_changed = 1;
    p->surface_unit = unit;
  }

  if (p->surface_type != type)
  {
    surface_changed = 1;
    p->surface_type = type;
  }

  if (surface_changed) /* establish new surface */
  {
    DBG(DBG_info, "preview_update_surface: establish new surface\n");

    if(p->current_zoom.zoom_mode == Zoom_mode_zoomed)
    {
      DBG(DBG_info, "preview_update_surface: calculate Zoom_mode_zoomed\n");

      /* Apply visible area to device coordinates, using the current parameters of zoom. */
      double visible_area[4];
      {
        double center_point_rot[2];
        geom_rotate_point(p, p->current_zoom.center_point, center_point_rot);

        const double win_dimension[] = { (double) p->preview_window_width, (double) p->preview_window_height };
        double win_half_dimension[2];
        geom_multiply_point_by_scalar(win_dimension, (double) (1. / 2. / p->current_zoom.zoom_factor), win_half_dimension);

        double visible_area_rot[4];
        geom_subtract_points(center_point_rot, win_half_dimension, visible_area_rot);
        geom_add_points(center_point_rot, win_half_dimension, visible_area_rot + 2);

        geom_rotate_back_surface(p, visible_area_rot, visible_area);

        DBG(DBG_info, "preview_update_surface: visible area before correction: [%f; %f; %f; %f]\n",
            visible_area[0], visible_area[1], visible_area[2], visible_area[3]);
      }

      /* Move visible area for covering empty gaps between image and borders of the window. */
      {
        DBG(DBG_info, "preview_update_surface: correct gaps\n");

        /* Gaps between window (device coordinates) and image. */
        double left = p->scanner_surface[0] - visible_area[0];
        double up = p->scanner_surface[1] - visible_area[1];
        double right = visible_area[2] - p->scanner_surface[2];
        double down = visible_area[3] - p->scanner_surface[3];

        int aligned_x = 0;
        int aligned_y = 0;

        double shift[2];

        if(left + right > 0.)
        {
          /* Align in X. */
          DBG(DBG_info, "preview_update_surface: gaps in X\n");

          aligned_x = 1;
          shift[0] = (double) ((left - right) / 2.);
        }
        else
        {
          if(right > 0.)
          {
            shift[0] = -right;
          }
          else
          {
            if(left > 0.)
            {
              shift[0] = left;
            }
            else
            {
              shift[0] = 0.;
            }
          }
        }

        if(up + down > 0.)
        {
          /* Align in Y. */
          aligned_y = 1;
          shift[1] = (double) ((up - down) / 2.);

          DBG(DBG_info, "preview_update_surface: gaps in Y\n");
        }
        else
        {
          if(down > 0.)
          {
            shift[1] = -down;
          }
          else
          {
            if(up > 0.)
            {
              shift[1] = up;
            }
            else
            {
              shift[1] = 0.;
            }
          }
        }

        /* If the entire window is visible, then switch to 'fit to window' is better now. */
        if((aligned_x || (fabs(left + shift[0]) < 2. && fabs(right - shift[0]) < 2.)) &&
           (aligned_y || (fabs(up + shift[1]) < 2. && fabs(down - shift[1]) < 2.))) {

          p->current_zoom.zoom_mode = Zoom_mode_fit_to_window;
          DBG(DBG_info, "preview_update_surface: switch to Zoom_mode_fit_to_window\n");
        }
        else
        {
          geom_set_surface(visible_area, p->image_surface);

          if(shift[0] != 0. || shift[1] != 0.)
          {
            /* Apply the shift. */
            geom_shift_surface_by_point(p->image_surface, shift);
            geom_shift_point_by_point(p->current_zoom.center_point, shift);
          }

          /* Finalize calculations. */
          geom_bound_surface_to_surface(p->image_surface, p->scanner_surface);
          geom_rotate_surface(p, p->image_surface, p->surface);

          DBG(DBG_info, "preview_update_surface: surface: [%f; %f; %f;%f]\n",
              p->surface[0], p->surface[1], p->surface[2], p->surface[3]);
        }
      }
    }

    if(p->current_zoom.zoom_mode == Zoom_mode_fit_to_window)
    {
      DBG(DBG_info, "preview_update_surface: calculate Zoom_mode_fit_to_window\n");

      /* All these surfaces are the same. */
      geom_set_surface(p->scanner_surface, p->image_surface);
      geom_rotate_surface(p, p->image_surface, p->surface);

      /* Calculate the center point. */
      geom_get_center_of_surface(p->image_surface, p->current_zoom.center_point);

      /* Calculate zoom factor. */
      const double aspect_surface_rot = geom_get_aspect_ratio(p->surface);
      const double aspect_window = (double)p->preview_window_width / (double) p->preview_window_height;
      if(aspect_window > aspect_surface_rot) {
        /* Fit by height. */
        p->current_zoom.zoom_factor = (double) p->preview_window_height / (double) geom_get_height_of_surface(p->surface);
      }
      else
      {
        /* Fit by width. */
        p->current_zoom.zoom_factor = (double) p->preview_window_width / (double) geom_get_width_of_surface(p->surface);
      }

      DBG(DBG_info, "preview_update_surface: zoom_factor: %f\n", p->current_zoom.zoom_factor);
    }

    /* Calculate dimensions of the preview in window.
       These are double values, but they are integer by nature and fit exactly to the pixel area. */
    {
      const int preview_width  = (int) (geom_get_width_of_surface(p->surface) * p->current_zoom.zoom_factor);
      const int preview_height = (int) (geom_get_height_of_surface(p->surface) * p->current_zoom.zoom_factor);

      p->win_image_surface[0] = (p->preview_window_width - preview_width) / 2.;
      p->win_image_surface[1] = (p->preview_window_height - preview_height) / 2;

      p->win_image_surface[2] = p->win_image_surface[0] + (double) preview_width + .1;
      p->win_image_surface[3] = p->win_image_surface[1] + (double) preview_height + .1;

      DBG(DBG_info, "preview_update_surface: wim_image_surface = [%f; %f; %f; %f]\n",
          p->win_image_surface[0], p->win_image_surface[1], p->win_image_surface[2], p->win_image_surface[3]);
    }

    /* Update the buffer for row.*/
    if (p->preview_row)
    {
      p->preview_row = realloc(p->preview_row, (size_t) (3 * p->preview_window_width));
    }
    else
    {
      p->preview_row = malloc(3 * (size_t) (p->preview_window_width));
    }

    preview_redraw_rulers(p);   /* correct rulers */
    preview_restore_image(p); /* load scanned image */

    preview_display_with_correction(p); /* draw preview */
    xsane_update_histogram(TRUE); /* update raw */

    p->previous_selection.active = FALSE;
    p->previous_selection_maximum.active = FALSE;
    preview_bound_selection(p);	 /* make sure selection is not larger than surface */
    preview_draw_selection(p);   /* the selection is overpainted, we have to update it */
    preview_establish_selection(p); /* send selection to backend, it may be changed */
  }
  else /* leave everything like it is */
  {
    DBG(DBG_info, "preview_update_surface: surface unchanged\n");
    preview_update_selection(p);
  }

  if (p->show_selection)
  {
    gtk_widget_set_sensitive(p->preset_area_option_menu, TRUE); /* enable preset area */
    gtk_widget_set_sensitive(p->full_area,  TRUE); /* enable selection buttons */
    gtk_widget_set_sensitive(p->autoselect, TRUE);
  }
  else
  {
    gtk_widget_set_sensitive(p->preset_area_option_menu, FALSE); /* disable preset area */
    gtk_widget_set_sensitive(p->full_area,  FALSE); /* no selection */
    gtk_widget_set_sensitive(p->autoselect, FALSE); /* no selection */
  }

  preview_update_zoom_buttons(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

/* preview_scan is called once when the "Preview scan" button is pressed */
void preview_scan(Preview *p)
{
 double min, max, dpi = 0;
 const SANE_Option_Descriptor *opt;
 int i;
 int gamma_gray_size  = 256; /* set this values to image depth for more than 8bpp input support!!! */
 int gamma_red_size   = 256;
 int gamma_green_size = 256;
 int gamma_blue_size  = 256;
 int gamma_gray_max   = 255; /* set this to to image depth for more than 8bpp output support */
 int gamma_red_max    = 255;
 int gamma_green_max  = 255;
 int gamma_blue_max   = 255;

  DBG(DBG_proc, "preview_scan\n");

  /* we are overpainting the image, so we do not have any visible selections */
  p->previous_selection.active = FALSE;
  p->previous_selection_maximum.active = FALSE;

  xsane.block_update_param = TRUE; /* do not change parameters each time */

  preview_save_option(p, xsane.well_known.dpi,      &p->saved_dpi,      &p->saved_dpi_valid);
  preview_save_option(p, xsane.well_known.dpi_x,    &p->saved_dpi_x,    &p->saved_dpi_x_valid);
  preview_save_option(p, xsane.well_known.dpi_y,    &p->saved_dpi_y,    &p->saved_dpi_y_valid);
  preview_save_option(p, xsane.well_known.scanmode, &p->saved_scanmode, &p->saved_scanmode_valid);

  for (i = 0; i < 4; ++i)
  {
    preview_save_option(p, xsane.well_known.coord[i], &p->saved_coord[i], p->saved_coord_valid + i);
  }

  preview_save_option(p, xsane.well_known.bit_depth, &p->saved_bit_depth, &p->saved_bit_depth_valid);

  /* determine dpi, if necessary: */

  if (xsane.well_known.dpi > 0)
  {
    opt = xsane_get_option_descriptor(xsane.dev, xsane.well_known.dpi);

    dpi = MM_PER_INCH * p->current_zoom.zoom_factor * preferences.preview_oversampling;

    xsane_get_bounds_double(opt, &min, &max);

    if (dpi < min)
    {
      dpi = min;
    }

    if (dpi > max)
    {
      dpi = max;
    }

    xsane_set_resolution(xsane.well_known.dpi,   dpi); /* set resolution to dpi or next higher value that is available */
    xsane_set_resolution(xsane.well_known.dpi_x, dpi); /* set resolution to dpi or next higher value that is available */
    xsane_set_resolution(xsane.well_known.dpi_y, dpi); /* set resolution to dpi or next higher value that is available */
  }

  for (i = 0; i < 4; ++i)
  {
    preview_set_option_double(p, xsane.well_known.coord[i], p->image_surface[i]);
  }

  preview_set_option_val(p, xsane.well_known.preview, SANE_TRUE);

  if ( (xsane.grayscale_scanmode) && (xsane.param.depth == 1) && (xsane.lineart_mode == XSANE_LINEART_GRAYSCALE) )
  {
    preview_set_option(p, xsane.well_known.scanmode, xsane.grayscale_scanmode);
  }

#if 0
  if ( (p->saved_bit_depth == 16) && (p->saved_bit_depth_valid) ) /* don't scan with 16 bpp */
  {
    preview_set_option_val(p, xsane.well_known.bit_depth, 8);
  }
#endif


  if (xsane.well_known.gamma_vector >0)
  {
   const SANE_Option_Descriptor *opt;
 
    opt = xsane_get_option_descriptor(xsane.dev, xsane.well_known.gamma_vector);
    if (SANE_OPTION_IS_ACTIVE(opt->cap))
    {
     SANE_Int *gamma_data;

      opt = xsane_get_option_descriptor(xsane.dev, xsane.well_known.gamma_vector);
      gamma_gray_size = opt->size / (int) sizeof(opt->type);
      gamma_gray_max  = opt->constraint.range->max;

      gamma_data = malloc((size_t) gamma_gray_size  * sizeof(SANE_Int));

      if ((xsane.xsane_channels > 1) || (xsane.no_preview_medium_gamma)) /* color scan or medium preview gamma disabled */
      {
        xsane_create_gamma_curve(gamma_data, 0, 1.0, 0.0, 0.0, 0.0, 100.0, 1.0, gamma_gray_size, gamma_gray_max);
      }
      else /* grayscale scan */
      {
        xsane_create_gamma_curve(gamma_data, xsane.medium_negative, 1.0, 0.0, 0.0,
                                 xsane.medium_shadow_gray, xsane.medium_highlight_gray, xsane.medium_gamma_gray,
                                 gamma_gray_size, gamma_gray_max);
      }
      
      xsane_back_gtk_update_vector(xsane.well_known.gamma_vector, gamma_data);
      free(gamma_data);
    }
  }

  if (xsane.well_known.gamma_vector_r >0)
  {
   const SANE_Option_Descriptor *opt;

    opt = xsane_get_option_descriptor(xsane.dev, xsane.well_known.gamma_vector_r);
    if (SANE_OPTION_IS_ACTIVE(opt->cap))
    {
     SANE_Int *gamma_data_red, *gamma_data_green, *gamma_data_blue;

      opt = xsane_get_option_descriptor(xsane.dev, xsane.well_known.gamma_vector_r);
      gamma_red_size = opt->size / (int) sizeof(opt->type);
      gamma_red_max  = opt->constraint.range->max;

      opt = xsane_get_option_descriptor(xsane.dev, xsane.well_known.gamma_vector_g);
      gamma_green_size = opt->size / (int) sizeof(opt->type);
      gamma_green_max  = opt->constraint.range->max;

      opt = xsane_get_option_descriptor(xsane.dev, xsane.well_known.gamma_vector_b);
      gamma_blue_size = opt->size / (int) sizeof(opt->type);
      gamma_blue_max  = opt->constraint.range->max;

      gamma_data_red   = malloc((size_t) gamma_red_size   * sizeof(SANE_Int));
      gamma_data_green = malloc((size_t) gamma_green_size * sizeof(SANE_Int));
      gamma_data_blue  = malloc((size_t) gamma_blue_size  * sizeof(SANE_Int));

      if (xsane.no_preview_medium_gamma) /* do not use medium gamma for preview */
      {
        DBG(DBG_info, "preview: not using medium gamma table\n");

        xsane_create_gamma_curve(gamma_data_red,   0, 1.0, 0.0, 0.0, 0.0, 100.0, 1.0, gamma_red_size,   gamma_red_max);
        xsane_create_gamma_curve(gamma_data_green, 0, 1.0, 0.0, 0.0, 0.0, 100.0, 1.0, gamma_green_size, gamma_green_max);
        xsane_create_gamma_curve(gamma_data_blue,  0, 1.0, 0.0, 0.0, 0.0, 100.0, 1.0, gamma_blue_size,  gamma_blue_max);
      }
      else /* use medium gamma for preview */
      {
        DBG(DBG_info, "preview: using medium gamma table\n");

        xsane_create_gamma_curve(gamma_data_red,   xsane.medium_negative, 1.0, 0.0, 0.0,
                                 xsane.medium_shadow_red, xsane.medium_highlight_red, xsane.medium_gamma_red,
                                 gamma_red_size,   gamma_red_max);
        xsane_create_gamma_curve(gamma_data_green, xsane.medium_negative, 1.0, 0.0, 0.0,
                                 xsane.medium_shadow_green, xsane.medium_highlight_green, xsane.medium_gamma_green,
                                 gamma_green_size, gamma_green_max);
        xsane_create_gamma_curve(gamma_data_blue,  xsane.medium_negative, 1.0, 0.0, 0.0,
                                 xsane.medium_shadow_blue, xsane.medium_highlight_blue, xsane.medium_gamma_blue,
                                 gamma_blue_size,  gamma_blue_max);
      }

      xsane_back_gtk_update_vector(xsane.well_known.gamma_vector_r, gamma_data_red);
      xsane_back_gtk_update_vector(xsane.well_known.gamma_vector_g, gamma_data_green);
      xsane_back_gtk_update_vector(xsane.well_known.gamma_vector_b, gamma_data_blue);

      free(gamma_data_red);
      free(gamma_data_green);
      free(gamma_data_blue);
    }
  }


  xsane.block_update_param = FALSE;
  p->preview_channels  = xsane.xsane_channels;
  p->scan_incomplete = FALSE;
  p->invalid = TRUE; /* no valid preview */
  p->scanning = TRUE;

  preview_display_valid(p);


  xsane_clear_histogram(&xsane.histogram_raw);
  xsane_clear_histogram(&xsane.histogram_enh);


  /* OK, all set to go */
  preview_scan_start(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_save_image_file(const Preview *p, FILE *out)
{
  DBG(DBG_proc, "preview_save_image_file\n");

  if (out)
  {
    /* always save it as a 16 bit PPM image: */
    fprintf(out, "P6\n"
                 "# surface: %g %g %g %g %u %u\n"
                 "# time: %d\n"
                 "%d %d\n65535\n",
                 p->image_surface[0], p->image_surface[1], p->image_surface[2], p->image_surface[3],
                 p->surface_type, p->surface_unit,
                 (int) time(NULL),
                 p->image_width, p->image_height);

    fwrite(p->image_data_raw, 6, (size_t) (p->image_width*p->image_height), out);
    fclose(out);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_save_image(const Preview *p)
{
 FILE *out;
 int level=0;

  DBG(DBG_proc, "preview_save_image\n");

  if (!p->image_data_raw)
  {
    return;
  }

  if ( GROSSLY_EQUAL(p->orig_scanner_surface[0], p->image_surface[0]) && /* full device surface */
       GROSSLY_EQUAL(p->orig_scanner_surface[1], p->image_surface[1]) &&
       GROSSLY_EQUAL(p->orig_scanner_surface[2], p->image_surface[2]) &&
       GROSSLY_EQUAL(p->orig_scanner_surface[3], p->image_surface[3]) )
  {
    level = 0;
  }
  else if ( GROSSLY_EQUAL(p->scanner_surface[0], p->image_surface[0]) && /* user defined surface */
            GROSSLY_EQUAL(p->scanner_surface[1], p->image_surface[1]) &&
            GROSSLY_EQUAL(p->scanner_surface[2], p->image_surface[2]) &&
            GROSSLY_EQUAL(p->scanner_surface[3], p->image_surface[3]) )
  {
    level = 1;
  }
  else /* zoom area */
  {
    level = 2;
  }

  if (p->filename[level])
  {
    /* save preview image */
    out = fopen(p->filename[level], "wb"); /* b = binary mode for win32*/

    preview_save_image_file(p, out);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_delete_images(Preview *p)
{
 FILE *out;
 int level=0;

  DBG(DBG_proc, "preview_delete_images_file\n");

  for (level = 0; level<3; level++)
  {
    out = fopen(p->filename[level], "wb"); /* b = binary mode for win32*/
    if (out)
    fclose(out);
  }
  preview_update_surface(p, 1);

  xsane_batch_scan_update_icon_list();
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_destroy(Preview *p)
{
 int level;

  DBG(DBG_proc, "preview_destroy\n");

  if (p->scanning)
  {
    preview_scan_done(p, 0);		/* don't save partial window */
  }

  for(level = 0; level <= 2; level++)
  {
    if (p->filename[level])
    {
      remove(p->filename[level]); /* remove existing preview */
    }
  }

  if (p->image_data_enh)
  {
    free(p->image_data_enh);
    p->image_data_enh = 0;
  }

  if (p->image_data_raw)
  {
    free(p->image_data_raw);
    p->image_data_raw = 0;
  }

  if (p->preview_row)
  {
    free(p->preview_row);
    p->preview_row = 0;
  }

  if (p->gc_selection)
  {
    gdk_gc_unref(p->gc_selection);
  }

  if (p->gc_selection_maximum)
  {
    gdk_gc_unref(p->gc_selection_maximum);
  }

  if (p->top)
  {
    gtk_widget_destroy(p->top);
  }
  free(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Zoom to selection. */
static void preview_zoom_area(GtkWidget *window, gpointer data)
{
  Preview *p=data;

  DBG(DBG_proc, "preview_zoom_area\n");

  double selection_dim[2];
  geom_get_dimensions_of_surface(p->selection.coordinate, selection_dim);
  if(selection_dim[0] <= 0. || selection_dim[1] <= 0.) {
    // Wrong input.
    return;
  }

  p->previous_zoom = p->current_zoom;

  p->current_zoom.zoom_mode = Zoom_mode_zoomed;
  geom_get_center_of_surface(p->selection.coordinate, p->current_zoom.center_point);
  p->current_zoom.zoom_factor = fmin((double) p->preview_window_width / selection_dim[0], (double) p->preview_window_height / selection_dim[1]);

  preview_update_surface(p, 1);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Disable zoom, i.e. switch to 'fit to window' mode. */
static void preview_zoom_not(GtkWidget *window, gpointer data)
{
  Preview *p=data;

  DBG(DBG_proc, "preview_zoom_not\n");

  p->previous_zoom = p->current_zoom;

  p->current_zoom.zoom_mode = Zoom_mode_fit_to_window;

  preview_update_surface(p, 1);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Zoom out. */
static void preview_zoom_out(GtkWidget *window, gpointer data)
{
  Preview *p=data;

  DBG(DBG_proc, "preview_zoom_out\n");

  p->previous_zoom = p->current_zoom;

  p->current_zoom.zoom_mode = Zoom_mode_zoomed;
  p->current_zoom.zoom_factor /= 1.2;

  preview_update_surface(p, 1);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Zoom around given point. */
static void preview_select_zoom_point(Preview *p, int preview_x, int preview_y)
{
  double dev_point[2];

  DBG(DBG_proc, "preview_select_zoom_point(%d, %d)\n", preview_x, preview_y);

  preview_transform_coordinate_window_to_device(p, (double) preview_x, (double) preview_y, dev_point);

  if(!geom_check_point_in_surface(dev_point, p->scanner_surface)) {
    /* Out of image, do nothing. */
    return;
  }

  p->previous_zoom = p->current_zoom;

  p->current_zoom.zoom_mode = Zoom_mode_zoomed;
  p->current_zoom.center_point[0] = dev_point[0] + (p->current_zoom.center_point[0] - dev_point[0]) / 1.2;
  p->current_zoom.center_point[1] = dev_point[1] + (p->current_zoom.center_point[1] - dev_point[1]) / 1.2;
  p->current_zoom.zoom_factor *= 1.2;

  preview_update_surface(p, 1);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_zoom_undo(GtkWidget *window, gpointer data)
{
  Preview *p=data;

  DBG(DBG_proc, "preview_zoom_undo\n");

  p->current_zoom = p->previous_zoom;
  p->previous_zoom.zoom_mode = Zoom_mode_undefined;

  preview_update_surface(p, 1);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_update_zoom_buttons(Preview *p) {
  /* Undo. */
  gtk_widget_set_sensitive(p->zoom_undo, p->previous_zoom.zoom_mode == Zoom_mode_undefined ? FALSE : TRUE);

  /* Fit to view. */
  gtk_widget_set_sensitive(p->zoom_not, p->current_zoom.zoom_mode == Zoom_mode_fit_to_window ? FALSE : TRUE);

  /* Zoom out. */
  gtk_widget_set_sensitive(p->zoom_out, p->current_zoom.zoom_mode == Zoom_mode_fit_to_window ? FALSE : TRUE);

  /* Check if any further Zoom in should be stopped.
     The limit is estimated as 'one pixel of image to 2*2 pixels of window when the resolution is 9600dpi. */
  int bEnableZoomIn = (p->current_zoom.zoom_factor < 756.) ? TRUE : FALSE;

  /* Zoom in. */
  gtk_widget_set_sensitive(p->zoom_in, bEnableZoomIn);

  /* Also check if the current mode is still MODE_ZOOM_IN. */
  if(!bEnableZoomIn && p->mode == MODE_ZOOM_IN)
  {
    p->mode = MODE_NORMAL;
  }

  /* Zoom to area. */
  gtk_widget_set_sensitive(p->zoom_area, p->selection.active);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_get_color(const Preview *p, int x, int y, int range, int *red, int *green, int *blue)
{
 int image_x, image_y;
 int image_x_min, image_y_min;
 int image_x_max, image_y_max;
 int offset;
 int count = 0;

  DBG(DBG_proc, "preview_get_color\n");

  if (p->image_data_raw)
  {
    double point_coord[2];
    point_coord[0] = (double) x;
    point_coord[1] = (double) y;

    double image_coord[2];
    preview_transform_coordinate_window_to_image(p, point_coord, image_coord);
    image_x = (int) image_coord[0];
    image_y = (int) image_coord[1];

    if ( (image_x >= 0) && (image_x < p->image_width) && (image_y >=0) && (image_y < p->image_height) )
    {
      image_x_min = image_x - range/2;
      image_y_min = image_y - range/2;
      image_x_max = image_x + range/2;
      image_y_max = image_y + range/2;

      xsane_bound_int(&image_x_min, 0, p->image_width - 1);
      xsane_bound_int(&image_x_max, 0, p->image_width - 1);
      xsane_bound_int(&image_y_min, 0, p->image_height - 1);
      xsane_bound_int(&image_y_max, 0, p->image_height - 1);

      *red   = 0;
      *green = 0;
      *blue  = 0;

      for (image_x = image_x_min; image_x <= image_x_max; image_x++)
      {
        for (image_y = image_y_min; image_y <= image_y_max; image_y++)
        {
          count++;

          offset = 3 * (image_y * p->image_width + image_x);
 
          if (!xsane.negative) /* positive */
          {
            *red   += (p->image_data_raw[offset    ]) >> 8;
            *green += (p->image_data_raw[offset + 1]) >> 8;
            *blue  += (p->image_data_raw[offset + 2]) >> 8;
          }
          else /* negative */
          {
            *red   += 255 - (p->image_data_raw[offset    ] >> 8);
            *green += 255 - (p->image_data_raw[offset + 1] >> 8);
            *blue  += 255 - (p->image_data_raw[offset + 2] >> 8);
          }
        }
      }
      if (count)
      {
        *red   /= count;
        *green /= count;
        *blue  /= count;
      }
      else
      {
        DBG(DBG_error, "preview_get_color: image is empty\n");
      }
    }
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_add_batch(GtkWidget *window, Preview *data)
{
  DBG(DBG_proc, "preview_add_batch\n");

  xsane_batch_scan_add(); /* add active settings to batch list */

#if 0
  preview_draw_selection(p); /* read selection from backend: correct rotation */
  preview_establish_selection(p); /* read selection from backend: correct rotation */
#endif
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_pipette_white(GtkWidget *window, gpointer data)
{
 Preview *p=data;
 GdkCursor *cursor;
 GdkColor fg;
 GdkColor bg;
 GdkPixmap *pixmap;
 GdkPixmap *mask;

  DBG(DBG_proc, "preview_pipette_white\n");

  p->mode = MODE_PIPETTE_WHITE;

  pixmap = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_pipette_white, CURSOR_PIPETTE_WIDTH, CURSOR_PIPETTE_HEIGHT);
  mask   = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_pipette_mask,  CURSOR_PIPETTE_WIDTH, CURSOR_PIPETTE_HEIGHT);

  fg.red   = 0;
  fg.green = 0;
  fg.blue  = 0;

  bg.red   = 65535;
  bg.green = 65535;
  bg.blue  = 65535;

  cursor = gdk_cursor_new_from_pixmap(pixmap, mask, &fg, &bg, CURSOR_PIPETTE_HOT_X, CURSOR_PIPETTE_HOT_Y);

  gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
  gdk_cursor_unref(cursor);
  p->cursornr = -1;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_pipette_gray(GtkWidget *window, gpointer data)
{
 Preview *p=data;
 GdkCursor *cursor;
 GdkColor fg;
 GdkColor bg;
 GdkPixmap *pixmap;
 GdkPixmap *mask;

  DBG(DBG_proc, "preview_pipette_gray\n");

  p->mode = MODE_PIPETTE_GRAY;

  pixmap = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_pipette_gray, CURSOR_PIPETTE_WIDTH, CURSOR_PIPETTE_HEIGHT);
  mask   = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_pipette_mask, CURSOR_PIPETTE_WIDTH, CURSOR_PIPETTE_HEIGHT);

  fg.red   = 0;
  fg.green = 0;
  fg.blue  = 0;

  bg.red   = 65535;
  bg.green = 65535;
  bg.blue  = 65535;

  cursor = gdk_cursor_new_from_pixmap(pixmap, mask, &fg, &bg, CURSOR_PIPETTE_HOT_X, CURSOR_PIPETTE_HOT_Y);

  gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
  gdk_cursor_unref(cursor);
  p->cursornr = -1;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_pipette_black(GtkWidget *window, gpointer data)
{
 Preview *p=data;
 GdkCursor *cursor;
 GdkColor fg;
 GdkColor bg;
 GdkPixmap *pixmap;
 GdkPixmap *mask;

  DBG(DBG_proc, "preview_pipette_black\n");

  p->mode = MODE_PIPETTE_BLACK;

  pixmap = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_pipette_black, CURSOR_PIPETTE_WIDTH, CURSOR_PIPETTE_HEIGHT);
  mask   = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_pipette_mask , CURSOR_PIPETTE_WIDTH, CURSOR_PIPETTE_HEIGHT);

  fg.red   = 0;
  fg.green = 0;
  fg.blue  = 0;

  bg.red   = 65535;
  bg.green = 65535;
  bg.blue  = 65535;

  cursor = gdk_cursor_new_from_pixmap(pixmap, mask, &fg, &bg, CURSOR_PIPETTE_HOT_X, CURSOR_PIPETTE_HOT_Y);

  gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
  gdk_cursor_unref(cursor);
  p->cursornr = -1;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_init_autoraise_scan_area(GtkWidget *window, gpointer data)
{
 Preview *p=data;
 GdkCursor *cursor;
 GdkColor fg;
 GdkColor bg;
 GdkPixmap *pixmap;
 GdkPixmap *mask;

  DBG(DBG_proc, "preview_init_autoraise_scan_area\n");

  p->mode = MODE_AUTORAISE_SCAN_AREA;

  pixmap = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_autoraise_scan_area,      CURSOR_AUTORAISE_SCAN_AREA_WIDTH, CURSOR_AUTORAISE_SCAN_AREA_HEIGHT);
  mask   = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_autoraise_scan_area_mask, CURSOR_AUTORAISE_SCAN_AREA_WIDTH, CURSOR_AUTORAISE_SCAN_AREA_HEIGHT);

  fg.red   = 0;
  fg.green = 0;
  fg.blue  = 0;

  bg.red   = 65535;
  bg.green = 65535;
  bg.blue  = 65535;

  cursor = gdk_cursor_new_from_pixmap(pixmap, mask, &fg, &bg, CURSOR_AUTORAISE_SCAN_AREA_HOT_X, CURSOR_AUTORAISE_SCAN_AREA_HOT_Y);

  gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
  gdk_cursor_unref(cursor);
  p->cursornr = -1;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_zoom_in(GtkWidget *window, gpointer data)
{
 Preview *p=data;
 GdkCursor *cursor;
 GdkColor fg;
 GdkColor bg;
 GdkPixmap *pixmap;
 GdkPixmap *mask;

  DBG(DBG_proc, "preview_zoom\n");

  p->mode = MODE_ZOOM_IN;

  pixmap = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_zoom,      CURSOR_ZOOM_WIDTH, CURSOR_ZOOM_HEIGHT);
  mask   = gdk_bitmap_create_from_data(gtk_widget_get_window(p->top), cursor_zoom_mask, CURSOR_ZOOM_WIDTH, CURSOR_ZOOM_HEIGHT);

  fg.red   = 0;
  fg.green = 0;
  fg.blue  = 0;

  bg.red   = 65535;
  bg.green = 65535;
  bg.blue  = 65535;

  cursor = gdk_cursor_new_from_pixmap(pixmap, mask, &fg, &bg, CURSOR_ZOOM_HOT_X, CURSOR_ZOOM_HOT_Y);

  gdk_window_set_cursor(gtk_widget_get_window(p->window), cursor);
  gdk_cursor_unref(cursor);
  p->cursornr = -1;
}
/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_select_full_preview_area(Preview *p)
{
 int i;

  DBG(DBG_proc, "preview_select_full_preview_area\n");

  p->selection.active = TRUE;

  for (i=0; i<4; i++)
  {
    p->selection.coordinate[i] = p->surface[i];
  }

  preview_update_maximum_output_size(p);
  preview_draw_selection(p);
  preview_establish_selection(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_full_preview_area_callback(GtkWidget *widget, gpointer call_data)
{
 Preview *p = call_data;

  DBG(DBG_proc, "preview_full_preview_area_callback\n");

  preview_select_full_preview_area(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_135_film_half_frame_area_callback(GtkWidget *widget, gpointer call_data)
{
  DBG(DBG_proc, "preview_135_film_half_frame_area_callback\n");

  Preview *p = call_data;

  // Size of the frame is 24x18 mm.
  double half_size[2];
  half_size[0] = 24. / 2.;
  half_size[1] = 18. / 2.;

  double center_of_window[2];
  geom_get_center_of_surface(p->image_surface, center_of_window);

  double new_selection[4];
  geom_subtract_points(center_of_window, half_size, new_selection);
  geom_add_points(center_of_window, half_size, new_selection + 2);

  /* Check it for almost unreal conditions. */
  if(!geom_check_point_in_surface(new_selection, p->scanner_surface) ||
     !geom_check_point_in_surface(new_selection + 2, p->scanner_surface))
  {
    DBG(DBG_proc, "suddenly, out of area\n");
    return;
  }

  p->selection.active = TRUE;
  geom_set_surface(new_selection, p->selection.coordinate);
  preview_draw_selection(p);
  preview_establish_selection(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_delete_images_callback(GtkWidget *widget, gpointer call_data)
{
 Preview *p = call_data;

  DBG(DBG_proc, "preview_delete_images_callback\n");

  preview_delete_images(p);
  p->invalid = TRUE;
  preview_display_valid(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_preset_area_rename_callback(GtkWidget *widget, GtkWidget *preset_area_widget)
{
 int selection;
 char *oldname;
 char *newname;
 Preview *p;
 GtkWidget *old_preset_area_menu;
 guint old_selection;

  DBG(DBG_proc, "preview_preset_area_rename_callback\n");

  selection = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(preset_area_widget), "Selection"));
  p = (Preview *) g_object_get_data(G_OBJECT(preset_area_widget), "Preview");

  DBG(DBG_info ,"rename %s\n", preferences.preset_area[selection]->name);

  /* set menu in correct state, is a bit strange this way but I do not have a better idea */
  old_preset_area_menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(p->preset_area_option_menu));
  old_selection = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(gtk_menu_get_active(GTK_MENU(old_preset_area_menu))), "Selection"));
  gtk_menu_popdown(GTK_MENU(old_preset_area_menu));
  gtk_option_menu_set_history(GTK_OPTION_MENU(p->preset_area_option_menu), old_selection);

  oldname = strdup(preferences.preset_area[selection]->name);

  if (!xsane_front_gtk_getname_dialog(WINDOW_PRESET_AREA_RENAME, DESC_PRESET_AREA_RENAME, oldname, &newname))
  {
    gtk_option_menu_remove_menu(GTK_OPTION_MENU(p->preset_area_option_menu));

    if (GTK_IS_WIDGET(old_preset_area_menu)) /* the menu normally is closed when we come here */
    {
      gtk_widget_destroy(old_preset_area_menu);
    }

    free(preferences.preset_area[selection]->name);
    preferences.preset_area[selection]->name = strdup(newname);
    DBG(DBG_info, "renaming %s to %s\n", oldname, newname);

    preview_create_preset_area_menu(p, old_selection);
  }
 
  free(oldname);
  free(newname);
 
  xsane_set_sensitivity(TRUE);

  return TRUE; /* event is handled */
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_preset_area_add_callback(GtkWidget *widget, GtkWidget *preset_area_widget)
{
 int selection, i;
 guint old_selection = 0;
 Preview *p;
 double coord[4];
 char suggested_name[PATH_MAX];
 char *newname;
 GtkWidget *old_preset_area_menu;

  DBG(DBG_proc, "preview_preset_area_add_callback\n");

  selection = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(preset_area_widget), "Selection"));
  p = (Preview *) g_object_get_data(G_OBJECT(preset_area_widget), "Preview");

  /* set menu in correct state, is a bit strange this way but I do not have a better idea */
  old_preset_area_menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(p->preset_area_option_menu));
  old_selection = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(gtk_menu_get_active(GTK_MENU(old_preset_area_menu))), "Selection"));
  gtk_menu_popdown(GTK_MENU(old_preset_area_menu));
  gtk_option_menu_set_history(GTK_OPTION_MENU(p->preset_area_option_menu), old_selection);

  /* suggest name = size in mm */
  geom_rotate_back_surface(p, p->selection.coordinate, coord);
  snprintf(suggested_name, sizeof(suggested_name), "%d mm x %d mm", (int) (coord[2]-coord[0]), (int) (coord[3]-coord[1]));

  if (!xsane_front_gtk_getname_dialog(WINDOW_PRESET_AREA_ADD, DESC_PRESET_AREA_ADD, suggested_name, &newname))
  {
    preferences.preset_area = realloc(preferences.preset_area, (size_t) (preferences.preset_area_definitions+1) * sizeof(void *));

    /* shift all items after selection */
    for (i = preferences.preset_area_definitions-1; i > selection; i--)
    {
      preferences.preset_area[i+1] = preferences.preset_area[i];
    }

    /* insert new item behind selected item */
    preferences.preset_area[selection+1] = calloc(sizeof(Preferences_preset_area_t), 1);
    preferences.preset_area[selection+1]->name    = strdup(newname);
    preferences.preset_area[selection+1]->xoffset = coord[0];
    preferences.preset_area[selection+1]->yoffset = coord[1];
    preferences.preset_area[selection+1]->width   = coord[2] - coord[0];
    preferences.preset_area[selection+1]->height  = coord[3] - coord[1];

    DBG(DBG_proc, "added %s\n", newname);

    preferences.preset_area_definitions++;

    preview_create_preset_area_menu(p, old_selection);
  }

  return TRUE; /* event is handled */
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_preset_area_delete_callback(GtkWidget *widget, GtkWidget *preset_area_widget)
{
 int selection, i;
 guint old_selection = 0;
 Preview *p;
 GtkWidget *old_preset_area_menu;

  DBG(DBG_proc, "preview_preset_area_delete_callback\n");

  selection = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(preset_area_widget), "Selection"));
  p = (Preview *) g_object_get_data(G_OBJECT(preset_area_widget), "Preview");


  if (selection) /* full size can not be deleted */
  {
    DBG(DBG_info ,"deleting %s\n", preferences.preset_area[selection]->name);

    free(preferences.preset_area[selection]);

    for (i=selection; i<preferences.preset_area_definitions-1; i++)
    {
      preferences.preset_area[i] = preferences.preset_area[i+1];
    }

    preferences.preset_area_definitions--;

    old_preset_area_menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(p->preset_area_option_menu));

    gtk_option_menu_remove_menu(GTK_OPTION_MENU(p->preset_area_option_menu));
    old_selection = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(gtk_menu_get_active(GTK_MENU(old_preset_area_menu))), "Selection"));

    if (old_selection == selection) /* we are deleting the selected surface */
    {
      old_selection = 0;
    }
    else if (old_selection > selection) /* we are deleting the selected surface */
    {
      old_selection--;
    }

    gtk_widget_destroy(old_preset_area_menu);

    preview_create_preset_area_menu(p, old_selection); /* build menu and set default to 0=full size */
  }

  return TRUE; /* event is handled */
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_preset_area_move_up_callback(GtkWidget *widget, GtkWidget *preset_area_widget)
{
 int selection;
 guint old_selection = 0;
 Preview *p;
 GtkWidget *old_preset_area_menu;

  DBG(DBG_proc, "preview_preset_area_move_up_callback\n");

  selection = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(preset_area_widget), "Selection"));
  p = (Preview *) g_object_get_data(G_OBJECT(preset_area_widget), "Preview");

  if (selection > 1) /* make sure "full area" stays at top */
  {
   Preferences_preset_area_t *help_area;

    DBG(DBG_info ,"moving up %s\n", preferences.preset_area[selection]->name);

    help_area = preferences.preset_area[selection-1];
    preferences.preset_area[selection-1] = preferences.preset_area[selection];
    preferences.preset_area[selection] = help_area;

    old_preset_area_menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(p->preset_area_option_menu));

    gtk_option_menu_remove_menu(GTK_OPTION_MENU(p->preset_area_option_menu));
    old_selection = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(gtk_menu_get_active(GTK_MENU(old_preset_area_menu))), "Selection"));

    if (old_selection == selection)
    {
      old_selection--;
    }
    else if (old_selection == selection-1)
    {
      old_selection++;
    }

    gtk_widget_destroy(old_preset_area_menu);

    preview_create_preset_area_menu(p, old_selection); /* build menu and set default to 0=full size */
  }

  return TRUE; /* event is handled */
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_preset_area_move_down_callback(GtkWidget *widget, GtkWidget *preset_area_widget)
{
 int selection;
 guint old_selection = 0;
 Preview *p;
 GtkWidget *old_preset_area_menu;

  DBG(DBG_proc, "preview_preset_area_move_down_callback\n");

  selection = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(preset_area_widget), "Selection"));
  p = (Preview *) g_object_get_data(G_OBJECT(preset_area_widget), "Preview");

  /* full size can not moved down */
  if ((selection) && (selection < preferences.preset_area_definitions-1))
  {
   Preferences_preset_area_t *help_area;

    DBG(DBG_info ,"moving down %s\n", preferences.preset_area[selection]->name);

    help_area = preferences.preset_area[selection];
    preferences.preset_area[selection] = preferences.preset_area[selection+1];
    preferences.preset_area[selection+1] = help_area;

    old_preset_area_menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(p->preset_area_option_menu));

    gtk_option_menu_remove_menu(GTK_OPTION_MENU(p->preset_area_option_menu));
    old_selection = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(gtk_menu_get_active(GTK_MENU(old_preset_area_menu))), "Selection"));

    if (old_selection == selection)
    {
      old_selection++;
    }
    else if (old_selection == selection+1)
    {
      old_selection--;
    }

    gtk_widget_destroy(old_preset_area_menu);

    preview_create_preset_area_menu(p, old_selection); /* build menu and set default to 0=full size */
  }

  return TRUE; /* event is handled */
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static gint preview_preset_area_context_menu_callback(GtkWidget *widget, GdkEvent *event)
{
 GtkWidget *menu;
 GtkWidget *menu_item;
 GdkEventButton *event_button;
 int selection;
 char buf[TEXTBUFSIZE];

  DBG(DBG_proc, "preview_preset_area_context_menu_callback\n");

  selection = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "Selection"));

  if (event->type == GDK_BUTTON_PRESS)
  {
    event_button = (GdkEventButton *) event;

    if (event_button->button == 3)
    {
      menu = gtk_menu_new();

      /** add selection */
      menu_item = gtk_menu_item_new_with_label(MENU_ITEM_PRESET_AREA_ADD_SEL);
      gtk_widget_show(menu_item);
      gtk_container_add(GTK_CONTAINER(menu), menu_item);
      g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(preview_preset_area_add_callback), widget);

      /* add separator */
      menu_item = gtk_menu_item_new();
      gtk_widget_show(menu_item);
      gtk_container_add(GTK_CONTAINER(menu), menu_item);

      /* rename preset area */
      snprintf(buf, sizeof(buf), "%s: %s", preferences.preset_area[selection]->name, MENU_ITEM_RENAME);
      menu_item = gtk_menu_item_new_with_label(buf);
      gtk_widget_show(menu_item);
      gtk_container_add(GTK_CONTAINER(menu), menu_item);
      g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(preview_preset_area_rename_callback), widget);

      if (selection) /* not available for "full area" */
      {
        /* delete preset area */
        snprintf(buf, sizeof(buf), "%s: %s", preferences.preset_area[selection]->name, MENU_ITEM_DELETE);
        menu_item = gtk_menu_item_new_with_label(buf);
        gtk_widget_show(menu_item);
        gtk_container_add(GTK_CONTAINER(menu), menu_item);
        g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(preview_preset_area_delete_callback), widget);
      }

      if (selection>1) /* available from 3rd item */
      {
        /* move up */
        snprintf(buf, sizeof(buf), "%s: %s", preferences.preset_area[selection]->name, MENU_ITEM_MOVE_UP);
        menu_item = gtk_menu_item_new_with_label(buf);
        gtk_widget_show(menu_item);
        gtk_container_add(GTK_CONTAINER(menu), menu_item);
        g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(preview_preset_area_move_up_callback), widget);
      }

      if ((selection) && (selection < preferences.preset_area_definitions-1))
      {
        /* move down */
        snprintf(buf, sizeof(buf), "%s: %s", preferences.preset_area[selection]->name, MENU_ITEM_MOVE_DWN);
        menu_item = gtk_menu_item_new_with_label(buf);
        gtk_widget_show(menu_item);
        gtk_container_add(GTK_CONTAINER(menu), menu_item);
        g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(preview_preset_area_move_down_callback), widget);
      }

/*      gtk_widget_show(menu); */
      gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, event_button->button, event_button->time);

     return TRUE; /* event is handled */
    }
  }

 return FALSE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_preset_area_callback(GtkWidget *widget, gpointer data)
{
 Preview *p = data;
 int selection;

  DBG(DBG_proc, "preview_preset_area_callback\n");

  selection = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "Selection"));

  p->preset_surface[0] = (double) preferences.preset_area[selection]->xoffset;
  p->preset_surface[1] = (double) preferences.preset_area[selection]->yoffset;
  p->preset_surface[2] = (double) (preferences.preset_area[selection]->xoffset + preferences.preset_area[selection]->width);
  p->preset_surface[3] = (double) (preferences.preset_area[selection]->yoffset + preferences.preset_area[selection]->height);

  preview_zoom_not(NULL, p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_rotation_callback(GtkWidget *widget, gpointer data)
{
  DBG(DBG_proc, "preview_rotation_callback\n");

  Preview *p = data;

  guint new_rotation = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(widget), "Selection"));

  if(new_rotation == p->rotation)
  {
    return;
  }

  p->rotation = new_rotation;

  p->block_update_maximum_output_size_clipping = TRUE; /* necessary when in copy mode */
  preview_update_surface(p, 2); /* rotate surfaces */
  p->block_update_maximum_output_size_clipping = FALSE;
  preview_update_selection(p); /* read selection from backend: correct rotation */
  xsane_batch_scan_update_icon_list(); /* rotate batch scan icons */
  preview_establish_ratio(p); /* make sure ratio is like selected - when selected */
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_establish_ratio(Preview *p)
{
  DBG(DBG_proc, "preview_establish_ratio\n");

  if (p->ratio == 0.0)
  {
    return;
  }

  double width  = geom_get_width_of_surface(p->selection.coordinate);
  double height = geom_get_height_of_surface(p->selection.coordinate);

  if (fabs(width / height - p->ratio) < .01)
  {
    return;
  }

  height = width / p->ratio;
  if (height > geom_get_height_of_surface(p->scanner_surface))
  {
    height = geom_get_height_of_surface(p->scanner_surface);
    width = height * p->ratio;
  }

  p->selection.coordinate[2] = p->selection.coordinate[0] + width;
  if (p->selection.coordinate[2] > p->scanner_surface[2])
  {
    p->selection.coordinate[2] = p->scanner_surface[2];
    p->selection.coordinate[0] = p->selection.coordinate[2] - width;
  }

  p->selection.coordinate[3] = p->selection.coordinate[1] + height;
  if (p->selection.coordinate[3] > p->scanner_surface[3])
  {
    p->selection.coordinate[3] = p->scanner_surface[3];
    p->selection.coordinate[1] = p->selection.coordinate[3] - height;
  }

  preview_draw_selection(p); 
  preview_establish_selection(p); 
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_ratio_callback(GtkWidget *widget, gpointer data)
{
 Preview *p = data;
 double *ratio;

  DBG(DBG_proc, "preview_ratio_callback\n");

  ratio = (double *) g_object_get_data(G_OBJECT(widget), "Selection");

  p->ratio = *ratio;

  preview_establish_ratio(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

static void preview_autoselect_scan_area_callback(GtkWidget *window, gpointer data)
{
 Preview *p=data;

  preview_autoselect_scan_area(p, p->selection.coordinate); /* get autoselection coordinates */
  preview_draw_selection(p); 
  preview_establish_selection(p); 
  xsane_update_histogram(TRUE /* update raw */); /* update histogram (necessary because overwritten by preview_update_surface */
}

/* ---------------------------------------------------------------------------------------------------------------------- */

/* Set initial size of the preview window. */
static void preview_init_window_size(Preview* p)
{
  DBG(DBG_proc, "preview_init_window_size\n");

  int i;
  gint min_width, min_height;
  GdkScreen *screen;
  GdkRectangle geometry;

  /* ensure preview window fits on displays, account for multi-head */
  min_width = gdk_screen_width();
  min_height = gdk_screen_height();

  screen = gdk_screen_get_default();
  for (i = 0; i < gdk_screen_get_n_monitors(screen); i++)
  {
    gdk_screen_get_monitor_geometry(screen, i, &geometry);
    min_width = MIN(min_width, geometry.width);
    min_height = MIN(min_height, geometry.height);
  }

  p->preview_window_width  = (int) (0.3 * min_width);
  p->preview_window_height = (int) (0.5 * min_height);

  gtk_widget_set_size_request(GTK_WIDGET(p->window), p->preview_window_width, p->preview_window_height);

  DBG(DBG_info, "preview_init_window_size: width = %d, height = %d\n",
      p->preview_window_width, p->preview_window_height);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_display_with_correction(Preview *p)
{
#ifdef HAVE_LIBLCMS
  if (xsane.enable_color_management)
  {
    preview_do_color_correction(p);
    gtk_widget_set_sensitive(p->pipette_white, FALSE); /* disable pipette buttons */
    gtk_widget_set_sensitive(p->pipette_gray,  FALSE); /* disable pipette buttons */
    gtk_widget_set_sensitive(p->pipette_black, FALSE); /* disable pipette buttons */

  }
  else
#endif
  {
    preview_do_gamma_correction(p);
    gtk_widget_set_sensitive(p->pipette_white, TRUE); /* enable pipette buttons */
    gtk_widget_set_sensitive(p->pipette_gray,  TRUE); /* enable pipette buttons */
    gtk_widget_set_sensitive(p->pipette_black, TRUE); /* enable pipette buttons */
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_do_gamma_correction(Preview *p)
{
 int x,y;
 int offset;
 u_char *image_data_enhp = NULL;
 const guint16 *image_data_rawp = NULL;
 guint rotate = 16 - preview_gamma_input_bits;

  DBG(DBG_proc, "preview_display_with_correction\n");

  if ((p->image_data_raw) && (p->params.depth > 1) && (preview_gamma_data_red))
  {
    if ( (xsane.param.format == SANE_FRAME_RGB) || /* color preview */
         (xsane.param.format == SANE_FRAME_RED) ||
         (xsane.param.format == SANE_FRAME_GREEN) ||
         (xsane.param.format == SANE_FRAME_BLUE) )
    {
      for (y=0; y < p->image_height; y++)
      {
        offset = 3 * (y * p->image_width);

        image_data_rawp = p->image_data_raw + offset;
        image_data_enhp = p->image_data_enh + offset;

        for (x=0; x < p->image_width; x++)
        {
          *image_data_enhp++ = preview_gamma_data_red  [(*image_data_rawp++) >> rotate];
          *image_data_enhp++ = preview_gamma_data_green[(*image_data_rawp++) >> rotate];
          *image_data_enhp++ = preview_gamma_data_blue [(*image_data_rawp++) >> rotate];
        }

        if (p->gamma_functions_interruptable)
        {
          while (gtk_events_pending())
          {
            DBG(DBG_info, "preview_display_with_correction: calling gtk_main_iteration\n");
            gtk_main_iteration();
          }
        }
      }
    }
    else /* grayscale preview */
    {
     int level;

      for (y=0; y < p->image_height; y++)
      {
        offset = 3 * (y * p->image_width);

        image_data_rawp = p->image_data_raw + offset;
        image_data_enhp = p->image_data_enh + offset;

        for (x=0; x < p->image_width; x++)
        {
          level = (*image_data_rawp++); /* red */
          level += (*image_data_rawp++); /* green */
          level += (*image_data_rawp++); /* blue */
          level /= 3;
          level >>= rotate;
          *image_data_enhp++ = preview_gamma_data_red  [level]; /* use 12 bit gamma table */
          *image_data_enhp++ = preview_gamma_data_green[level];
          *image_data_enhp++ = preview_gamma_data_blue [level];
        }

        if (p->gamma_functions_interruptable)
        {
          while (gtk_events_pending())
          {
            DBG(DBG_info, "preview_read_image_data (raw): calling gtk_main_iteration\n");
            gtk_main_iteration();
          }
        }
      }
    }
  }

  if (p->image_data_enh)
  {
    preview_display_partial_image(p);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

#ifdef HAVE_LIBLCMS
int preview_do_color_correction(Preview *p)
{
 int y;
 u_char *image_data_enhp = NULL;
 const guint16 *image_data_rawp = NULL;
 cmsHPROFILE hInProfile = NULL;
 cmsHPROFILE hOutProfile = NULL;
 cmsHPROFILE hProofProfile = NULL;
 cmsHTRANSFORM hTransform = NULL;
 cmsUInt32Number input_format, output_format;
 cmsUInt32Number cms_flags = 0;
 int proof = 0;
 char *cms_proof_icm_profile = NULL;
 int linesize = 0;


  DBG(DBG_proc, "preview_do_color_correction\n");

#ifdef HAVE_LIBLCMS1
  cmsErrorAction(LCMS_ERROR_SHOW);
#endif

  if (preferences.cms_bpc)
  {
    cms_flags |= cmsFLAGS_BLACKPOINTCOMPENSATION;
  }

  switch (p->cms_proofing)
  {
    default:
    case 0: /* display */
      proof = 0;
     break;

    case 1: /* proof printer */
      cms_proof_icm_profile  = preferences.printer[preferences.printernr]->icm_profile;
      proof = 1;
     break;

    case 2: /* proof custom proofing */
      cms_proof_icm_profile  = preferences.custom_proofing_icm_profile;
      proof = 1;
     break;
  }

  if ( (xsane.param.format == SANE_FRAME_RGB) || /* color preview */
       (xsane.param.format == SANE_FRAME_RED) ||
       (xsane.param.format == SANE_FRAME_GREEN) ||
       (xsane.param.format == SANE_FRAME_BLUE) )
  {
    input_format = TYPE_RGB_16;
    output_format = TYPE_RGB_8;
    linesize = p->image_width * 3;
  }
  else
  {
    input_format = TYPE_GRAY_16;
    output_format = TYPE_GRAY_8;
    linesize = p->image_width;
  }

  hInProfile  = cmsOpenProfileFromFile(xsane.scanner_default_color_icm_profile, "r");
  if (!hInProfile)
  {
   char buf[TEXTBUFSIZE];

    snprintf(buf, sizeof(buf), "%s\n%s %s: %s\n", ERR_CMS_CONVERSION, ERR_CMS_OPEN_ICM_FILE, CMS_SCANNER_ICM, xsane.scanner_default_color_icm_profile);
    xsane_back_gtk_error(buf, TRUE);
   return -1;
  }

  hOutProfile = cmsOpenProfileFromFile(preferences.display_icm_profile, "r");
  if (!hOutProfile)
  {
   char buf[TEXTBUFSIZE];

    cmsCloseProfile(hInProfile);

    snprintf(buf, sizeof(buf), "%s\n%s %s: %s\n", ERR_CMS_CONVERSION, ERR_CMS_OPEN_ICM_FILE, CMS_DISPLAY_ICM, preferences.display_icm_profile);
    xsane_back_gtk_error(buf, TRUE);
   return -1;
  }

  if (proof == 0)
  {
    hTransform = cmsCreateTransform(hInProfile, input_format,
                                    hOutProfile, output_format,
                                    preferences.cms_intent, cms_flags);
  }
  else /* proof */
  {
    cms_flags |= cmsFLAGS_SOFTPROOFING;

    if (p->cms_gamut_check)
    {
      cms_flags |= cmsFLAGS_GAMUTCHECK;
    }

    hProofProfile = cmsOpenProfileFromFile(cms_proof_icm_profile, "r");
    if (!hProofProfile)
    {
     char buf[TEXTBUFSIZE];

      cmsCloseProfile(hInProfile);
      cmsCloseProfile(hOutProfile);

      snprintf(buf, sizeof(buf), "%s\n%s %s: %s\n", ERR_CMS_CONVERSION, ERR_CMS_OPEN_ICM_FILE, CMS_PROOF_ICM, cms_proof_icm_profile);
      xsane_back_gtk_error(buf, TRUE);
     return -1;
    }

    hTransform = cmsCreateProofingTransform(hInProfile, input_format,
                                              hOutProfile, output_format,
                                              hProofProfile,
                                              preferences.cms_intent, p->cms_proofing_intent, cms_flags);
  }

  if (!hTransform)
  {
   char buf[TEXTBUFSIZE];

    cmsCloseProfile(hInProfile);
    cmsCloseProfile(hOutProfile);
    if (proof)
    {
      cmsCloseProfile(hProofProfile);
    }

    snprintf(buf, sizeof(buf), "%s\n%s\n", ERR_CMS_CONVERSION, ERR_CMS_CREATE_TRANSFORM);
    xsane_back_gtk_error(buf, TRUE);
   return -1;
  }


  for (y=0; y < p->image_height; y++)
  {
    image_data_rawp = p->image_data_raw + linesize * y;
    image_data_enhp = p->image_data_enh + linesize * y;

    cmsDoTransform(hTransform, image_data_rawp, image_data_enhp, (cmsUInt32Number) p->image_width);

    if (p->gamma_functions_interruptable)
    {
      while (gtk_events_pending())
      {
        DBG(DBG_info, "preview_do_color_correction: calling gtk_main_iteration\n");
        gtk_main_iteration();
      }
    }
  }

  if (p->image_data_enh)
  {
    preview_display_partial_image(p);
  }

 return 0;
}
#endif

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_calculate_raw_histogram(const Preview *p, SANE_Int *count_raw, SANE_Int *count_raw_red,
                                     SANE_Int *count_raw_green, SANE_Int *count_raw_blue)
{
 int x, y;
 int offset;
 SANE_Int red_raw, green_raw, blue_raw;
 const guint16 *image_data_rawp;
 
  DBG(DBG_proc, "preview_calculate_raw_histogram\n");

  double image_rectangle_f[4];
  preview_tranform_surface_device_to_image(p, p->selection.coordinate, image_rectangle_f);

  int image_rectangle[4];
  geom_set_int_surface_by_double_surface(image_rectangle_f, image_rectangle);
  xsane_bound_int(image_rectangle, 0, p->image_width);
  xsane_bound_int(image_rectangle + 2, 0, p->image_width);
  xsane_bound_int(image_rectangle + 1, 0, p->image_height);
  xsane_bound_int(image_rectangle + 3, 0, p->image_height);

  if ((p->image_data_raw) && (p->params.depth > 1) && (preview_gamma_data_red))
  {
    for (y = image_rectangle[1]; y <= image_rectangle[3]; y++)
    {
      offset = 3 * (y * p->image_width + image_rectangle[0]);
      image_data_rawp = p->image_data_raw + offset;

      if (!histogram_medium_gamma_data_red) /* no medium gamma table for histogran */
      {
        for (x = image_rectangle[0]; x <= image_rectangle[2]; x++)
        {
          red_raw   = (*image_data_rawp++) >> 8; /* reduce from 16 to 8 bits */
          green_raw = (*image_data_rawp++) >> 8;
          blue_raw  = (*image_data_rawp++) >> 8;

          count_raw      [(u_char) ((red_raw + green_raw + blue_raw)/3)]++;
          count_raw_red  [red_raw]++;
          count_raw_green[green_raw]++;
          count_raw_blue [blue_raw]++;
        }
      }
      else /* use medium gamma table for raw histogram */
      {
       guint rotate = 16 - preview_gamma_input_bits;

        for (x = image_rectangle[0]; x <= image_rectangle[2]; x++)
        {
          red_raw   = histogram_medium_gamma_data_red  [(*image_data_rawp++) >> rotate];
          green_raw = histogram_medium_gamma_data_green[(*image_data_rawp++) >> rotate];
          blue_raw  = histogram_medium_gamma_data_blue [(*image_data_rawp++) >> rotate];

          count_raw      [(u_char) ((red_raw + green_raw + blue_raw)/3)]++;
          count_raw_red  [red_raw]++;
          count_raw_green[green_raw]++;
          count_raw_blue [blue_raw]++;
        }
      }

      if (p->gamma_functions_interruptable)
      {
        while (gtk_events_pending())
        {
          DBG(DBG_info, "preview_calculate_raw_histogram: calling gtk_main_iteration\n");
          gtk_main_iteration();
        }
      }
    }
  }
  else /* no preview image => all colors = 1 */
  {
   int i;

    for (i = 1; i <= 254; i++)
    {
      count_raw      [i] = 0;
      count_raw_red  [i] = 0;
      count_raw_green[i] = 0;
      count_raw_blue [i] = 0;
    }

    count_raw      [0] = 10;
    count_raw_red  [0] = 10;
    count_raw_green[0] = 10;
    count_raw_blue [0] = 10;

    count_raw      [255] = 10;
    count_raw_red  [255] = 10;
    count_raw_green[255] = 10;
    count_raw_blue [255] = 10;
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_calculate_enh_histogram(const Preview *p, SANE_Int *count, SANE_Int *count_red, SANE_Int *count_green, SANE_Int *count_blue)
{
 int x, y;
 int offset;
 u_char red, green, blue;
 const guint16 *image_data_rawp;
 guint rotate = 16 - preview_gamma_input_bits;
 
  DBG(DBG_proc, "preview_calculate_enh_histogram\n");

  double image_rectangle_f[4];
  preview_tranform_surface_device_to_image(p, p->selection.coordinate, image_rectangle_f);

  int image_rectangle[4];
  geom_set_int_surface_by_double_surface(image_rectangle_f, image_rectangle);
  xsane_bound_int(image_rectangle, 0, p->image_width);
  xsane_bound_int(image_rectangle + 2, 0, p->image_width);
  xsane_bound_int(image_rectangle + 1, 0, p->image_height);
  xsane_bound_int(image_rectangle + 3, 0, p->image_height);
   
  if ((p->image_data_raw) && (p->params.depth > 1) && (preview_gamma_data_red))
  {
    for (y = image_rectangle[1]; y <= image_rectangle[3]; y++)
    {
      offset = 3 * (y * p->image_width + image_rectangle[0]);
      image_data_rawp = p->image_data_raw + offset;

      for (x = image_rectangle[0]; x <= image_rectangle[2]; x++)
      {
        red   = histogram_gamma_data_red  [(*image_data_rawp++) >> rotate];
        green = histogram_gamma_data_green[(*image_data_rawp++) >> rotate];
        blue  = histogram_gamma_data_blue [(*image_data_rawp++) >> rotate];

        count      [(u_char) ((red + green + blue)/3)]++;
        count_red  [red]++;
        count_green[green]++;
        count_blue [blue]++;
      }

      if (p->gamma_functions_interruptable)
      {
        while (gtk_events_pending())
        {
          DBG(DBG_info, "preview_calculate_enh_histogram: calling gtk_main_iteration\n");
          gtk_main_iteration();
        }
      }
    }
  }
  else /* no preview image => all colors = 1 */
  {
   int i;

    for (i = 1; i <= 254; i++)
    {
      count      [i] = 0;
      count_red  [i] = 0;
      count_green[i] = 0;
      count_blue [i] = 0;
    }

    count      [0] = 10;
    count_red  [0] = 10;
    count_green[0] = 10;
    count_blue [0] = 10;

    count      [255] = 10;
    count_red  [255] = 10;
    count_green[255] = 10;
    count_blue [255] = 10;
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_gamma_correction(Preview *p, guint gamma_input_bits,
                              u_char *gamma_red, u_char *gamma_green, u_char *gamma_blue,
                              u_char *gamma_red_hist, u_char *gamma_green_hist, u_char *gamma_blue_hist,
                              u_char *medium_gamma_red_hist, u_char *medium_gamma_green_hist, u_char *medium_gamma_blue_hist)
{
  DBG(DBG_proc, "preview_gamma_correction\n");

  preview_gamma_data_red   = gamma_red;
  preview_gamma_data_green = gamma_green;
  preview_gamma_data_blue  = gamma_blue;

  histogram_gamma_data_red   = gamma_red_hist;
  histogram_gamma_data_green = gamma_green_hist;
  histogram_gamma_data_blue  = gamma_blue_hist;

  histogram_medium_gamma_data_red   = medium_gamma_red_hist;
  histogram_medium_gamma_data_green = medium_gamma_green_hist;
  histogram_medium_gamma_data_blue  = medium_gamma_blue_hist;

  preview_gamma_input_bits   = gamma_input_bits;

  preview_display_with_correction(p);

  p->previous_selection.active = FALSE;
  preview_draw_selection(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_redraw_rulers(Preview *p)
{
  DBG(DBG_proc, "preview_redraw_rulers\n");

  /* Values for the top left corner of window. */
  const double left_value = p->surface[0] - p->win_image_surface[0] / p->current_zoom.zoom_factor;
  const double top_value = p->surface[1] - p->win_image_surface[1] / p->current_zoom.zoom_factor;

  /* Values for the bottom right corner of window. */
  const double right_value = left_value + (double) p->preview_window_width / p->current_zoom.zoom_factor;
  const double bottom_value = top_value + (double) p->preview_window_height / p->current_zoom.zoom_factor;

  gtk_ruler_set_range(GTK_RULER(p->hruler), left_value, right_value, p->win_image_surface[0], 20);
  gtk_ruler_set_range(GTK_RULER(p->vruler), top_value, bottom_value, p->win_image_surface[1], 20);

  gtk_label_set_text(GTK_LABEL(p->unit_label), xsane_back_gtk_unit_string(p->surface_unit));
}

/* ---------------------------------------------------------------------------------------------------------------------- */

gint preview_area_resize_handler(GtkWidget *widget, GdkEvent *event, gpointer data)
{
 Preview *p = (Preview *) data;
 GtkAllocation alloc;
  DBG(DBG_proc, "preview_area_resize_handler\n");

  gtk_widget_get_allocation(p->window, &alloc);
  p->preview_window_width  = alloc.width;
  p->preview_window_height = alloc.height;

  preview_update_surface(p, 1);

  return FALSE;
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_update_maximum_output_size(Preview *p)
{
 double xxx = 0.0;
 double yyy = 0.0;

 int paper_orientation = 0;

  if (p->block_update_maximum_output_size_clipping)
  {
    DBG(DBG_info, "preview_update_maximum_output_size: blocked\n");
   return;
  }

  DBG(DBG_proc, "preview_update_maximum_output_size\n");

  p->block_update_maximum_output_size_clipping = TRUE;

  if ( (p->maximum_output_width >= INF) || (p->maximum_output_height >= INF) )
  {
    if (p->selection_maximum.active)
    {
      p->selection_maximum.active = FALSE;
    }
  }
  else /* we have a maximum output size definition */
  {
    p->previous_selection_maximum = p->selection_maximum;
    p->selection_maximum.active = TRUE;

    if (p->paper_orientation & 4) /* center? */
    {
      paper_orientation = p->paper_orientation;
    }
    else /* not in center */
    {
      switch (p->rotation)
      {
        default:
        case 0: /* 0 degree */
          paper_orientation = p->paper_orientation & 3;
         break;

        case 1: /* 90 degree */
          paper_orientation = (1 - p->paper_orientation) & 3;
         break;

        case 2: /* 180 degree */
          paper_orientation = (2 + p->paper_orientation) & 3;
         break;

        case 3: /* 270 degree */
          paper_orientation = (3 - p->paper_orientation) & 3;
         break;
  
        case 4: /* 0 degree, x mirror */
          paper_orientation = (1 - p->paper_orientation) & 3;
         break;
 
        case 5: /* 90 degree, x mirror */
          paper_orientation = p->paper_orientation & 3;
         break;

        case 6: /* 180 degree, x mirror */
          paper_orientation = (3 - p->paper_orientation) & 3;
         break;

        case 7: /* 270 degree, x mirror */
          paper_orientation = (2 + p->paper_orientation) & 3;
         break;
      }
    }

    switch (paper_orientation)
    {
      default:
      case 0: /* top left portrait */
      case 8: /* top left landscape */
        xxx = 0.0;
        yyy = 0.0;
       break;

      case 1: /* top right portrait */
      case 9: /* top right landscape */
        xxx = 1.0;
        yyy = 0.0;
       break;

      case 2: /* bottom right portrait */
      case 10: /* bottom right landscape */
        xxx = 1.0;
        yyy = 1.0;
       break;

      case 3: /* bottom left portrait */
      case 11: /* bottom left landscape */
        xxx = 0.0;
        yyy = 1.0;
       break;

      case 4: /* center portrait */
      case 12: /* center landscape */
        xxx = 0.5;
        yyy = 0.5;
       break;
    }

    double coord_dimensions[2];
    geom_get_dimensions_of_surface(p->selection.coordinate, coord_dimensions);

    p->selection_maximum.coordinate[0] = p->selection.coordinate[0] + xxx * coord_dimensions[0] - p->maximum_output_width  * xxx;
    p->selection_maximum.coordinate[1] = p->selection.coordinate[1] + yyy * coord_dimensions[1] - p->maximum_output_height * yyy;
    p->selection_maximum.coordinate[2] = p->selection.coordinate[0] + xxx * coord_dimensions[0] + p->maximum_output_width  * (1.0f - xxx);
    p->selection_maximum.coordinate[3] = p->selection.coordinate[1] + yyy * coord_dimensions[1] + p->maximum_output_height * (1.0f - yyy);


    if ( (p->selection.coordinate[0] < p->selection_maximum.coordinate[0]) ||
         (p->selection.coordinate[1] < p->selection_maximum.coordinate[1]) ||
         (p->selection.coordinate[2] > p->selection_maximum.coordinate[2]) ||
         (p->selection.coordinate[3] > p->selection_maximum.coordinate[3]) )
    {
     int selection_changed = FALSE;

      if (p->selection.coordinate[0] < p->selection_maximum.coordinate[0])
      {
        p->selection.coordinate[0] = p->selection_maximum.coordinate[0];
        selection_changed = TRUE;
      }

      if (p->selection.coordinate[1] < p->selection_maximum.coordinate[1])
      {
        p->selection.coordinate[1] = p->selection_maximum.coordinate[1];
        selection_changed = TRUE;
      }

      if (p->selection.coordinate[2] > p->selection_maximum.coordinate[2])
      {
        p->selection.coordinate[2] = p->selection_maximum.coordinate[2];
        selection_changed = TRUE;
      }

      if (p->selection.coordinate[3] > p->selection_maximum.coordinate[3])
      {
        p->selection.coordinate[3] = p->selection_maximum.coordinate[3];
        selection_changed = TRUE;
      }

      preview_draw_selection(p);

      if (selection_changed)
      {
        preview_establish_selection(p); 
      }
    }
  }

  p->block_update_maximum_output_size_clipping=FALSE;
}
/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_set_maximum_output_size(Preview *p, double width, double height, int paper_orientation)
{
 /* witdh and height in device units */
  DBG(DBG_proc, "preview_set_maximum_output_size\n");

  p->maximum_output_width  = width;
  p->maximum_output_height = height;
  p->paper_orientation     = paper_orientation;

  preview_update_maximum_output_size(p);
  preview_draw_selection(p);
}

/* ---------------------------------------------------------------------------------------------------------------------- */
#define AUTORAISE_ERROR 30
void preview_autoraise_scan_area(const Preview *p, int preview_x, int preview_y, double *autoselect_coord)
{
 int x, y;
 int offset;
 double average_color_r, average_color_g, average_color_b;
 int count;
 double error;
 int top, bottom, left, right;
 int top_ok, bottom_ok, left_ok, right_ok;

  DBG(DBG_proc, "preview_autoraise_scan_area\n");

  int image_x, image_y;
  {
    double preview_coord[2];
    preview_coord[0] = (double) preview_x;
    preview_coord[1] = (double) preview_y;

    double image_coord[2];
    preview_transform_coordinate_window_to_image(p, preview_coord, image_coord);
    image_x = (int) image_coord[0];
    image_y = (int) image_coord[1];
  }

  top_ok    = FALSE;
  bottom_ok = FALSE;
  left_ok   = FALSE;
  right_ok  = FALSE;

  top    = image_y - 5;
  bottom = image_y + 5;
  left   = image_x - 5;
  right  = image_x + 5;

  while (!(top_ok && bottom_ok && left_ok && right_ok))
  {
    /* search top */
    if (!top_ok)
    {
      top--;
    }

    top_ok = TRUE;

    if (top <= 0)
    {
      top = 0;
    }
    else
    {
      average_color_r = average_color_g = average_color_b = 0;
      count = 0;

      for (x = left; x < right; x++)
      {
        offset = 3 * (top * p->image_width + x);
        average_color_r += p->image_data_enh[offset + 0];
        average_color_g += p->image_data_enh[offset + 1];
        average_color_b += p->image_data_enh[offset + 2];
        count++;
      }

      average_color_r /= (double) count;
      average_color_g /= (double) count;
      average_color_b /= (double) count;

      for (x = left; x < right; x++)
      {
        offset = 3 * (top * p->image_width + x);

        error = fabs(p->image_data_enh[offset + 0] - average_color_r) +
                fabs(p->image_data_enh[offset + 1] - average_color_g) +
                fabs(p->image_data_enh[offset + 2] - average_color_b);

        if (error > AUTORAISE_ERROR)
        {
          top_ok = FALSE;
          break;
        }
      }
    }

    /* search bottom */
    if (!bottom_ok)
    {
      bottom++;
    }

    bottom_ok = TRUE;

    if (bottom >= p->image_height-1)
    {
      bottom = p->image_height-1;
    }
    else
    {
      average_color_r = average_color_g = average_color_b = 0;
      count = 0;

      for (x = left; x < right; x++)
      {
        offset = 3 * (bottom * p->image_width + x);
        average_color_r += p->image_data_enh[offset + 0];
        average_color_g += p->image_data_enh[offset + 1];
        average_color_b += p->image_data_enh[offset + 2];
        count++;
      }

      average_color_r /= (double) count;
      average_color_g /= (double) count;
      average_color_b /= (double) count;

      for (x = left; x < right; x++)
      {
        offset = 3 * (bottom * p->image_width + x);

        error = fabs(p->image_data_enh[offset + 0] - average_color_r) +
                fabs(p->image_data_enh[offset + 1] - average_color_g) +
                fabs(p->image_data_enh[offset + 2] - average_color_b);

        if (error > AUTORAISE_ERROR)
        {
          bottom_ok = FALSE;
          break;
        }
      }
    }

    /* search left */
    if (!left_ok)
    {
      left--;
    }

    left_ok = TRUE;

    if (left <= 0)
    {
      left = 0;
    }
    else
    {
      average_color_r = average_color_g = average_color_b = 0;
      count = 0;

      for (y = top; y < bottom; y++)
      {
        offset = 3 * (left + y * p->image_width);
        average_color_r += p->image_data_enh[offset + 0];
        average_color_g += p->image_data_enh[offset + 1];
        average_color_b += p->image_data_enh[offset + 2];
        count++;
      }

      average_color_r /= (double) count;
      average_color_g /= (double) count;
      average_color_b /= (double) count;

      for (y = top; y < bottom; y++)
      {
        offset = 3 * (left + y * p->image_width);

        error = fabs(p->image_data_enh[offset + 0] - average_color_r) +
                         fabs(p->image_data_enh[offset + 1] - average_color_g) +
                         fabs(p->image_data_enh[offset + 2] - average_color_b);

        if (error > AUTORAISE_ERROR)
        {
          left_ok = FALSE;
          break;
        }
      }
    }

    /* search right */
    if (!right_ok)
    {
      right++;
    }

    right_ok = TRUE;

    if (right >= p->image_width-1)
    {
      right = p->image_width-1;
    }
    else
    {
      average_color_r = average_color_g = average_color_b = 0;
      count = 0;

      for (y = top; y < bottom; y++)
      {
        offset = 3 * (right + y * p->image_width);
        average_color_r += p->image_data_enh[offset + 0];
        average_color_g += p->image_data_enh[offset + 1];
        average_color_b += p->image_data_enh[offset + 2];
        count++;
      }

      average_color_r /= (double) count;
      average_color_g /= (double) count;
      average_color_b /= (double) count;

      for (y = top; y < bottom; y++)
      {
        offset = 3 * (right + y * p->image_width);

        error = fabs(p->image_data_enh[offset + 0] - average_color_r) +
                fabs(p->image_data_enh[offset + 1] - average_color_g) +
                fabs(p->image_data_enh[offset + 2] - average_color_b);

        if (error > AUTORAISE_ERROR)
        {
          right_ok = FALSE;
          break;
        }
      }
    }
  }

  double isurface[4] = {(double) left, (double) top, (double) right, (double) bottom };
  preview_transform_surface_image_to_device(p, isurface, autoselect_coord);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_autoselect_scan_area(const Preview *p, double *autoselect_coord)
{
 int x, y;
 int offset;
 double color;
 int top, bottom, left, right;
 long bright_sum = 0;
 int brightness;
 int background_white;

  DBG(DBG_proc, "preview_autoselect_scan_area\n");

  /* try to find out background color */
  /* add color values at the margins */
  /* and see if it is more black or more white */

  /* upper line */
  for (x = 0; x < p->image_width; x++)
  {
    offset = 3 * x;
    bright_sum += (long int) ((p->image_data_enh[offset + 0] + p->image_data_enh[offset + 1] + p->image_data_enh[offset + 2]) / 3.0); 
  }

  /* lower line */
  for (x = 0; x < p->image_width; x++)
  {
    offset = 3 * ( (p->image_height-1) * p->image_width + x);
    bright_sum += (long int) ((p->image_data_enh[offset + 0] + p->image_data_enh[offset + 1] + p->image_data_enh[offset + 2]) / 3.0); 
  }

  /* left line */
  for (y = 0; y < p->image_height; y++)
  {
    offset = 3 * y * p->image_width;
    bright_sum += (long int) ((p->image_data_enh[offset + 0] + p->image_data_enh[offset + 1] + p->image_data_enh[offset + 2]) / 3.0);
  }

  /* right line */
  for (y = 0; y < p->image_height; y++)
  {
    offset = 3 * (y * p->image_width + p->image_width - 1);
    bright_sum += (long int) ((p->image_data_enh[offset + 0] + p->image_data_enh[offset + 1] + p->image_data_enh[offset + 2]) / 3.0);
  }

  brightness = (int) (bright_sum / (2 * (p->image_width + p->image_height) ));
  DBG(DBG_info, "preview_autoselect_scan_area: average margin brightness is %d\n", brightness);

  if ( brightness > 128 )
  {
    DBG(DBG_info, "preview_autoselect_scan_area: background is white\n");
    background_white = 1;
  }
  else
  {
    DBG(DBG_info, "preview_autoselect_scan_area: background is black\n");
    background_white = 0;
  }
  

  /* search top */
  top = 0;
  for (y = 0; y < p->image_height; y++)
  {
    for (x = 0; x < p->image_width; x++)
    {
      offset = 3 * (y * p->image_width + x);
      color = (p->image_data_enh[offset + 0] + p->image_data_enh[offset + 1] + p->image_data_enh[offset + 2]) / 3.0;
      if (background_white)
      {
        if (color < 200)
        {
          top = y;
          break;
        }
      }

      else if (color > 55 )
      {
        top = y;
        break;
      }
    }
    if (top)
    {
      break;
    }
  }


  /* search bottom */
  bottom = 0;
  for (y = p->image_height-1; y > top; y--)
  {
    for (x = 0; x < p->image_width; x++)
    {
      offset = 3 * (y * p->image_width + x);
      color = (p->image_data_enh[offset + 0] + p->image_data_enh[offset + 1] + p->image_data_enh[offset + 2]) / 3.0;
      if (background_white)
      {
        if (color < 200)
        {
          bottom = y;
          break;
        }
      }
      else if (color > 55 )
      {
        bottom = y;
        break;
      }
    }
    if (bottom)
    {
      break;
    }
  }


  /* search left */
  left = 0;
  for (x = 0; x < p->image_width; x++)
  {
    for (y = 0; y < p->image_height; y++)
    {
      offset = 3 * (y * p->image_width + x);
      color = (p->image_data_enh[offset + 0] + p->image_data_enh[offset + 1] + p->image_data_enh[offset + 2]) / 3.0;
      if (background_white)
      {
        if (color < 200)
        {
          left = x;
          break;
        }
      }
      else if (color > 55 )
      {
        left = x;
        break;
      }
    }
    if (left)
    {
      break;
    }
  }


  /* search right */
  right = 0;
  for (x = p->image_width-1; x > left; x--)
  {
    for (y = 0; y < p->image_height; y++)
    {
      offset = 3 * (y * p->image_width + x);
      color = (p->image_data_enh[offset + 0] + p->image_data_enh[offset + 1] + p->image_data_enh[offset + 2]) / 3.0;
      if (background_white)
      {
        if (color < 200)
        {
          right = x;
          break;
        }
      }
      else if (color > 55 )
      {
        right = x;
        break;
      }
    }
    if (right)
    {
      break;
    }
  }

  if ( (top >= bottom) || (right <= left) ) /* empty selection: use complete image */
  {
    DBG(DBG_info, "autoselect_scan_area: empty selection: using complete area\n");
    top    = 0;
    bottom = p->image_height -1;
    left   = 0;
    right  = p->image_width -1;
  }

  double isurface[4] = {(double) left, (double) top, (double) right, (double) bottom };
  preview_transform_surface_image_to_device(p, isurface, autoselect_coord);
}

/* ---------------------------------------------------------------------------------------------------------------------- */

void preview_display_valid(Preview *p)
{
  DBG(DBG_proc, "preview_display_valid\n");

  if (p->scanning)/* we are just scanning the preview */
  {
    DBG(DBG_info, "preview scanning\n");

    gtk_widget_show(p->scanning_pixmap);
    gtk_widget_hide(p->incomplete_pixmap);
    gtk_widget_hide(p->valid_pixmap);
    gtk_widget_hide(p->invalid_pixmap);
  }
  else if ((xsane.medium_changed) || (xsane.xsane_channels != p->preview_channels) || (p->invalid) ) /* preview is not valid */
  {
    DBG(DBG_info, "preview not valid\n");

    gtk_widget_show(p->invalid_pixmap);
    gtk_widget_hide(p->scanning_pixmap);
    gtk_widget_hide(p->incomplete_pixmap);
    gtk_widget_hide(p->valid_pixmap);
  }
  else if (p->scan_incomplete)/* preview scan has been cancled */
  {
    DBG(DBG_info, "preview incomplete\n");

    gtk_widget_show(p->incomplete_pixmap);
    gtk_widget_hide(p->scanning_pixmap);
    gtk_widget_hide(p->valid_pixmap);
    gtk_widget_hide(p->invalid_pixmap);
  }
  else /* preview is valid */
  {
    DBG(DBG_info, "preview valid\n");

    gtk_widget_show(p->valid_pixmap);
    gtk_widget_hide(p->scanning_pixmap);
    gtk_widget_hide(p->incomplete_pixmap);
    gtk_widget_hide(p->invalid_pixmap);
  }
}

/* ---------------------------------------------------------------------------------------------------------------------- */
